package common;

import java.io.Serializable;

import javafx.beans.property.SimpleStringProperty;

public class Msg implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -939349049723002744L;
	private String mID;
	private String hisMsg;
	private String ansMsg;
	private String status;
	private String fromID;
	private String subject;
	private String fromName;
	public String getmID() {
		return mID;
	}
	public void setmID(String mID) {
		this.mID = mID;
	}
	public String getHisMsg() {
		return hisMsg;
	}
	public void setHisMsg(String hisMsg) {
		this.hisMsg = hisMsg;
	}
	public String getAnsMsg() {
		return ansMsg;
	}
	public void setAnsMsg(String ansMsg) {
		this.ansMsg = ansMsg;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFromID() {
		return fromID;
	}
	public void setFromID(String fromID) {
		this.fromID = fromID;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getFromName() {
		return fromName;
	}
	public void setFromName(String fromName) {
		this.fromName = fromName;
	}
	
	
	
	
}
