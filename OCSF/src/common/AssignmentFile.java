package common;

import java.io.File;
import java.io.Serializable;

/**
 * IMPORTENT:
 * Must set all attributes in the constructor.
 * @author adinn
 *
 */
public class AssignmentFile implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 4591674110008983974L;
	/**Max of 500mb*/
	private final long MaxSize = 500000000 ;
	private long FileSize = 0;
	/**Name of the File -> file.getFileName()*/
	private String FileName = null;
	/**Array of bytes of the file*/
	private  byte[] mybytearray;
	/**Grade of the ass*/
	private String AssGrade = null;
	/**Name of assignment file*/
	private String AssName = null;
	/**Date of the upload*/
	private String UploadDate = null;
	/**Date of the due date.*/
	private String DueDate = null;
	/**Semester ID of the Ass*/
	private String SemesterID = null;
	/**Course ID of the Ass*/
	private String CourseID = null;
	/**The user name*/
	private String UserName = null;
	/**Number of the ass*/
	private String AssNum = null;
	/**True to set a grade false otherwise*/
	private boolean setGrade = false;
	/**Student ID */
	private String StudentID = null;
	/**Replace exist one*/
	private boolean replace = false;
	/**UserType 2 - For Student , 3 - For Teacher */
	private int UserType = 0;
	/**The File exc*/
	private String FileExc = null;
	/**
	 * 
	 * @param FileName
	 * @param MyFile
	 * @param UserName
	 * @param SemesterID
	 * @param CourseID
	 */
	public AssignmentFile(String FileName, String UserName, String SemesterID, String CourseID,String dueDate,String uploadDate,String AssName,int UserType) {
		this.FileName = FileName;
		this.UserName = UserName;
		this.SemesterID = SemesterID;
		this.CourseID = CourseID;
		this.UserType = UserType;
		this.DueDate = dueDate;
		this.UploadDate = uploadDate;
		this.AssName = AssName;
	}
	
	public AssignmentFile (){
		
	}

	public String getCourseID() {
		return CourseID;
	}

	public void setCourseID(String courseID) {
		CourseID = courseID;
	}

	/**
	 * Get the file name and his extension.
	 */
	public String getFileName() {
		return FileName;
	}

	/**
	 * Set the file name and his extension.
	 */
	public void setFileName(String fileName) {
		this.FileName = fileName;
	}

	public String getSemesterID() {
		return SemesterID;
	}

	public void setSemesterID(String semesterID) {
		SemesterID = semesterID;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getAssName() {
		return AssName;
	}

	public void setAssName(String assName) {
		AssName = assName;
	}

	public String getUploadDate() {
		return UploadDate;
	}

	public void setUploadDate(String uploadDate) {
		this.UploadDate = uploadDate;
	}

	public String getDueDate() {
		return DueDate;
	}

	public void setDueDate(String dueDate) {
		this.DueDate = dueDate;
	}

	public int getUserType() {
		return UserType;
	}

	public void setUserType(int userType) {
		UserType = userType;
	}

	public long getMaxSize() {
		return MaxSize;
	}

	public String getAssNum() {
		return AssNum;
	}

	public void setAssNum(String assNum) {
		this.AssNum = assNum;
	}

	public String getAssGrade() {
		return AssGrade;
	}

	public void setAssGrade(String assGrade) {
		AssGrade = assGrade;
	}

	public byte[] getMybytearray() {
		return mybytearray;
	}

	public void setMybytearray(byte[] mybytearray) {
		this.mybytearray = mybytearray;
	}

	public long getFileSize() {
		return FileSize;
	}

	public void setFileSize(long fileSize) {
		FileSize = fileSize;
	}

	public void initArray(int length) {
		mybytearray = new byte[length];
	}

	public void setArrayOfBytes(byte[] arrayOfBytes) {
		this.mybytearray = arrayOfBytes;
	}

	public boolean isSetGrade() {
		return setGrade;
	}

	public void setSetGrade(boolean setGrade) {
		this.setGrade = setGrade;
	}

	public String getStudentID() {
		return StudentID;
	}

	public void setStudentID(String studentID) {
		StudentID = studentID;
	}

	public boolean isReplace() {
		return replace;
	}

	public void setReplace(boolean replace) {
		this.replace = replace;
	}

	public String getFileExc() {
		return FileExc;
	}

	public void setFileExc(String fileExc) {
		FileExc = fileExc;
	}
}
