package serverClient;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import app.Main;
import app.common.Assignment;
import client.ClientSide;
import common.AssignmentFile;
import common.ChatIF;
import common.Msg;

/**
 * This class constructs the UI for a chat client.  It implements the
 * chat interface in order to activate the display() method.
 * Warning: Some of the code here is cloned in ServerConsole 
 *
 * @author Ran Algawi
 * @author Adi Dinner
 * @version 1.1
 */
@SuppressWarnings("static-access")
public class ClientConsole implements ChatIF 
{

	
  //Class variables *************************************************
  private Main main;
  private boolean Connected = false;
  
  /**
   * The default port to connect on.
   */
  final public static int DEFAULT_PORT = 5555;
  
  
  /**
   * The default IP to connect on.
   */
  final public static String DEFAULT_SERVER_IP = "127.0.0.1";
  
  //Instance variables **********************************************
  
  /**
   * The instance of the client that created this ConsoleChat.
   */
  ClientSide client;

  
  //Constructors ****************************************************

  /**
   * Constructs an instance of the ClientConsole UI.
   *
   * @param host The host to connect to.
   * @param port The port to connect on.
   */
public ClientConsole(String host, int port) 
  {
    try 
    {
      client= new ClientSide(host, port, this);
      main.setActive(false);

    } 
    catch(IOException exception) 
    {
      System.out.println("Error: Can't setup connection!"
                + " Terminating client.");
      main.setActive(true);
    }
  }

  
  //Instance methods ************************************************

  
  
  
  /**
   * This method waits for input from the application.  Once it is 
   * received, it sends it to the client's message handler.
   */
  public void test(String message) 
  {
	client.handleMessageFromClientUI(message);
  }
  
  public void Queries(String[] message) 
  {
	client.handleMessageFromClientUI_String(message);
  }
  
  public void insertUpdate(ArrayList<String> message) 
  {
	client.handleMessageFromClientUI_TEST(message);
  }
  
  public void insertFile(AssignmentFile assFile) {
	  client.handleMessageFromClientUI_AssignmentFile(assFile);
}
  
  public void insertFile(AssignmentFile[] assFile) {
	  client.handleMessageFromClientUI_AssignmentFile(assFile);
}
  
  public void sendMsg(Msg newMsg){
	  client.handleMessageFromClientUI_Msg(newMsg);
  }
  
 

    

  /**
   * This method overrides the method in the ChatIF interface.  It
   * displays a message onto the screen.
   *
   * @param message The string to be displayed.
   */
  public void display(String message) 
  {

    System.out.println("> " + message);
  }


public boolean isConnected() {
	return Connected;
}


public void setConnected(boolean connected) {
	Connected = connected;
}






  
  //Class methods ***************************************************
  

}
//End of ConsoleChat class
