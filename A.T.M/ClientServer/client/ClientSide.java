package client;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import app.Main;
import app.common.Assignment;
import app.common.sendMsg.SendMsgGUIcontroller;
import common.AssignmentFile;
import common.ChatIF;
import common.Msg;
import javafx.application.Platform;
import ocsf.client.AbstractClient;

/**
 * This class overrides some of the methods defined in the abstract
 * superclass in order to give more functionality to the client.
 *
 * @author Ran Algawi
 * @author Adi Dinner
 * @version 1.01
 */
@SuppressWarnings("static-access")
public class ClientSide extends AbstractClient
{
  //Instance variables **********************************************
  
	
  /**
   * The interface type variable.  It allows the implementation of 
   * the display method in the client.
   */
  ChatIF clientUI;

  
  //Constructors ****************************************************
  
  /**
   * Constructs an instance of the chat client.
   *
   * @param host The server to connect to.
   * @param port The port number to connect on.
   * @param clientUI The interface type variable.
   */
  
  public ClientSide(String host, int port, ChatIF clientUI) 
    throws IOException 
  {
    super(host, port); //Call the superclass constructor
    this.clientUI = clientUI;
    openConnection();
  }

  
  //Instance methods ************************************************
  /**
   * This method handles all data that comes in from the server.
   *
   * @param msg The message from the server.
 * @throws SQLException 
   */


public void handleMessageFromServer(Object msg)
  {
	if(msg instanceof String){
		String newString = (String)msg;
		if (newString.compareTo("ServerIsDown")==0){
			Main.setActive(true);
			Main.setDBcontrollerNull();
			Main.connectToServer();
			Main.getBottomPaneController().setNoConnection();
			Main.setConnected(false);
		}
		if(newString.compareTo("FileSaved")==0){
			Main.getDbController().go();
		}
	}//End instanceof String
	else if (msg instanceof Integer){
		Main.getDbController().setType((int)msg);
		Main.getDbController().serverResponsed();
	  }//End instanceof of Integer
	  else if (msg instanceof ArrayList<?>){
		  try{
		  ArrayList<Object[][]> resultsList = (ArrayList<Object[][]>) msg;
		  Main.getDbController().go();
		  Main.getDbController().setResultsList(resultsList);
		  }catch(Exception e){
			  e.printStackTrace();
			  Main.getDbController().go(); 
		  }
	  }//End instanceof of ArrayList<?>
	  else if(msg instanceof Msg){
		  try{
		  SendMsgGUIcontroller.addNewMsg((Msg)msg);
		  }catch(ClassCastException e){}
	  }
  }

  /**
   * This method handles all data coming from the UI            
   *
   * @param message The message from the UI.    
   */
  public void handleMessageFromClientUI(String message)
  {
    try
    {
    	sendToServer(message);
    }
    catch(IOException e)
    {
      clientUI.display
        ("Could not send message to server.  Terminating client.");
      quit();
    }
  }
  
  public void handleMessageFromClientUI_String(String[] message)
  {
    try
    {
    	sendToServer(message);
    }
    catch(IOException e)
    {
      clientUI.display
        ("Could not send message to server.  Terminating client.");
      quit();
    }
  }
  
  
  public void handleMessageFromClientUI_TEST(ArrayList<String> message)
  {
    try
    {
    	sendToServer(message);
    }
    catch(IOException e)
    {
      clientUI.display
        ("Could not send message to server.  Terminating client.");
      quit();
    }
  }
  
  public void handleMessageFromClientUI_AssignmentFile(AssignmentFile assFile)
  {
    try
    {
    	sendToServer(assFile);
    }
    catch(IOException e)
    {
    	e.printStackTrace();
      clientUI.display
        ("Could not send message to server.  Terminating client.");
      quit();
    }
  }
  
  
  public void handleMessageFromClientUI_AssignmentFile(AssignmentFile[] assFile)
  {
    try
    {
    	sendToServer(assFile);
    }
    catch(IOException e)
    {
    	e.printStackTrace();
      clientUI.display
        ("Could not send message to server.  Terminating client.");
      quit();
    }
  }
  
  public void handleMessageFromClientUI_Msg(Msg newMsg)
  {
    try
    {
    	sendToServer(newMsg);
    }
    catch(IOException e)
    {
    	e.printStackTrace();
      clientUI.display
        ("Could not send message to server.  Terminating client.");
      //quit();
    }
  }
  
  
  
  /**
   * This method terminates the client.
   */
  public void quit()
  {
    try
    {
      closeConnection();
    }
    catch(IOException e) {}
   
  }

  
  
}
//End of ChatClient class

