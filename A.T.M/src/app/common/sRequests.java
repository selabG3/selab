package app.common;

import javafx.beans.property.SimpleStringProperty;

public class sRequests {
	
	//Authors: Ran Algawi, Max Leshkov
	private SimpleStringProperty  rId;
	private SimpleStringProperty  sId;
	private SimpleStringProperty  cId;
	private SimpleStringProperty  Date;
	private SimpleStringProperty  Answer;
	private SimpleStringProperty  cName;
	private SimpleStringProperty  sFullName;
	private SimpleStringProperty  sClass;

//------------------------Class Constructors------------------------
	
	public sRequests(String rID,String sID,String cID,String Date,String Answer){
		
		this.rId = new SimpleStringProperty(rID);
		this.sId = new SimpleStringProperty(sID);
		this.cId = new SimpleStringProperty(cID);
		this.Date = new SimpleStringProperty(Date);
		this.Answer = new SimpleStringProperty(Answer);
		
		
	}
	
	public sRequests(String sID,String cID,String cName ,String Date){
		this.sId = new SimpleStringProperty(sID);
		this.cId = new SimpleStringProperty(cID);
		this.cName = new SimpleStringProperty(cName);
		this.Date = new SimpleStringProperty(Date);
	}
	
	public sRequests(String rID){
		this.rId = new SimpleStringProperty(rID);
	}
	
//------------------------Class "Get" Methods------------------------

	/**
	 * gets the Request ID
	 * @return rId
	 */
	public SimpleStringProperty getrID(){
		
		return rId;
	}
	
	/**
	 * gets the Student ID
	 * @return sId
	 */
	public SimpleStringProperty getsID(){
		
		return sId;
	}
	
	/**
	 * gets the Date the request was made
	 * @return Date;
	 */
	public SimpleStringProperty getDate(){
		
		return Date;
	}
	
	/**
	 * gets the Answer of the request (Status: 0=Unhandled, 1=Approved, -1=Dissapproved, 2=Handled)
	 * @return Answer
	 */
	public SimpleStringProperty getAnswer(){
		
		return Answer;
	}
	
	/**
	 * gets the Course ID
	 * @return cId
	 */
	public SimpleStringProperty getcID(){
		
		return cId;
	}
	
	/**
	 * gets the Course Name
	 * @return cName
	 */
	public SimpleStringProperty getcName() {
		return cName;
	}

	/**
	 * gets the Students Full Name
	 * @return sFullName
	 */
	public SimpleStringProperty getsFullName() {
		return sFullName;
	}
	
	/**
	 * gets the Students Current Class
	 * @return sClass
	 */
	public SimpleStringProperty getsClass(){
		return sClass;
	}

//------------------------Class "Set" Methods------------------------
	
	/**
	 * sets the Request ID
	 * @param rID
	 */
	public void setrID(String rID){
		this.rId = new SimpleStringProperty(rID);
	}
	
	/**
	 * sets the Student ID
	 * @param sID
	 */
	public void setsID(String sID)
	{
		this.sId = new SimpleStringProperty(sID);
	}
	
	/**
	 * sets the Date the request was made
	 * @param Date
	 */
	public void setDate(String Date){
		this.Date = new SimpleStringProperty(Date);

	}
	
	/**
	 * sets the Answer of the request (Status: 0=Unhandled, 1=Approved, -1=Dissapproved, 2=Handled)
	 * @param Answer
	 */
	public void setAnswer(String Answer){
		this.Answer = new SimpleStringProperty(Answer);

	}
	
	/**
	 * sets the CourseID
	 * @param cID
	 */
	public void setcID(String cID){
		this.cId = new SimpleStringProperty(cID);
	}
	
	/**
	 * sets the Course Name
	 * @param cName
	 */
	public void setcName(String cName){
		this.cName = new SimpleStringProperty(cName);
	}
	
	/**
	 * sets the Students Full Name
	 * @param sFullName
	 */
	public void setsFullName(String sFullName) {
		this.sFullName = new SimpleStringProperty(sFullName);
	}
	
	/**
	 * sets the Students current Class
	 * @param sClass
	 */
	public void setsClass(String sClass){
		this.sClass = new SimpleStringProperty(sClass);
	}
}