package app.common;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class tRequests {

	//Author: Max Leshkov, Ran Algawi
	
	private SimpleIntegerProperty RequestID;
	private SimpleStringProperty TeacherID;
	private SimpleStringProperty CourseID;
	private SimpleStringProperty CourseName;
	private SimpleStringProperty fName;
	private SimpleStringProperty ClassName;
	private SimpleStringProperty ClassID;
	private SimpleStringProperty Class;
	private SimpleStringProperty Date;
	private SimpleIntegerProperty Status = new SimpleIntegerProperty(0);
	private SimpleStringProperty rType;
	
	
//------------------------Class Constructors------------------------
	
	
	public tRequests(String rID,String tID,String cID,String Date, String clID, String ClName, String type){
		
		this.RequestID = new SimpleIntegerProperty(Integer.valueOf(rID));
		this.TeacherID = new SimpleStringProperty(tID);
		this.CourseID = new SimpleStringProperty(cID);
		this.ClassName = new SimpleStringProperty(clID);
		this.ClassID = new SimpleStringProperty(ClName);
		this.Date = new SimpleStringProperty(Date);
		this.rType = new SimpleStringProperty(type);
				
	}
	
	public tRequests(Integer RequestID){
		this.RequestID = new SimpleIntegerProperty(RequestID);
	}
	
	
//------------------------Class "Get" Methods------------------------


	/**
	 * 
	 * @return RequestID
	 */
	public SimpleIntegerProperty getRequestID(){
		return RequestID;
	}

	/**
	 * 
	 * @return TeacherID
	 */
	public SimpleStringProperty getTeacherID(){
		return TeacherID;
	}
		
	/**
	 * 
	 * @return CourseID
	 */
	public SimpleStringProperty getCourseID(){
		return CourseID;
	}
	
	/**
	 * 
	 * @return CourseName
	 */
	public SimpleStringProperty getCourseName(){
		return CourseName;
	}
	
	/**
	 * 
	 * @return tName
	 */
	public SimpleStringProperty getfName(){
		return fName;
	}
	/**
	 * 
	 * @return Date
	 */
	public SimpleStringProperty getDate(){
		return Date;
	}
	
	/**
	 * 
	 * @return Status
	 */
	public SimpleIntegerProperty getStatus(){
		return Status;
	}
	
	/**
	 * 
	 * @return Type
	 */
	public SimpleStringProperty getType(){
		return rType;
	}

	/**
	 * 
	 * @return ClassID
	 */
	public SimpleStringProperty getClassID(){
		return ClassID;
	}

	/**
	 * 
	 * @return ClassName
	 */
	public SimpleStringProperty getClassName(){
		return ClassName;
	}
	
	/**
	 * 
	 * @return Full Class
	 */
	public SimpleStringProperty getFullClass(){
		return Class;
	}
	
//------------------------Class "Set" Methods------------------------

	/**
	 * 
	 * @param RequestID
	 */
	public void setRequestID(Integer RequestID){
		this.RequestID = new SimpleIntegerProperty(RequestID);
	}
	
	/**
	 * 
	 * @param TeacherID
	 */
	public void setTeacherID(String TeacherID){
		this.TeacherID = new SimpleStringProperty(TeacherID);
	}
	
	/**
	 * 
	 * @param CourseID
	 */
	public void setCourseID(String CourseID){
		this.CourseID = new SimpleStringProperty(CourseID);
	}
	
	/**
	 * 
	 * @param CourseName
	 */
	public void setCourseName(String CourseName){
		this.CourseName = new SimpleStringProperty(CourseName);
	}
	
	/**
	 * 
	 * @param fName
	 */
	public void setfName(String fName){
		this.fName = new SimpleStringProperty(fName);
	}
	
	/**
	 * 
	 * @param Date
	 */
	public void setDate(String Date){
		this.Date = new SimpleStringProperty(Date);
	}
	
	/**
	 * 
	 * @param Status
	 */
	public void setStatus(int Status){
		this.Status = new SimpleIntegerProperty(Status);
	}
	
	/**
	 * 
	 * @param rType
	 */
	public void setType(String rType){
		this.rType = new SimpleStringProperty (rType);
	}
	
	/**
	 * 
	 * @param ClassID
	 */
	public void setClassID(String ClassID) {
		this.ClassID = new SimpleStringProperty(ClassID);
	}
	
	/**
	 * 
	 * @param ClassName
	 */
	public void setClassName(String ClassName) {
		this.ClassName = new SimpleStringProperty(ClassName);
	}
	
	/**
	 * 
	 * @param Class
	 */
	public void setFullClass(String Class){
		this.Class = new SimpleStringProperty(Class);
	}
}