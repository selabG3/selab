package app.common.sendMsg;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.Main;
import common.AssignmentFile;
import common.Msg;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class SendMsgGUIcontroller implements Initializable {

	@FXML
	private ComboBox<String> cbSubject;
	@FXML
	private TableColumn<Msg, String> tcMyMsg;
	@FXML
	private TextArea txtMsg;
	@FXML
	private TextField txtFrom;
	@FXML
	private Button btnSendMsg;
	@FXML
	private Button btnSendAss;
	@FXML
	private Button btnGetAss;
	@FXML
	private TableView<Msg> tvMsg;
	@FXML
	private TableColumn<Msg, String> tcAnswer;

	private static ObservableList<Msg> myMsgs_OL;
	private ArrayList<Object[][]> ResultsList;
	private String[] Queries = new String[1];

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		String[] Subjects = { "New Statisics", "New Student", "New Teacher", "New Course", "Other" };
		ObservableList<String> Subjects_OL = FXCollections.observableArrayList(Subjects);
		this.cbSubject.setItems(Subjects_OL);

		myMsgs_OL = FXCollections.observableArrayList();

		tcAnswer.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAnsMsg()));
		tcMyMsg.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getHisMsg()));
		tvMsg.setItems(myMsgs_OL);
	}

	/**
	 * Sends a message to system manager.
	 */
	@FXML
	private void sendNewMsg() {
		if ((txtFrom.getText().length() > 0) && (txtMsg.getText().length() > 0)
				&& (cbSubject.getSelectionModel().getSelectedItem() != null)) {
			Msg newMsg = new Msg();
			newMsg.setFromName(txtFrom.getText());
			newMsg.setFromID(Main.getUserName());
			newMsg.setHisMsg(txtMsg.getText());
			newMsg.setSubject(cbSubject.getSelectionModel().getSelectedItem());
			Queries[0] = "INSERT INTO messages (messages.mText , messages.mStatus , messages.fromUserID , messages.subject ,messages.from) VALUES ('"
					+ newMsg.getHisMsg() + "','" + '0' + "','" + newMsg.getFromID() + "','" + newMsg.getSubject()
					+ "','" + newMsg.getFromName() + "')";

			Main.getDbController().sendMsg(newMsg,this);

		}
	}

	public void sendInsertQuery() {
		Main.getDbController().sendInsertAndUpdate(Queries);
	}

	/**
	 * Gets all the Messages in the database.
	 */
	public void getData() {
		Main.getDbController().getAllMsg(this);
	}

	/**
	 * Shows all messages into the table.
	 */
	public void showMsg() {
		ResultsList = Main.getDbController().getResultsList();
		Msg newMsg = null;
		if (ResultsList != null)
			if (ResultsList.get(0)[0][0] != null) {
				int maxRows = ResultsList.get(0).length;
				for (int index = 0; index < maxRows; index++) {
					newMsg = new Msg();
					newMsg.setmID(ResultsList.get(0)[index][0].toString());
					newMsg.setHisMsg(ResultsList.get(0)[index][1].toString());
					newMsg.setFromID(ResultsList.get(0)[index][3].toString());
					newMsg.setSubject(ResultsList.get(0)[index][5].toString());
					myMsgs_OL.add(newMsg);
				}
			}
	}

	public static void addNewMsg(Msg newMsg) {
		if (myMsgs_OL != null)
			myMsgs_OL.add(newMsg);
	}

	/**
	 * This is an example of how to sends an assignment with a file. Student and
	 * Teacher can use this. IMPORTENT! You must fill the fields in the
	 * constructor: 
	 * 1)File name. 
	 * 2)UserName. 
	 * 3)SemesterID. 
	 * 4)CourseID. 
	 * 5)Upload
	 * Date. 
	 * 6)Due Date.
	 * 7)Assignment Name. 
	 * 8)If it is a student ->2 || If it is
	 * a teacher ->3.
	 */
	@FXML
	private void sendQuery() {

		// Creates a file chooser.
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Open File");
		File file = chooser.showOpenDialog(new Stage());
		// Create example of AssignmentFile.
		AssignmentFile assFile = new AssignmentFile(file.getName(), "21", "202", "00003", "2017-06-29", "2017-06-29",
				"Ass1", 3);
		assFile.setAssNum("50");
		assFile.setAssGrade("45");
		assFile.setStudentID("1");
		assFile.setSetGrade(true);
		// This is how we will send the file. NO need to create a new method.
		Main.getDbController().sendFile(assFile, file);

	}

	/**
	 * This is an example getting assignment.
	 */
	@FXML
	public void getAss() {
		Main.getDbController().getFiles(this);
	}

	/**
	 * This is an example how to build any assignment.
	 */
	public void buildData() {
		try {
			ArrayList<Object[][]> ResultsList = Main.getDbController().getResultsList();
			// Creates file chooser.
			FileChooser chooser = new FileChooser();
			chooser.setTitle("Save File");
			// sets file extension filter . This extension stored in the
			// database.
			FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
					ResultsList.get(0)[0][7].toString() + " File (*." + ResultsList.get(0)[0][7].toString() + ")",
					"*." + ResultsList.get(0)[0][7].toString() + "");
			// Adding the filter.
			chooser.getExtensionFilters().add(extFilter);
			// Starts the saving stage.
			File dest = chooser.showSaveDialog(new Stage());
			// If the user canceled.
			if (dest != null) {
				FileOutputStream fout;
				fout = new FileOutputStream(dest);
				// Converting the array of bytes into the file.
				BufferedOutputStream bout = new BufferedOutputStream(fout);
				byte b[] = ((byte[]) ResultsList.get(0)[0][8]);
				bout.write(b);
				bout.flush();
				bout.close();
				fout.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
