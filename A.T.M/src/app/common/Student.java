package app.common;

import java.util.ArrayList;

import javafx.beans.property.SimpleStringProperty;

public class Student extends User {

	/**
	 * All the courses the student passed.
	 */
	private ArrayList<Course> Courses = new ArrayList<Course>();
	private ArrayList<Class> StudentClasses = new ArrayList<Class>();
	private ArrayList<Semester> prevSemesers = new ArrayList<Semester>();
	
	public Student(){
		super();
	}
	
	public Student(String  ID) {
		super(ID);
	}
	
	/**
	 * 
	 */
	public String toString(){
		return super.toString();
	}
	
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return super.getLastName();
	}
	
	/**
	 * 
	 */
	public SimpleStringProperty getLastNameProperty(){
		return super.getLastNameProperty();
	}
	
	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		super.setLastName(lastName);
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return super.getFirstName();
	}

	/**
	 * 
	 */
	public SimpleStringProperty getFirstNameProperty(){
		return super.getFirstNameProperty();
	}
	
	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		super.setFirstName(firstName);
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Course> getCourses() {
		return Courses;
	}
	
	/**
	 * 
	 * @param prevCourses
	 */
	public void setCourses(ArrayList<Course> Courses) {
		this.Courses = Courses;
	}
	
	
	/**
	 * 
	 * @param course
	 */
	public void addCourse(Course course){
		if(!Courses.contains(course))
			Courses.add(course);
	}

	/**
	 * 
	 * @param studentClass
	 */
	public void addClass(Class studentClass) {
		if(!StudentClasses.contains(studentClass))
			StudentClasses.add(studentClass);
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Class> getStudentClasses() {
		return StudentClasses;
	}

	/**
	 * 
	 * @param studentClasses
	 */
	public void setStudentClasses(ArrayList<Class> studentClasses) {
		StudentClasses = studentClasses;
	}

	public ArrayList<Semester> getPrevSemesers() {
		return prevSemesers;
	}

	public void setPrevSemesers(ArrayList<Semester> prevSemesers) {
		this.prevSemesers = prevSemesers;
	}
}
