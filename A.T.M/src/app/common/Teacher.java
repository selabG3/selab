package app.common;

import java.util.ArrayList;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Teacher extends User {

	private ArrayList<Class> TeachingClasses = new ArrayList<Class>();
	private ArrayList<Course> TeachingCourses = new ArrayList<Course>();
	private ArrayList<Assignment> Assignments = new ArrayList<Assignment>();
	private ArrayList<Dept> Depts = new ArrayList<Dept>();
	private int WeeklyTeachingHours = 0;
	private int MaxWeeklyTeachingHours = 20;

	public Teacher(){
		super();
	}
	
	public Teacher(String ID){
		super(ID);
	}

	/**
	 * 
	 */
	public String toString(){
		return super.toString();
	}
	
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return super.getLastName();
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		super.setLastName(lastName);
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return super.getFirstName();
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		super.setFirstName(firstName);
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Dept> getDepts() {
		return Depts;
	}
	
	/**
	 * 
	 * @param depts
	 */
	public void setDepts(ArrayList<Dept> depts) {
		Depts = depts;
	}
	
	/**
	 * 
	 * @param newDept
	 */
	public void addDept(Dept newDept){
		if(!checkDepts(newDept))
			this.Depts.add(newDept);
	}
	
	/**
	 * 
	 * @param dept
	 * @return
	 */
	public boolean checkDepts(Dept dept){
		for(Dept d : Depts){
			if(d.getDeptID().compareTo(dept.getDeptID())==0){
				return true;
			}
		}
		return false;		
	}
	
	/**
	 * 
	 * @param deptID
	 * @return
	 */
	public boolean checkDepts(String deptID){
		for(Dept d : Depts){
			if(d.getDeptID().compareTo(deptID)==0){
				return true;
			}
		}
		return false;		
	}
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<Course> getTeacheringCourses() {
		return this.TeachingCourses;
	}
	
	/**
	 * 
	 * @param courses
	 */
	public void setCourses(ArrayList<Course> courses) {
		this.TeachingCourses = courses;
	}
	
	/**
	 * 
	 * @param newCourse
	 */
	public void addCourse(Course newCourse){
		if(!checkCourse(newCourse))
			this.TeachingCourses.add(newCourse);
	}
	
	/**
	 * 
	 * @param course
	 * @return
	 */
	public boolean checkCourse(Course course){
		for(Course c : TeachingCourses){
			if(c.getCourseID().compareTo(course.getCourseID())==0){
				return true;
			}
		}
		return false;		
	}
	
	/**
	 * 
	 * @param courseID
	 * @return
	 */
	public boolean checkCourse(String courseID){
		for(Course c : TeachingCourses){
			if(c.getCourseID().compareTo(courseID)==0){
				return true;
			}
		}
		return false;		
	}
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<Class> getTeacheringClasses() {
		return this.TeachingClasses;
	}
	
	/**
	 * 
	 * @param classes
	 */
	public void setClasses(ArrayList<Class> classes) {
		this.TeachingClasses = classes;
	}
	
	/**
	 * 
	 * @param newClass
	 */
	public void addClass(Class newClass){
		if(!checkClass(newClass))
			this.TeachingClasses.add(newClass);
	}
	
	/**
	 * 
	 * @param newClass
	 * @return
	 */
	public boolean checkClass(Class newClass){
		for(Class c : TeachingClasses){
			if(c.getClassName().compareTo(newClass.getClassName())==0)
				if(c.getClassNumber().compareTo(newClass.getClassNumber())==0)
					return true;	
		}
		return false;		
	}
	
	/**
	 * 
	 * @param classID
	 * @param className
	 * @return
	 */
	public boolean checkClass(String classID,String className){
		for(Class c : TeachingClasses){
			if(c.toString().compareTo(classID+" "+className)==0)
				return true;
		}
		return false;		
	}
	
	/**
	 * 
	 * @param dept
	 * @return
	 */
	public boolean checkExists(Dept dept){
		if(Depts.contains(dept))
			return true;
		return false;
	}
	
	/**
	 * 
	 * @param dept
	 */
	public void addNewDept(Dept dept){
		if(!checkExists(dept))
			Depts.add(dept);
	}

	/**
	 * Return theacher's max weekly teaching hours.
	 * Default is 20.
	 * @return
	 */
	public SimpleIntegerProperty getWeeklyTeachingHoursProperty(){
		return new SimpleIntegerProperty(WeeklyTeachingHours);
	}
	
	/**
	 * Return teacher's weekly teaching hours.
	 * @return
	 */
	public int getWeeklyTeachingHours() {
		return WeeklyTeachingHours;
	}
	
	/**
	 * Return TRUE if there are any more teaching hours left for the teacher.
	 * @return
	 */
	public boolean checkAvailableTeachingHours(){
		if (MaxWeeklyTeachingHours>WeeklyTeachingHours)
			return true;
		return false;
	}
	
	/**
	 * Return the available teaching hours that left to the teacher.
	 * @return
	 */
	public int getAvailableTeachingHours(){
		return (MaxWeeklyTeachingHours-WeeklyTeachingHours);
	}

	/**
	 * 
	 * @param weeklyTeachingHours
	 */
	public void setWeeklyTeachingHours(int weeklyTeachingHours) {
		WeeklyTeachingHours = weeklyTeachingHours;
	}

	/**
	 * 
	 * @return
	 */
	public SimpleIntegerProperty getMaxWeeklyTeachingHoursProperty(){
		return new SimpleIntegerProperty(MaxWeeklyTeachingHours);
	}
	
	/**
	 * 
	 * @return
	 */
	public int getMaxWeeklyTeachingHours() {
		return MaxWeeklyTeachingHours;
	}

	/**
	 * 
	 * @param maxWeeklyTeachingHours
	 */
	public void setMaxWeeklyTeachingHours(int maxWeeklyTeachingHours) {
		MaxWeeklyTeachingHours = maxWeeklyTeachingHours;
	}
	
	/**
	 * 
	 * @param weeklyHours
	 * @return TRUE is successfully to add teaching hours.
	 * FALSE otherwise.
	 */
	public boolean addWeeklyTeachingHours(int weeklyHours){
		if(getAvailableTeachingHours()>=weeklyHours){
			this.WeeklyTeachingHours+=weeklyHours;
			return true;
		}
		else return false;
	}
	
	/**
	 * Removes if it can be done, the weeklyHours from WeeklyTeachingHours.
	 * @param weeklyHours
	 */
	public void removeWeeklyTeachingHours(int weeklyHours){
		if(this.WeeklyTeachingHours>=weeklyHours)
			this.WeeklyTeachingHours-=weeklyHours;
	}
	
	/**
	 * Return TRUE if the teacher is teaching the dept,
	 * else FALSE.
	 * @param DeptID
	 * @return
	 */
	public boolean isTeachingDept(String DeptID){
		for(Dept dept : Depts)
			if(dept.getDeptID().compareTo(DeptID)==0)
				return true;
		return false;
	}

}
