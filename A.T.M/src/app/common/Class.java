package app.common;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

public class Class {
	
	private SimpleStringProperty ClassName;
	private SimpleStringProperty ClassNumber;
	private ArrayList<Student> StudentsList = new ArrayList<Student>();
	private ArrayList<Course> CoursesList = new ArrayList<Course>();
	private ArrayList<Teacher> TeachersList = new ArrayList<Teacher>();
	
	//******CONSTRACTURS
	
	public Class(){
		
	}
	
	public Class(String ClassID){
		this.ClassNumber = new SimpleStringProperty(ClassID);
	}
	
	public Class(String ClassID,String ClassName){
		this.ClassNumber = new SimpleStringProperty(ClassID);
		this.ClassName = new SimpleStringProperty(ClassName);
	}

	//******CLASS METHODS
	
	/**
	 * 
	 */
	public String toString(){
		return ClassName.get()+" "+ClassNumber.get();
	}
	
	
	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getFullClassNameProperty(){
		return new SimpleStringProperty(ClassName.get()+" "+ClassNumber.get());
	}
	
	/**
	 * 
	 * @return
	 */
	public String getFullClassName(){
		return (ClassName.get()+" "+ClassNumber.get());
	}
	
	/**
	 * 
	 * @return
	 */
	public String getClassName() {
		return ClassName.get();
	}
	
	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getClassNameProperty(){
		return ClassName;
	}

	/**
	 * 
	 * @param ClassName
	 */
	public void setClassName(String ClassName) {
		this.ClassName = new SimpleStringProperty(ClassName);
	}

	/**
	 * 
	 * @return
	 */
	public String getClassNumber() {
		return ClassNumber.get();
	}
	
	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getClassNumberProperty(){
		return ClassNumber;
	}

	/**
	 * 
	 * @param ClassNumber
	 */
	public void setClassNumber(String ClassNumber) {
		this.ClassNumber = new SimpleStringProperty(ClassNumber);
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Student> getStudentList() {
		return StudentsList;
	}

	/**
	 * 
	 * @param Student
	 */
	public void addStudent(Student Student) {
		if(!StudentsList.contains(Student))
			this.StudentsList.add(Student);
	}
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<Teacher> getTeachersList() {
		return TeachersList;
	}

	/**
	 * 
	 * @param Teacher
	 */
	public void addTeacher(Teacher Teacher) {
		if(!TeachersList.contains(Teacher))
			this.TeachersList.add(Teacher);
	}
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<Course> getCoursetList() {
		return CoursesList;
	}

	/**
	 * 
	 * @param Course
	 */
	public void addCourse(Course Course) {
		this.CoursesList.add(Course);
	}
	
	/**
	 * Returns the Course instance
	 * @param existCourse
	 * @return
	 */
	public Course getCourse(Course existCourse){
		if(CoursesList.contains(existCourse))
			return CoursesList.get(CoursesList.indexOf(existCourse));
		return null;
	}
}
