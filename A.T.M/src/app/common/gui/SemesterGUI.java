package app.common.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import app.Main;
import app.common.Course;
import app.common.Semester;
import app.common.Student;
import app.users.parent.semesters.semester.ParentCourseGUIcontroller;
import app.users.student.semesters.semester.StudentCourseGUIcontroller;
import app.users.teacher.semesters.semester.TeacherCourseGUIcontroller;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Pagination;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class SemesterGUI implements ISemester {


	//******************CLASS VARIABLES******************

	private Pagination pagination;
	private double NewHeight;
	private double NewWidth;
	private final int ItemHight = 150;
	private final int ItemWidth = 200;
	private int TotalItemsPerPage = 0;
	private int RowsPerGrid = 0;
	private int ColsPerGrid = 0;
	private int TotalPages = 0;
	private int TotalItems = 0;
	private ArrayList<Course> CourseList;
	private TabPane MainTabPane;
	private HashMap <String,Tab> myCoursesTabs = new HashMap<String,Tab>();
	private int Type = Main.getDbController().getType();
	private Semester semester;
	private Student SelectedChild;
	
	//******************PUBLIC CLASS METHODS******************

	public void setCourseList(ArrayList<Course> CourseList){
		this.CourseList = CourseList;
	}
	
	public void setPagination(Pagination pagination){
		this.pagination = pagination;
	}
	
	public void buildAllCoursesForSemester(){
		setTotalItems();
		setItemsPerPage();
		setColsPerGrid();
		setRowsPerGrid();
		setTotalPages();
		createPagination();
	}
	
	//******************PRIVATE CLASS METHODS******************
	
	
	private void createPagination() {
		System.out.println("Total Page: " + TotalPages + " Total Item Per Page: " + TotalItemsPerPage);
		this.pagination.setPageCount(TotalPages);
		this.pagination.setCurrentPageIndex(0);
		this.pagination.getStyleClass().add(Pagination.STYLE_CLASS_BULLET);
		this.pagination.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
		this.pagination.setPageFactory((Integer pageIndex) -> createPages(pageIndex));
	}

	private GridPane createPages(int pageIndex) {

		GridPane page = new GridPane();
		page.setHgap(10);
		page.setVgap(10);
		page.setPadding(new Insets(10, 10, 10, 10));

		int index = TotalItemsPerPage * pageIndex;

		for (int row = 0; row < RowsPerGrid; row++) {
			for (int col = 0; col < ColsPerGrid; col++) {

				if (index == (TotalItems))
					break;
				StackPane container = new StackPane();
				container.setOnMouseEntered(new EventHandler<Event>() {

					@Override
					public void handle(Event event) {
						container.setStyle("-fx-background-color:lightgreen");
					}
				});

				container.setOnMouseExited(new EventHandler<Event>() {

					@Override
					public void handle(Event event) {
						container.setStyle("-fx-background-color:lightgrey");

					}
				});

				container.setStyle("-fx-background-color:lightgrey");
				container.setPrefSize(ItemWidth, ItemHight);
				container.setMaxSize(ItemWidth, ItemHight);

				VBox element = new VBox(5);
				element.setAlignment(Pos.TOP_CENTER);
				//Set the hyper link by the name of the course.
				Hyperlink link = new Hyperlink(CourseList.get(index).getCourseName());
				link.setFont(new Font(20));
				link.setAlignment(Pos.TOP_CENTER);
				final int i = index;
				link.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						try {
							setCourseDetails(i);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				});

				element.getChildren().add(link);
				
				/*
				Label label = new Label();
				label.setAlignment(Pos.CENTER_RIGHT);
				label.setFont(new Font(10));			
				element.getChildren().add(label);
				*/
				
				container.getChildren().add(element);
				GridPane.setRowIndex(container, row);
				GridPane.setColumnIndex(container, col);
				GridPane.setHgrow(container, Priority.NEVER);
				GridPane.setVgrow(container, Priority.NEVER);
				page.getChildren().add(container);
				index++;
			}
		}

		return page;
	}

	private void setCourseDetails(int index) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		BorderPane CoursetItems = null;
		
		if (Type==1){//For Parent
			CoursetItems = loader.load(Main.class.getResource("users/parent/semesters/semester/ParentCourseGUI.fxml").openStream());
			ParentCourseGUIcontroller parentCourseGUIcontroller = (ParentCourseGUIcontroller) loader.getController();
			parentCourseGUIcontroller.setMainTabPane(MainTabPane);
			parentCourseGUIcontroller.setCourse(CourseList.get(index));
			parentCourseGUIcontroller.setSelectedChild(SelectedChild);
			parentCourseGUIcontroller.buildCourseDetails();
			Tab newTab = new Tab(CourseList.get(index).getCourseName());
			newTab.setContent(CoursetItems);
			myCoursesTabs.put(CourseList.get(index).getCourseID(),newTab);
			MainTabPane.getTabs().add(newTab);
			MainTabPane.getSelectionModel().select(MainTabPane.getTabs().indexOf(myCoursesTabs.get(0)));
		}
		else if (Type==2){//For Student
	
		}

		else if(Type==3){//For Teacher
			CoursetItems = loader.load(Main.class.getResource("users/teacher/semesters/semester/TeacherCourseGUI.fxml").openStream());
			TeacherCourseGUIcontroller teacherCourseGUIcontroller = (TeacherCourseGUIcontroller) loader.getController();
			teacherCourseGUIcontroller.setMainTabPane(MainTabPane);
			teacherCourseGUIcontroller.setCourse(CourseList.get(index));
			Tab newTab = new Tab(CourseList.get(index).getCourseName());
			newTab.setContent(CoursetItems);
			myCoursesTabs.put(CourseList.get(index).getCourseID(),newTab);
			MainTabPane.getTabs().add(newTab);
			MainTabPane.getSelectionModel().select(MainTabPane.getTabs().indexOf(myCoursesTabs.get(0)));
		}
		
		
		
	}
	


	private void setTotalItems() {
		TotalItems = CourseList.size();
	}

	private void setItemsPerPage() {
		TotalItemsPerPage = (int) Math.round(((NewHeight * NewWidth) / (ItemHight * ItemWidth)));
	}

	private void setColsPerGrid() {
		ColsPerGrid = (int) (Math.round((NewWidth / ItemWidth)));
	}

	private void setRowsPerGrid() {
		RowsPerGrid = (int) (Math.round((NewHeight / ItemHight)));
	}

	private void setTotalPages() {
		TotalPages = (int) Math.ceil((TotalItems / TotalItemsPerPage));
		if (TotalItems > (TotalPages * TotalItemsPerPage)) {
			int addPages = (int) (Math.round((TotalItems - (TotalPages * TotalItemsPerPage)) / TotalItemsPerPage));
			if (addPages == 0)
				addPages++;
			TotalPages += addPages;
		}
	}


	public void setMainTabPane(TabPane mainTabPane) {
		this.MainTabPane = mainTabPane;
		NewHeight = MainTabPane.getHeight();
		NewWidth =  MainTabPane.getWidth();
		
		MainTabPane.heightProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				NewHeight = newValue.doubleValue();
				if(pagination!=null){
					setTotalItems();
					setItemsPerPage();
					setColsPerGrid();
					setRowsPerGrid();
					setTotalPages();
					createPagination();
				}
			}
		});

		MainTabPane.widthProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				NewWidth = newValue.doubleValue();
				if(pagination!=null){
					setTotalItems();
					setItemsPerPage();
					setColsPerGrid();
					setRowsPerGrid();
					setTotalPages();
					createPagination();
				}
			}
		});
		
	}

	public void setStudent(Student SelectedChild) {
		this.SelectedChild = SelectedChild;
	}



}
