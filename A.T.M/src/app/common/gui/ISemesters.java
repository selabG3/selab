package app.common.gui;

import java.util.ArrayList;

import app.common.Semester;
import javafx.scene.control.Pagination;
import javafx.scene.control.TabPane;

public interface ISemesters {

	
	public abstract void buildAllSemestersForSemester();
	
	public abstract void setSemesterList(ArrayList<Semester> SemesterList);
	
	public abstract void setPagination(Pagination pagination);
	
	public abstract void setMainTabPane(TabPane mainTabPane);
	
	
}
