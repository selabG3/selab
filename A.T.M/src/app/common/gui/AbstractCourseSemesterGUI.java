package app.common.gui;

import app.common.Course;
import javafx.scene.control.TabPane;


public abstract class AbstractCourseSemesterGUI {

	public abstract void setMainTabPane(TabPane mainTabPane);

	public abstract void setCourse(Course Course);
}
