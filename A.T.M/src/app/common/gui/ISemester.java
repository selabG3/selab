package app.common.gui;

import javafx.scene.control.TabPane;

public interface ISemester {

	public abstract void setMainTabPane(TabPane mainTabPane);

}
