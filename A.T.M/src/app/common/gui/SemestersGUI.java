package app.common.gui;

import java.io.IOException;
import java.util.ArrayList;

import app.Main;
import app.common.Semester;
import app.common.Student;
import app.users.parent.semesters.semester.ParentSemesterGUIcontroller;
import app.users.secretary.semesters.prev.SecretarySemesterGUIcontroller;
import app.users.teacher.semesters.semester.TeacherSemesterGUIcontroller;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class SemestersGUI implements ISemesters {


	//******************CLASS VARIABLES******************

	private Pagination pagination;
	private double NewHeight;
	private double NewWidth;
	private final int ItemHight = 150;
	private final int ItemWidth = 200;
	private int TotalItemsPerPage = 0;
	private int RowsPerGrid = 0;
	private int ColsPerGrid = 0;
	private int TotalPages = 0;
	private int TotalItems = 0;
	private ArrayList<Semester> SemesterList;
	private TabPane MainTabPane;
	private int Type = Main.getDbController().getType();
	private Student selectedChild;
	
	//******************PUBLIC CLASS METHODS******************

	public void setSemesterList(ArrayList<Semester> SemesterList){
		this.SemesterList = SemesterList;
	}
	
	public void setPagination(Pagination pagination){
		this.pagination = pagination;

	}
	
	public void buildAllSemestersForSemester(){
		setTotalItems();
		setItemsPerPage();
		setColsPerGrid();
		setRowsPerGrid();
		setTotalPages();
		createPagination();
	}
	
	//******************PRIVATE CLASS METHODS******************
	
	
	private void createPagination() {
		System.out.println("Total Page: " + TotalPages + " Total Item Per Page: " + TotalItemsPerPage);
		this.pagination.setPageCount(TotalPages);
		this.pagination.setCurrentPageIndex(0);
		this.pagination.getStyleClass().add(Pagination.STYLE_CLASS_BULLET);
		this.pagination.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
		this.pagination.setPageFactory((Integer pageIndex) -> createPages(pageIndex));
	}

	private GridPane createPages(int pageIndex) {

		GridPane page = new GridPane();
		page.setHgap(10);
		page.setVgap(10);
		page.setPadding(new Insets(10, 10, 10, 10));

		int index = TotalItemsPerPage * pageIndex;

		for (int row = 0; row < RowsPerGrid; row++) {
			for (int col = 0; col < ColsPerGrid; col++) {

				if (index == (TotalItems))
					break;
				StackPane container = new StackPane();
				container.setOnMouseEntered(new EventHandler<Event>() {

					@Override
					public void handle(Event event) {
						container.setStyle("-fx-background-color:lightgreen");
					}
				});

				container.setOnMouseExited(new EventHandler<Event>() {

					@Override
					public void handle(Event event) {
						container.setStyle("-fx-background-color:lightgrey");

					}
				});

				container.setStyle("-fx-background-color:lightgrey");
				container.setPrefSize(ItemWidth, ItemHight);
				container.setMaxSize(ItemWidth, ItemHight);

				VBox element = new VBox(5);
				element.setAlignment(Pos.TOP_CENTER);
				//Set the hyper link by the name of the course.
				Hyperlink link = new Hyperlink(SemesterList.get(index).getSemesterName());
				link.setFont(new Font(20));
				link.setAlignment(Pos.TOP_CENTER);
				final int i = index;
				link.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						try {
							boolean flag  = false;
							for(Tab tab : MainTabPane.getTabs()){
								if((tab.getId()!=null)&&(tab.getId().compareTo(SemesterList.get(i).getSemesterID())==0)){
									flag = true;
									MainTabPane.getSelectionModel().select(tab);
								}
							}
							if(!flag){
								setDetails(i);
							}
						} catch (IOException e) {
							e.printStackTrace();
						}finally {
							link.setVisited(false);
						}
					}
				});

				element.getChildren().add(link);
				
				
				Label startDate = new Label("Started: "+SemesterList.get(index).getStartDate());
				startDate.setAlignment(Pos.CENTER_RIGHT);
				startDate.setFont(new Font(15));		
				element.getChildren().add(startDate);

				Label endDate = new Label("Ended: "+SemesterList.get(index).getEndDate());
				endDate.setAlignment(Pos.CENTER_RIGHT);
				endDate.setFont(new Font(15));			
				element.getChildren().add(endDate);
				
				
				container.getChildren().add(element);
				GridPane.setRowIndex(container, row);
				GridPane.setColumnIndex(container, col);
				GridPane.setHgrow(container, Priority.NEVER);
				GridPane.setVgrow(container, Priority.NEVER);
				page.getChildren().add(container);
				index++;
			}
		}

		return page;
	}

	private void setDetails(int index) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		Parent Item;
		Semester curSemester = SemesterList.get(index);
		
		if (Type==1){//For Parent
			Item = loader.load(Main.class.getResource("users/parent/semesters/semester/ParentSemesterGUI.fxml").openStream());
			ParentSemesterGUIcontroller semesterItem = (ParentSemesterGUIcontroller) loader.getController();
			semesterItem.setMainTabPane(MainTabPane);
			semesterItem.setCourseList(SemesterList.get(index).getCoursesList());
			semesterItem.setStudent(selectedChild);
			semesterItem.setPagination(null);
			semesterItem.buildAllCoursesForSemester();
			semesterItem.show();
			Tab newTab = new Tab(curSemester.getSemesterType());
			newTab.setContent(Item);
			newTab.setId(curSemester.getSemesterID());
			MainTabPane.getTabs().add(newTab);
			MainTabPane.getSelectionModel().select(newTab);
		}
		/*else if (Type==2){//For Student
			Item = loader.load(Main.class.getResource("users/student/semesters/semester/StudentSemesterGUI.fxml").openStream());
			SemesterGUI semesterItem = (StudentSemesterGUIcontroller) loader.getController();
			//semesterItem.setCourseList(SemesterList.get(index).getCoursesList());
			semesterItem.setMainTabPane(MainTabPane);
			semesterItem.buildAllCoursesForSemester();
			Tab newTab = new Tab(curSemester.getSemesterName());
			newTab.setContent(Item);
			newTab.setId(curSemester.getSemesterID());
			MainTabPane.getTabs().add(newTab);
			MainTabPane.getSelectionModel().select(newTab);
		}*/
		else if(Type==3){//For Teacher
			Item = loader.load(Main.class.getResource("users/teacher/semesters/semester/TeacherSemesterGUI.fxml").openStream());
			SemesterGUI semesterItem = (TeacherSemesterGUIcontroller) loader.getController();
			semesterItem.setCourseList(null);
			semesterItem.setMainTabPane(MainTabPane);
			semesterItem.setPagination(null);
			semesterItem.buildAllCoursesForSemester();
			Tab newTab = new Tab(curSemester.getSemesterName());
			newTab.setContent(Item);
			newTab.setId(curSemester.getSemesterID());
			MainTabPane.getTabs().add(newTab);
			MainTabPane.getSelectionModel().select(newTab);
		}
		else if(Type==4){//For Secretary
			Item = loader.load(Main.class.getResource("users/secretary/semesters/prev/SecretarySemesterGUI.fxml").openStream());
			SecretarySemesterGUIcontroller semesterItem = (SecretarySemesterGUIcontroller) loader.getController();
			semesterItem.setMainTabPane(MainTabPane);
			semesterItem.setSemester(curSemester);
			Main.getDbController().getSemesterDetails(curSemester.getSemesterID(),semesterItem);
			Tab newTab = new Tab(curSemester.getSemesterName());
			newTab.setContent(Item);
			newTab.setId(curSemester.getSemesterID());
			MainTabPane.getTabs().add(newTab);
			MainTabPane.getSelectionModel().select(newTab);
		}

	}
	


	private void setTotalItems() {
		TotalItems = SemesterList.size();
	}

	private void setItemsPerPage() {
		TotalItemsPerPage = (int) Math.round(((NewHeight * NewWidth) / (ItemHight * ItemWidth)));
	}

	private void setColsPerGrid() {
		ColsPerGrid = (int) (Math.round((NewWidth / ItemWidth)));
	}

	private void setRowsPerGrid() {
		RowsPerGrid = (int) (Math.round((NewHeight / ItemHight)));
	}

	private void setTotalPages() {
		TotalPages = (int) Math.ceil((TotalItems / TotalItemsPerPage));
		if (TotalItems > (TotalPages * TotalItemsPerPage)) {
			int addPages = (int) (Math.round((TotalItems - (TotalPages * TotalItemsPerPage)) / TotalItemsPerPage));
			if (addPages == 0)
				addPages++;
			TotalPages += addPages;
		}
	}


	public void setMainTabPane(TabPane mainTabPane) {
		this.MainTabPane = mainTabPane;
		NewHeight = MainTabPane.getHeight();
		NewWidth =  MainTabPane.getWidth();
		
		MainTabPane.heightProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				NewHeight = newValue.doubleValue();
				setTotalItems();
				setItemsPerPage();
				setColsPerGrid();
				setRowsPerGrid();
				setTotalPages();
				createPagination();
			}
		});

		MainTabPane.widthProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				NewWidth = newValue.doubleValue();
				setTotalItems();
				setItemsPerPage();
				setColsPerGrid();
				setRowsPerGrid();
				setTotalPages();
				createPagination();
			}
		});
		
	}

	public void setStudent(Student selectedChild) {
		this.selectedChild = selectedChild;
	}

}
