package app.common;

import javafx.beans.property.SimpleStringProperty;

public class Assignment 
{

	private SimpleStringProperty assNum;
	private SimpleStringProperty assName;
	private SimpleStringProperty upDate;
	private SimpleStringProperty dueDate;
	private SimpleStringProperty courseID;
	private SimpleStringProperty semesterID;
	private SimpleStringProperty grade;
	private SimpleStringProperty studentID;

	public Assignment (String assNum,String assName,String upDate,String dueDate)
	{
		this.assNum= new SimpleStringProperty(assNum);
		this.assName=new SimpleStringProperty(assName);
		this.upDate= new SimpleStringProperty(upDate);
		this.dueDate= new SimpleStringProperty(dueDate);
	}
	
	public Assignment() {
	
	}

	public void setAssNum(String string)
	{
		this.assNum=new SimpleStringProperty(string);
	}
	
	public String getAssNum()
	{
		return this.assNum.get();
	}
	
	public SimpleStringProperty getAssNumProperty()
	{
		return this.assNum;
	}
	
	public void setAssName(String assName)
	{
		this.assName=new SimpleStringProperty(assName);
	}
	
	public String getAssName()
	{
		return this.assName.get();
	}
	
	public SimpleStringProperty getAssNameProperty()
	{
		return this.assName;
	}
	
	public void setUpDate(String upDate)
	{
		this.upDate=new SimpleStringProperty(upDate);
	}
	
	public String getUploadDate()
	{
		return this.upDate.get();
	}
	
	public void setDueDate(String dueDate)
	{
		this.dueDate=new SimpleStringProperty(dueDate);
	}
	
	public  String getDueDate()
	{
		return this.dueDate.get();
	}
	
	public void setCourseID(String courseID)
	{
		this.courseID=new SimpleStringProperty(courseID);
	}
	
	public String getCourseID()
	{
		 return courseID.get();
	}

	public String getSemesterID() {
		return semesterID.get();
	}

	public void setSemesterID(String semesterID) {
		this.semesterID = new SimpleStringProperty (semesterID);
	}

	public String getGrade() {
		return grade.get();
	}

	public void setGrade(String grade) {
		this.grade = new SimpleStringProperty (grade);
	}

	public void setStudentID(String studentID) {
		this.studentID = new SimpleStringProperty (studentID);
	}
	
	
	public String getStudentID() {
		return studentID.get();
	}

	public SimpleStringProperty getGradeProperty() {
		return this.grade;
	}

	public SimpleStringProperty getDueDateProperty() {
		return this.dueDate;
	}

	public SimpleStringProperty getUploadDateProperty() {
		return this.upDate;
	}

	public SimpleStringProperty getassNumssimple() {
		return assName;
	}

	public SimpleStringProperty getAssNamesimple() {
	return  this.assName;
	}

	public SimpleStringProperty getdueDatessimple() {
		return this.dueDate;
	}

	public SimpleStringProperty getupDatessimple() {
		return this.upDate;
	}
}

