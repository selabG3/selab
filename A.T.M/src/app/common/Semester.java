package app.common;

import java.util.ArrayList;

import app.comparators.ClassComparator;
import javafx.beans.property.SimpleStringProperty;

public class Semester {

	private SimpleStringProperty SemesterType;
	private SimpleStringProperty SemesterStartDate;
	private SimpleStringProperty SemesterEndDate;
	private SimpleStringProperty SemesterYear;
	private SimpleStringProperty SemesterID;
	private ArrayList<Course> CoursesList = new ArrayList<Course>();
	private ArrayList<Class> ClassesList = new ArrayList<Class>();
	private ArrayList<Teacher> TeachrsList = new ArrayList<Teacher>();
	private ArrayList<Student> StudentsList = new ArrayList<Student>();
	
	//****CONSTRACTURS
	
	
	public Semester(){
		
	}
	
	public Semester(String Type){
		this.SemesterType = new SimpleStringProperty(Type);
	}
	
	public Semester(String id,String type,String sDate,String eDate,String year){
		
		this.SemesterID = new SimpleStringProperty(id);
		this.SemesterType = new SimpleStringProperty(type);
		this.SemesterStartDate = new SimpleStringProperty(sDate);
		this.SemesterEndDate = new SimpleStringProperty(eDate);
		this.SemesterYear = new SimpleStringProperty(year);
	}
	

	
	//*****CLASS METHODS
	


	/**
	 * 
	 */
	public String toString(){
		return SemesterType.get()+" "+SemesterID.get()+" "+SemesterYear.get();
	}
	
	
	/**
	 * 
	 * @return
	 */
	public String getSemesterName() {
		return SemesterType.get() +" "+SemesterYear.get();
	}

	
	/**
	 * 
	 * @return
	 */
	public String getStartDate() {
		return SemesterStartDate.get();
	}

	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getStartDateProperty(){
		return SemesterStartDate;
	}

	/**
	 * 
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		SemesterStartDate = new SimpleStringProperty(startDate);
	}


	/**
	 * 
	 * @return
	 */
	public String getEndDate() {
		return SemesterEndDate.get();
	}
	
	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getEndDateProperty(){
		return SemesterEndDate;
	}


	/**
	 * 
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		SemesterEndDate = new SimpleStringProperty(endDate);
	}


	/**
	 * 
	 * @return
	 */
	public String getSemesterID() {
		return SemesterID.get();
	}

	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getSemesterIdProperty(){
		return SemesterID;
	}
	
	/**
	 * 
	 * @param semesterID
	 */
	public void setSemesterID(String semesterID) {
		SemesterID = new SimpleStringProperty(semesterID);
	}

	/**
	 * 
	 * @param semseterType
	 */
	public void setSemesterType(String semseterType) {
		SemesterType = new SimpleStringProperty(semseterType);
	}
	
	/**
	 * 
	 * @return
	 */
	public String getSemesterType() {
		return SemesterType.get();
	}

	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getSemesterTypeProperty(){
		return SemesterType;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<Course> getCoursesList() {
		return CoursesList;
	}
	
	/**
	 * 
	 * @param coursesList
	 */
	public void setCourseList(ArrayList<Course> coursesList){
		this.CoursesList = coursesList;
	}
	
	/**
	 * 
	 * @param newCourse
	 */
	public void addCourses(Course newCourse) {
		if(!CoursesList.contains(newCourse))
			CoursesList.add(newCourse);
	}
	


	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getYearProperty() {
		return SemesterYear;
	}

	/**
	 * 
	 * @return
	 */
	public String getYear(){
		return SemesterYear.get();
	}
	
	/**
	 * 
	 * @param year
	 */
	public void setYear(String year) {
		SemesterYear = new SimpleStringProperty(year);
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Class> getClassesList() {
		return ClassesList;
	}
	
	/**
	 * 
	 * @param newClass
	 */
	public void addClass(Class newClass) {
		if(!ClassesList.contains(newClass))
			ClassesList.add(newClass);
	}
	
	
	/**
	 * 
	 * @param classesList
	 */
	public void setClassesList(ArrayList<Class> classesList) {
		ClassesList = classesList;
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Teacher> getTeachrsList() {
		return TeachrsList;
	}

	
	/**
	 * 
	 * @param newTeacher
	 */
	public void addTeacher(Teacher newTeacher) {
		if(!TeachrsList.contains(newTeacher))
			TeachrsList.add(newTeacher);
	}
	
	
	/**
	 * 
	 * @param teachrsList
	 */
	public void setTeachrsList(ArrayList<Teacher> teachrsList) {
		TeachrsList = teachrsList;
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Student> getStudentsList() {
		return StudentsList;
	}

	/**
	 * 
	 * @param studentsList
	 */
	public void setStudentsList(ArrayList<Student> studentsList) {
		StudentsList = studentsList;
	}


	/**
	 * 
	 * @param newStudent
	 */
	public void addStudent(Student newStudent) {
		if(!StudentsList.contains(newStudent))
			StudentsList.add(newStudent);
	}
	
	/**
	 * 
	 * @param Course
	 * @return Instance of the course. Null if not exists.
	 */
	public Course getCourse(Course Course){
		for(Course course : CoursesList)
			if(course.getCourseID().compareTo(Course.getCourseID())==0)
				return course;
		return null;
	}
	

	/**
	 * 
	 * @param CourseID
	 * @return Instance of the course. Null if not exists.
	 */
	public Course getCourse(String CourseID){
		for(Course course : CoursesList)
			if(course.getCourseID().compareTo(CourseID)==0)
				return course;
		return null;
	}

	/**
	 * 
	 * @param classNumber
	 * @param className
	 * @return Instance of the class. Null if not exists.
	 */
	public Class getExistClass(String classNumber, String className) {
		for (Class existClass : ClassesList)
			if((existClass.getClassName().compareTo(className)==0)&&(existClass.getClassNumber().compareTo(classNumber)==0))
				return existClass;
		return null;
	}

	/**
	 * 
	 * @param teacherID
	 * @return Instance of the teacher. Null if not exists.
	 */
	public Teacher getTeacher(String teacherID) {
		for(Teacher teacher : TeachrsList)
			if(teacher.getID().compareTo(teacherID)==0)
				return teacher;
		return null;
	}

	/**
	 * 
	 * @param studentID
	 * @return Instance of the student. Null if not exists.
	 */
	public Student getStudent(String studentID) {
		for(Student student : StudentsList)
			if(student.getID().compareTo(studentID)==0)
				return student;
		return null;
	}
	
	



}
