package app.common;

import javafx.beans.property.SimpleStringProperty;

public class User {

	private SimpleStringProperty  ID;
	private SimpleStringProperty  FirstName;
	private SimpleStringProperty  LastName;

	//*****CONSTRACTURS
	
	public User(){
		
	}
	
	public User(String ID){
		this.ID = new SimpleStringProperty (ID);
	}

	public User(String ID,String FirstName , String LastName){
		this.ID = new SimpleStringProperty(ID);
		this.FirstName = new SimpleStringProperty(FirstName);
		this.LastName = new SimpleStringProperty(LastName);
	}
	
	//****CLASS METHODS
	
	/**
	 * 
	 */
	public String toString(){
		return FirstName.get()+" "+LastName.get();
	}
	
	
	/**
	 * @return the iD
	 */
	public String getID() {
		return ID.get();
	}
	
	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getIdProperty(){
		return ID;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return FirstName.get();
	}
	
	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getFirstNameProperty(){
		return FirstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		FirstName = new SimpleStringProperty (firstName);
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return LastName.get();
	}
	
	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getLastNameProperty(){
		return LastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		LastName = new SimpleStringProperty (lastName);
	}

	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getFullNameProperty(){
		return new SimpleStringProperty(FirstName.get()+" "+LastName.get());
	}
	
	/**
	 * 
	 * @return
	 */
	public String getFullName(){
		return (FirstName.get()+" "+LastName.get());
	}
}
