package app.common;

import java.util.ArrayList;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Course {
	
	private SimpleStringProperty CourseID;
	private SimpleStringProperty CourseName;
	private SimpleStringProperty CourseDeptID;
	private SimpleStringProperty CourseDeptName;
	private SimpleIntegerProperty HoursPerWeek;
	private SimpleStringProperty SemesterID;
	private ArrayList<Course> PrevCourses = new ArrayList<Course>();
	private ArrayList<Class> ClassList = new ArrayList<Class>();
	private ArrayList<Assignment> Assignments = new ArrayList<Assignment>();
	private ArrayList<Student> StudentList = new ArrayList<Student>();
	private ArrayList<Teacher> TeacherList = new ArrayList<Teacher>();
	
	//------------------------Class Constructors------------------------
	
	public Course (){
		
	}
	
	public Course (String CourseID){
		this.CourseID = new SimpleStringProperty(CourseID);
	}


	//------------------------Class Methods------------------------
	
	/**
	 * 
	 * @return
	 */
	public String toString(){
		return CourseName.get() + " " + CourseID.get();
	}
	

	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getCourseIDProperty() {
		return CourseID;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getCourseID(){
		return CourseID.get();
	}

	/**
	 * 
	 * @param courseID
	 */
	public void setCourseID(String courseID) {
		this.CourseID = new SimpleStringProperty(courseID);
	}
	
	/**
	 * 
	 * @return
	 */
	public String getCourseName() {
		return CourseName.get();
	}

	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getCourseNameProperty(){
		return CourseName;
	}

	/**
	 * 
	 * @param courseName
	 */
	public void setCourseName(String courseName) {
		this.CourseName = new SimpleStringProperty(courseName);
	}

	
	/**
	 * Return the course dept id property.
	 * @return
	 */
	public SimpleStringProperty getCourseDeptIdProperty() {
		return CourseDeptID;
	}
	
	/**
	 * Return the course dept id.
	 * @return
	 */
	public String getCourseDeptID(){
		return CourseDeptID.get();
	}
	
	/**
	 * 
	 * @param dept
	 */
	public void setCourseDeptID(String deptID) {
		this.CourseDeptID = new SimpleStringProperty(deptID);
	}
	
	/**
	 * Return the course dept name property.
	 * @return
	 */
	public SimpleStringProperty getCourseDeptNameProperty() {
		return CourseDeptName;
	}
	
	/**
	 * Return the course dept name.
	 * @return
	 */
	public String getCourseDeptName(){
		return CourseDeptName.get();
	}
	
	/**
	 * 
	 * @param dept
	 */
	public void setCourseDeptName(String deptName) {
		this.CourseDeptName = new SimpleStringProperty(deptName);
	}
	
	/**
	 * Set the cpurse's teaching hours in a week.
	 * @param maxHoursPerWeek
	 */
	public void setHoursPerWeek(int hoursPerWeek) {
		this.HoursPerWeek = new SimpleIntegerProperty(hoursPerWeek);
	}
	
	/**
	 * 
	 * @param hoursPerWeek
	 */
	public void setHoursPerWeek(String hoursPerWeek) {
		this.HoursPerWeek = new SimpleIntegerProperty((Integer.parseInt(hoursPerWeek)));
	}
	
	/**
	 * Returns the cpurse's teaching hours in a week property. 
	 * @return
	 */
	public SimpleIntegerProperty getHoursPerWeekProperty() {
		return HoursPerWeek;
	}
	
	/**
	 * Returns the cpurse's teaching hours in a week. 
	 * @return
	 */
	public int getHoursPerWeek(){
		return HoursPerWeek.get();
	}
	
	/**
	 * 
	 * @return
	 */
	public String getHoursPerWeekString(){
		return String.valueOf(this.HoursPerWeek.get());
	}

	
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<Class> getClassList() {
		return ClassList;
	}

	/**
	 * 
	 * @param classList
	 */
	public void setNewClassList(ArrayList<Class> classList){
		this.ClassList = classList;
	}
	
	/**
	 * 
	 * @param newClass
	 */
	public void addNewClass(Class newClass) {
		if(!checkClassRegisteration(newClass))
			ClassList.add(newClass);
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public Class getClass(int index){
		return ClassList.get(index);
	}
	
	/**
	 * 
	 * @param Class
	 * @return
	 */
	public boolean checkClassRegisteration(Class Class){
		if(ClassList.contains(Class))
			return true;
		return false;
	}
	
	/**
	 * 
	 * @param Class
	 * @return
	 */
	public boolean removeClass(Class Class){
		if(checkClassRegisteration(Class)){
			ClassList.remove(Class);
			return true;
		}
		return false;
	}



	/**
	 * 
	 * @return
	 */
	public ArrayList<Course> getPrevCoursesList() {
		return PrevCourses;
	}

	/**
	 * 
	 * @param prevCourse
	 */
	public void addNewPrevCourse(Course prevCourse) {
		if(!checkClassRegisteration(prevCourse))
			PrevCourses.add(prevCourse);
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public Course getCourse(int index){
		return PrevCourses.get(index);
	}
	
	/**
	 * 
	 * @param course
	 * @return
	 */
	public boolean checkClassRegisteration(Course course){
		if(PrevCourses.contains(course))
			return true;
		return false;
	}
	
	/**
	 * 
	 * @param course
	 * @return
	 */
	public boolean removePrevCourse(Course course){
		if(checkClassRegisteration(course)){
			PrevCourses.remove(course);
			return true;
		}
		return false;
	}

	public ArrayList<Student> getStudentList() {
		return StudentList;
	}

	public void setStudentList(ArrayList<Student> studentList) {
		StudentList = studentList;
	}

	public ArrayList<Teacher> getTehacerList() {
		return TeacherList;
	}

	public void setTehacerList(ArrayList<Teacher> tehacerList) {
		TeacherList = tehacerList;
	}
	
	public Class getClass(Class existClass){
		if(ClassList.contains(existClass))
			return ClassList.get(ClassList.indexOf(existClass));
		return null;
	}

	public void addTeacher(Teacher Teacher) {
		if(!TeacherList.contains(Teacher))
			this.TeacherList.add(Teacher);
	}

	/**
	 * 
	 * @param Student
	 */
	public void addStudent(Student Student) {
		if(!StudentList.contains(Student))
			this.StudentList.add(Student);
	}

	public ArrayList<Assignment> getAssignments() {
		return Assignments;
	}

	public void setAssignments(ArrayList<Assignment> assignments) {
		Assignments = assignments;
	}

	public SimpleStringProperty getSemesterIDProprty() {
		return SemesterID;
	}
	
	public String getSemesterID() {
		return SemesterID.get();
	}

	public void setSemesterID(String semesterID) {
		SemesterID = new SimpleStringProperty(semesterID) ;
	}
	
	
}
