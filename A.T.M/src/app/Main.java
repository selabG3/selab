package app;

import java.io.IOException;
import java.util.Optional;

import app.dbController.dbController;
import app.server.ChangeServerIPcontroller;
import app.users.view.UsersViewGUIcontroller;
import app.view.BottomPaneController;
import app.view.access.LoginGUIcontroller;
import app.view.access.MainLoginGUIcontroller;
import extra.LoadingScreen;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

@SuppressWarnings("static-access")
public class Main extends Application{
	
	
	//******************CLASS VARIABLES******************
	
	private static dbController DB_CONTROLLER;
	private static Stage primaryStage;
	private static BorderPane mainLayout;
	private static double[] mainSize = new double[2];
	private static LoginGUIcontroller loginGUIcontroller;
	private static MainLoginGUIcontroller mainLoginGUIcontroller;
	private static BottomPaneController bottomPaneController;
	private static String UserName = "";
	private static String Password = "";
	private static boolean active = true;
	private static boolean LoginAlready = false;
	private static String[] Args;
	private final KeyCombination kb = KeyCodeCombination.keyCombination("Ctrl+Shift+A");
	private static String Host;
	private static int Port;
	private static boolean isConnected = false;
	
	//******************MAIN APPLICATION METHOD******************
	
	
	@Override
	public void start(Stage primaryStage) {
				
	
		this.primaryStage = primaryStage;

		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			
			@Override
			public void handle(WindowEvent event) {
				try{
				DB_CONTROLLER.closeConnection();
				}catch(Exception e){}
				finally {
					System.exit(1);
				}
			}
		});
		
		try{
		showMainView();
		showLoginItems();
		showBottomPane();
		}
		catch (IOException e){}
		connectToServer();
		
		mainLayout.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				if(kb.match(event))
					try {
						openChangeHostPortServer();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		});
		
	}

	public static void main(String[] args) {
		Args = args;
		launch(args);
	}
	
	

	
	//******************MAIN STATIC METHODS******************
	
	
	/*
	 * Main interface functions.
	 * Each function shows different view.
	 * Shows the main view, as border panex
	 * 
	 * Main View:
	 * Top:Login view or Logout view - same in every user.
	 * Right:Each user have his own control panel.
	 * Middle:Each user have his own interface - for each user the pane in the same. 
	 */

	
	/**
	 * Shows the main view for the whole program.
	 * Each scene will be in this primary stage.
	 **/
	public static void showMainView() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/MainViewGUI.fxml"));
		mainLayout = loader.load();
		Scene scene = new Scene(mainLayout);
		scene.getStylesheets().add(Main.class.getResource("Main.css").toExternalForm());
		primaryStage.setTitle("High School Info System");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	/**
	 * Shows the bottom panel
	 * Connected to server or not
	 **/
	public static void showBottomPane() throws IOException{
		FXMLLoader loader = new FXMLLoader();
		Parent loginItems = loader.load(Main.class.getResource("view/BottomPane.fxml").openStream());
		bottomPaneController = (BottomPaneController) loader.getController();
		if(DB_CONTROLLER==null)
			bottomPaneController.setNoConnection();
		mainLayout.setBottom(loginItems);
	}
	
	
	/**
	 * Shows (loads) the main Login items
	 * Login button, Symbol, Exit button.
	 * The same for each user.
	 */
	public static void showLoginItems() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		Parent loginItems = loader.load(Main.class.getResource("view/access/LoginGUI.fxml").openStream());
		loginGUIcontroller = (LoginGUIcontroller) loader.getController();
		mainLayout.setCenter(loginItems);
		showTopPaneLoginItems();
	}
	
	
	/**
	 * Shows in the top pane the logout items.
	 * Load the GUI.
	 */
	public static void showTopPaneLogoutItems() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/access/MainLogoutGUI.fxml"));
		Parent mainItemsNoLogin = loader.load();
		mainLayout.setTop(mainItemsNoLogin);
	}
	
	
	/**
	 * Shows in the top pane the login items
	 */
	public static void showTopPaneLoginItems() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		Parent mainLoginItems = loader.load(Main.class.getResource("view/access/MainLoginGUI.fxml").openStream());
		mainLoginGUIcontroller = (MainLoginGUIcontroller) loader.getController();
		if(isConnected)
			mainLoginGUIcontroller.showLogin();
		mainLayout.setTop(mainLoginItems);
	}
	
	
	/**
	 * Try to connect to the server
	 */
	public static void connectToServer(){
		setConnected(false);
		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				Platform.runLater(()->LoadingScreen.showLoadingScreen());
				while(active){
					DB_CONTROLLER = new dbController();
					if (LoginAlready)
					{
						String userLigin = "Username " + UserName+ " Password " +Password;
						DB_CONTROLLER.obtainAccess(userLigin);
					}
				}
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			
			@Override
			public void handle(WorkerStateEvent event) {
				try {
					Platform.runLater(()->LoadingScreen.hideLoadingScreen());
					setConnected(true);
					bottomPaneController.setConnectionEstablish();
				} catch (Exception e) {
				}
				
			}
		});
		
		Thread t = new Thread(task);
		t.start();
	}
	
	/**
	 * Try to connect to the server with new host and port
	 */
	public static void connectToServerNew(){
		setConnected(false);
		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				Platform.runLater(()->LoadingScreen.showLoadingScreen());
				while(active){
					DB_CONTROLLER = new dbController(Host,Port);
					if (LoginAlready)
					{
						Platform.runLater(()->LoadingScreen.hideLoadingScreen());
						String userLigin = "Username " + UserName+ " Password " +Password;
						DB_CONTROLLER.obtainAccess(userLigin);
					}
				}
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			
			@Override
			public void handle(WorkerStateEvent event) {
				try {
					setConnected(true);
					Platform.runLater(()->LoadingScreen.hideLoadingScreen());
					bottomPaneController.setConnectionEstablish();
				} catch (Exception e) {
				}
				
			}
		});		

		Thread t = new Thread(task);
		t.start();
	}

	
	/**
	 * Patent view.
	 */
	public static void showParentItemsView() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		Parent userView = loader.load(Main.class.getResource("users/view/UsersViewGUI.fxml").openStream());
		UsersViewGUIcontroller usersViewController = (UsersViewGUIcontroller) loader.getController();
		usersViewController.setUserType(1);
		mainLayout.setCenter(userView);
		showTopPaneLogoutItems();
	}

	
	/**
	 * Student view.
	 */
	public static void showStudentItemsView() throws IOException{
		FXMLLoader loader = new FXMLLoader();
		Parent userView;
		userView = loader.load(Main.class.getResource("users/view/UsersViewGUI.fxml").openStream());		
		UsersViewGUIcontroller usersViewController = (UsersViewGUIcontroller) loader.getController();
		usersViewController.setUserType(2);
		mainLayout.setCenter(userView);
		showTopPaneLogoutItems();
	}

	
	/**
	 * Teacher view.
	 */
	public static void showTeacherItemsView() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		Parent userView = loader.load(Main.class.getResource("users/view/UsersViewGUI.fxml").openStream());
		UsersViewGUIcontroller usersViewController = (UsersViewGUIcontroller) loader.getController();
		usersViewController.setUserType(3);
		mainLayout.setCenter(userView);
		showTopPaneLogoutItems();
	}
	
	
	/**
	 * Secretary view.
	 */
	public static void showSecretaryItemsView() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		Parent userView = loader.load(Main.class.getResource("users/view/UsersViewGUI.fxml").openStream());
		UsersViewGUIcontroller usersViewController = (UsersViewGUIcontroller) loader.getController();
		usersViewController.setUserType(4);
		mainLayout.setCenter(userView);
		showTopPaneLogoutItems();
	}

	
	/**
	 * Principal view.
	 */
	public static void showPrincipalItemsView() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		Parent userView = loader.load(Main.class.getResource("users/view/UsersViewGUI.fxml").openStream());
		UsersViewGUIcontroller usersViewController = (UsersViewGUIcontroller) loader.getController();
		usersViewController.setUserType(5);
		mainLayout.setCenter(userView);
		showTopPaneLogoutItems();
	}

	
	/**
	 *
	 */
	public static void showUserPane() throws IOException{
		
		switch (DB_CONTROLLER.getType()) {
	case 1:
		if(!LoginAlready)
		{
			LoginAlready = true;
			showParentItemsView();
		}
		break;
	case 2:
		if(!LoginAlready)
		{
			LoginAlready = true;
			showStudentItemsView();
		}
		break;
	case 3:
		if(!LoginAlready)
		{
			LoginAlready = true;
			showTeacherItemsView();
		}
		break;
	case 4:
		if(!LoginAlready)
		{
			LoginAlready = true;
			showSecretaryItemsView();
		}
		break;
	case 5:
		if(!LoginAlready)
		{
			LoginAlready = true;
			showPrincipalItemsView();
		}
		break;
	case 99:
		DB_CONTROLLER.setType(0);
		loginGUIcontroller.showError();
		break;
		}
	}
	
	
	/**
	 *When the user click on key combination this method start.
	 * @throws IOException 
	 */
	private static void openChangeHostPortServer() throws IOException{
		FXMLLoader loader = new FXMLLoader();
		Parent newServer = loader.load(Main.class.getResource("server/ChangeServerIP.fxml").openStream());
		ChangeServerIPcontroller changeServerIPcontroller = (ChangeServerIPcontroller) loader.getController();
		Scene scene = new Scene(newServer);
		Stage stage = new Stage();
		stage.setScene(scene);
		changeServerIPcontroller.setStage(stage);
		stage.setResizable(false);
		stage.show();
	}
	
	
	/*
	 * Helpful functions.
	 * All function are static.
	 * Each controller need access to the main class. 
	 */

	
	//******************SYNCHRONIZED METHODS******************
	
	/**
	 * Change the active status by the value
	 * @param value
	 */
	public static synchronized void setActive(boolean value){
		active = value;
	}
	
	/**
	 * Gets the active status
	 */
	public static synchronized boolean getActive(){
		return active;
	}
	
	/**
	 * 
	 */
	public static synchronized void setDBcontrollerNull(){
		DB_CONTROLLER = null;
	}
	
	
	//******************OTHER METHODS******************
	
	
	public static Stage getPrimaryStage(){
		return primaryStage;
	}


	public static void setCurrentSize() {
		mainSize[0] = mainLayout.getWidth();
		mainSize[1] = mainLayout.getHeight();
	}


	public static double getCurrentWidth() {
		return mainSize[0];
	}


	public static double getCurrentHeight() {
		return mainSize[1];
	}
	
	
	public static void showMainMenu() throws IOException{

		primaryStage.getScene().getWindow().hide();
	}
	
	public static String getPassword() {
		return Password;
	}


	public static void setPassword(String password) {
		Password = password;
	}

	public static String getUserName() {
		return UserName;
	}


	public static void setUserName(String userName) {
		UserName = userName;
	}

	
	public static void clearUserDetails() {
		UserName = null;
		Password = null;
		DB_CONTROLLER.setType(0);//Resting the user type.
	}
	

	public static void fireLoginEventInMian() {
		mainLoginGUIcontroller.fireLoginEvent();
	}


	public static BorderPane getMainLayout() {
		return mainLayout;
	}
	
	public static dbController getDbController(){
		return DB_CONTROLLER;
	}
	
	
	public static BottomPaneController getBottomPaneController(){
		return bottomPaneController;
	}

	public static void setError(){
		loginGUIcontroller.showError();
	}

	public static boolean isLoginAlready() {
		return LoginAlready;
	}

	public static void setLoginAlready(boolean loginAlready) {
		LoginAlready = loginAlready;
	}
	
	public static String[] getArgs(){
		return Args;
	}
	
	public static boolean isConnected(){
		return isConnected;
	}
	
	public static void setConnected(boolean status){
		isConnected = status;
		if(isConnected)
			mainLoginGUIcontroller.showLogin();
		else
			mainLoginGUIcontroller.disableLogin();
			
	}
	

	public static void setPort(int port) {
		Port = port;
	}

	public static void setHost(String host) {
		Host = host;
	}

}
