/*
 * Handle the Main Login items for the program.
 * The user cannot try to login until filling
 * username and password fields, only then the dbController tries to connect to DB.
 * Each required fields that missing the program will show. 
 */


package app.view.access;

import java.net.URL;
import java.util.ResourceBundle;

import app.Main;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;

public class LoginGUIcontroller implements Initializable {
    
	
    @FXML
    private BorderPane loginMainPane;
    @FXML
    private TextField userNameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Text wrongUserPass;

    /*
     * For each change in password or username text fields it will save it.
     * 
     */
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		wrongUserPass.setVisible(false);
		
	    userNameField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
	        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				Main.setUserName(userNameField.getText());
	            	
	        }
	    });	
	    userNameField.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				if(event.getCode()==KeyCode.ENTER){
					passwordField.requestFocus();
					wrongUserPass.setVisible(false);
					Main.fireLoginEventInMian();
				}
				
			}
		});
		
		
	    passwordField.focusedProperty().addListener(new ChangeListener<Boolean>() {
	        @Override
	        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
	        	Main.setPassword(passwordField.getText());
	        }
	    });
	    passwordField.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
						if(event.getCode()==KeyCode.ENTER){
							userNameField.requestFocus();
							wrongUserPass.setVisible(false);
							Main.fireLoginEventInMian();
						}
			}
		});


	    
	}

	public void showError() {
		try{
		wrongUserPass.setVisible(true);
		}catch(Exception e){e.printStackTrace();}
	}

	
}
