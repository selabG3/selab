package app.view.access;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import app.Main;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class MainLoginGUIcontroller implements Initializable {

	@FXML // fx:id="loginBtn"
	private Button loginBtn; // Value injected by FXMLLoader
	@FXML // fx:id="titleLabel"
	private Label titleLabel; // Value injected by FXMLLoader
	@FXML // fx:id="exitBtn"
	private Button exitBtn; // Value injected by FXMLLoader

	@FXML
	private void login_btn() {
		if (Main.isConnected()) {
			if ((Main.getUserName().length() > 0) || (Main.getPassword().length() > 0)) {
				String userLigin = "Username " + Main.getUserName() + " Password " + Main.getPassword();
				Main.getDbController().obtainAccess(userLigin);
			} else {
				Main.setError();
			}
		}
	}

	@FXML
	private void exit_btn() throws SQLException {
		try {
			Main.getDbController().closeConnection();
		} catch (RuntimeException e) {

		} finally {
			System.exit(1);
		}
	}

	public void fireLoginEvent() {
		login_btn();
	}

	public void showLogin() {
		loginBtn.setDisable(false);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loginBtn.setDisable(true);
	}

	public void disableLogin() {
		loginBtn.setDisable(true);
	}

}
