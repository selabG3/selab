package app.view.access;

import java.io.IOException;
import java.sql.SQLException;

import app.Main;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

@SuppressWarnings("static-access")
public class MainLogoutGUIcontroller {

		
	@FXML
	private Button logoutBtn;
	@FXML
	private Label titleLabel;
	@FXML
	private Button exitBtn;
	
	
	
	@FXML
	private void logoutBtn(){
		try {
			Main.setLoginAlready(false);
			Main.getDbController().closeConnection();
			Main.clearUserDetails();
			Main.showTopPaneLoginItems();
			Main.showMainView();
			Main.showLoginItems();
			Main.showBottomPane();
			Main.getBottomPaneController().setConnectionEstablish();
		} catch (IOException e) {
		}
	}

	@FXML
	private void exitBtn() throws SQLException {

		try {
			Main.getDbController().closeConnection();
		} catch (RuntimeException e) {

		} finally {
    		System.exit(1);
		}

	}

}
