package app.view;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class BottomPaneController {

    @FXML // fx:id="connectionEstablish"
    private Label connectionEstablish; // Value injected by FXMLLoader

    
    public void setConnectionEstablish(){
    	connectionEstablish.setFont(new Font(10.0));
    	connectionEstablish.setTextFill(Color.GREEN);
    	Platform.runLater(()->connectionEstablish.setText("*Connected to the server."));
    }
    
    public void setNoConnection(){
    	connectionEstablish.setTextFill(Color.RED);
    	connectionEstablish.setFont(new Font(10.0));
    	Platform.runLater(()->connectionEstablish.setText("*Not connected to the server."));
    }
}
