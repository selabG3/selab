/*
 * THIS CLASS CONTAINS ALL SQL QUEIRS FOR THE WHOLE PROGRAM.
 * EACH ACTOR,USER AND HIS NEED.
 * EACH QUIEY IS SENDING FROM THIS CLIENT TO THE SERVER.
 * THE SERVER WILL RETURN THE RESULTSET OF EACH QUIERY.
 */

package app.dbController;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import app.Main;
import app.common.Course;
import app.common.Semester;
import app.common.Student;
import app.common.sendMsg.SendMsgGUIcontroller;
import app.users.parent.ParentGUImenuController;
import app.users.parent.semesters.ParentSemestersGUIcontroller;
import app.users.parent.semesters.semester.ParentCourseGUIcontroller;
import app.users.parent.semesters.semester.ParentSemesterGUIcontroller;
import app.users.principal.requests.StudentRequestsGUIcontroller;
import app.users.principal.requests.TeacherRequestsGUIcontroller;
import app.users.principal.statistics.StatisticsGradesClassCoursesGUIcontroller;
import app.users.principal.statistics.StatisticsGradesClassTeachersGUIcontroller;

import app.users.principal.statistics.StatisticsGradesTeacherClassesGUIcontroller;
import app.users.secretary.SecretaryGUImenuController;

import app.users.secretary.classes.ExistClassGUIcontroller;
import app.users.secretary.infoAbout.InfoAboutStudentsGUIcontroller;
import app.users.secretary.infoAbout.InfoAboutTeacherGUIcontroller;
import app.users.secretary.classes.ExistClassGUIcontroller;
import app.users.secretary.classes.OpenNewClassGUIcontroller;
import app.users.secretary.requests.StudentReqGuiController;
import app.users.secretary.requests.TeacherReqGuiController;
import app.users.secretary.semesters.prev.SecretarySemesterGUIcontroller;
import app.users.secretary.semesters.semester.AbstractSecretarySemesterGUI;
import app.users.student.semesters.StudentSemestersGUIcontroller;
import app.users.student.semesters.semester.StudentCourseGUIcontroller;
import app.users.student.semesters.semester.StudentSemesterGUIcontroller;

import app.users.teacher.TeacherGUImenuController;
import common.AssignmentFile;
import common.Msg;
import extra.LoadingScreen;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import serverClient.ClientConsole;

@SuppressWarnings("static-access")
public final class dbController {

	// ******************CLASS VARIABLES******************

	private ClientConsole clientConsole;
	private int uType = 0;
	private ArrayList<Object[][]> ResultsList;
	private boolean active = true;
	private String IP_ADDRESS;
	private boolean Connected = false;
	private LoadingScreen loadingScreen;

	// ****************CLASS CUNSTRACTORS**************************

	/**
	 * Constructor that create an object with default IP and PORT. or from
	 * arguments if there are any. Arguments comes form the CMD.
	 */
	public dbController() {
		try {
			IP_ADDRESS = Main.getArgs()[0];
		} catch (Exception e) {
			IP_ADDRESS = ClientConsole.DEFAULT_SERVER_IP;
		} finally {
			clientConsole = new ClientConsole(IP_ADDRESS, ClientConsole.DEFAULT_PORT);
			Connected = clientConsole.isConnected();
		}
		if (!Connected)
			waitFewSeconds();
	}

	/**
	 * Constructor that build an object with new Host IP and Port. Happens when
	 * the user click the key combination.
	 * 
	 * @param host
	 * @param port
	 */
	public dbController(String host, int port) {
		try {
			clientConsole = new ClientConsole(host, port);
			Connected = clientConsole.isConnected();
		} catch (Exception e) {
		} finally {
			if (!Connected)
				waitFewSeconds();
		}
	}

	// *****************SYNCHRONIZED METHOD*******************

	/**
	 * Waits 2 seconds.
	 */
	private synchronized void waitFewSeconds() {
		try {
			wait(2000);
		} catch (InterruptedException e) {
		}
	}

	/**
	 * Waits until user type changes from 0. Using this method only when trying
	 * to login.
	 */
	public synchronized void waitForServer() {
		while (uType == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Waits for any response from the server side. This methods uses only when
	 * we login into the server with user name and password.
	 */
	public synchronized void serverResponsed() {
		try {
			notifyAll();
		} catch (Exception e) {
		}
	}

	/**
	 * Set the user type.
	 * 
	 * @param User
	 *            Type
	 */
	public synchronized void setType(int type) {
		this.uType = type;
	}

	/**
	 * Set the results
	 * 
	 * @param resultsList
	 */
	public synchronized void setResultsList(ArrayList<Object[][]> ResultsList) {
		this.ResultsList = ResultsList;
	}

	/**
	 * Waits for any response from the server side. This method can use for any
	 * response from the server EXEPT: login.
	 */
	public synchronized void waitForServerRespones() {
		while (active) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
	}

	/**
	 * When the client side gets response from the server this method will
	 * notify for all threads.
	 */
	public synchronized void go() {
		setActiveStatus(false);
		try {
			notifyAll();
		} catch (Exception e) {
		}
	}

	/**
	 * Changes the active status for the method "waitForServerResponse"
	 * 
	 * @param active
	 */
	public synchronized void setActiveStatus(boolean active) {
		this.active = active;
	}

	/**
	 * 
	 * @return
	 */
	public synchronized boolean getActiveStatus() {
		return active;
	}

	/**
	 * Set the status of connection between client side and the server side.
	 * 
	 * @param status
	 */
	public synchronized void setConnected(boolean status) {
		this.Connected = status;
	}

	/**
	 * Return TRUE if the client is connect to the server. Return FALSE if the
	 * client is not connect to the server.
	 * 
	 * @return Connected
	 */
	public synchronized boolean isConnected() {
		return this.Connected;
	}

	// ****************************CLASS METHODS****************************

	/**
	 * 
	 * @return User type.
	 */
	public int getType() {
		return uType;
	}

	/**
	 * Set the object clientConsole to null.
	 */
	public void disconnectedFromServer() {
		clientConsole = null;
	}

	/**
	 * Send to the server a message to close all open communication with this
	 * client.
	 */
	public void closeConnection() {
		clientConsole.test("closeConnection");
	}

	/**
	 * Returning the results from the server.
	 * 
	 * @return the ResultList
	 */
	public ArrayList<Object[][]> getResultsList() {
		return this.ResultsList;
	}

	// ************************SQL QUERIES FOR EVERY USER**********************

	/**
	 * 
	 * @param User
	 *            Login.
	 * @return True if success to login. False if failed to login.
	 */
	public void obtainAccess(String userLogin) {

		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.test(userLogin);
				waitForServer();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				try {
					Platform.runLater(() -> loadingScreen.hideLoadingScreen());
					Main.showUserPane();
				} catch (IOException e) {
				}

			}
		});
		Thread t = new Thread(task);
		t.start();

	}

	/**
	 * Insert and update queries.
	 * 
	 * @param Queries
	 */
	public void sendInsertAndUpdate(ArrayList<String> Queries) {

		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.insertUpdate(Queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
			}
		});

		Thread t = new Thread(task);
		t.start();

	}

	public void sendInsertAndUpdate(String[] Queries, Object GUI) {

		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(Queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());

			}
		});

		Thread t = new Thread(task);
		t.start();

	}

	/**
	 * Select Queries
	 * 
	 * @param Queries
	 * @param GUI
	 */
	public void sendSelect(String[] Queries, Object GUI) {

		ResultsList = null;
		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());

				clientConsole.Queries(Queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				if (GUI instanceof StudentRequestsGUIcontroller) {
					StudentRequestsGUIcontroller tmp = (StudentRequestsGUIcontroller) GUI;

					tmp.getMissingCourses();
				}
			}
		});

		Thread t = new Thread(task);
		t.start();

	}

	public void sendInsertAndUpdate(String[] Queries) {

		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(Queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
			}
		});

		Thread t = new Thread(task);
		t.start();

	}

	public void sendFile(AssignmentFile assFile, File file) {
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());

				// Creating array of bytes.
				byte[] mybytearray = new byte[(int) file.length()];
				// Converting the file into FileInputStream (Array of bytes)
				FileInputStream fis = new FileInputStream(file);
				// Buffer the array of bytes.
				BufferedInputStream bis = new BufferedInputStream(fis);
				// Reading it to the array of bytes.
				bis.read(mybytearray);
				// Saving the array of bytes.
				assFile.setArrayOfBytes(mybytearray);
				// close
				bis.close();
				fis.close();
				// Send the file to the server.
				clientConsole.insertFile(assFile);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	// ************************SQL QUERIES FOR PARENT************************

	// ************************SQL QUERIES FOR STUDENT************************
	public void getPrevSems(int flag, StudentSemestersGUIcontroller studentSemestersGUIcontroller) {

		String[] queryListStr = new String[1];

		queryListStr[0] = "select * from semesters as s where s.SemesterType =" + String.valueOf(flag);

		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(queryListStr);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent e) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				studentSemestersGUIcontroller.buildData();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	public void getInfoAboutCourse(String s, StudentSemestersGUIcontroller studentSemestersGUIcontroller) {

		String[] queryListStr = new String[1];

		queryListStr[0] = "select * from courses as c where c.courseID =" + s;

		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(queryListStr);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent e) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				studentSemestersGUIcontroller.buildCourse();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	public void getCoursesOfSemesterX(String s, StudentSemestersGUIcontroller studentSemestersGUIcontroller) {

		String[] queryListStr = new String[1];

		queryListStr[0] = "select * from course_semester as sc where sc.CouSemSemesterID='" + s + "'";

		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(queryListStr);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent e) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				studentSemestersGUIcontroller.buildData();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	public void getAssInfo(String string, StudentCourseGUIcontroller studentCourseGUIcontroller) {
		String[] queryListStr = new String[1];

		queryListStr[0] = "select * from assignments as a where a.courseID='" + string + "'";

		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(queryListStr);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent e) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				studentCourseGUIcontroller.buildAss();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}
	// ************************SQL QUEIRES FOR TEACHER************************

	public void getCurrentSemesterForTeacher(TeacherGUImenuController teacherGUImenuController) {
		ResultsList = null;
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] queries = {
						"SELECT * FROM teacher_course tc , courses c, semesters s WHERE tc.semID=s.SemesterID AND s.Current='1' AND tc.TCouCourseID=c.courseID AND tc.TCouTeacherID='"
								+ Main.getUserName() + "'",
						"SELECT a.assNum , a.assName , a.assSemID ,a.courseID, a.uploadDate ,a.dueDate FROM assignments a , assignment_teacher ass , semesters s WHERE a.assNum=ass.assTAssID AND a.assSemID=s.SemesterID AND s.Current='0' AND ass.assTTeacherID='"
								+ Main.getUserName() + "'" };
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				teacherGUImenuController.buildData();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	// ************************SQL QUERIES FOR SECRETARY************************

	public void getAllStudentsInClass(ExistClassGUIcontroller s) {

		String[] queries = new String[5];

		// 0 - returns all students which aren't assigned to any class
		// (uID,uFirstName,uLastName)
		queries[0] = "SELECT u.uID, u.uFirstName, u.uLastName FROM users AS u WHERE u.uType = 2 and u.uID not in (SELECT  s1.stdClassStudentID FROM student_class AS s1)";
		// 1 - get current semester ID
		// (semesterID)
		queries[1] = "SELECT s.SemesterID from semesters as s where s.Current=1";
		// 2 - all data about current classes
		// (ClassNum,ClassName,numOfStudents);
		queries[2] = "select * from classes";
		// 3 - all students which are assigned to a class
		// (stdClassClassID,stclassClassName,stdClassStudentID)
		queries[3] = "select * from student_class";
		// 4 - data about all students
		// (uID,uFirstName,uLastName)
		queries[4] = "select * from users as u where u.uType=2";

		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent e) {

				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				s.buildData();
			}
		});

		Thread t = new Thread(task);
		t.start();

	}

	/**
	 * 
	 * requird for creating a new class
	 * 
	 * @param openNewClassGUIcontroller
	 */
	public void getNewClassInfo(OpenNewClassGUIcontroller s) {

		String[] queries = new String[2];

		// 0 - returns all students which aren't assigned to any class
		queries[0] = "SELECT u.uID,u.uFirstName,u.uLastName from users as u where u.uID not in (select s1.stdClassStudentID from student_class as s1)";
		// 1 - get current semester ID
		queries[1] = "SELECT s.SemesterID from semesters as s where s.Current=1";

		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent e) {

				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				s.buildData();
			}
		});

		Thread t = new Thread(task);
		t.start();

	}

	/**
	 * get's all requests for teacher which the principle has approved and extra
	 * data
	 * 
	 * @param teacherReqGuiController
	 */
	public void getTeacherRequests(TeacherReqGuiController s) {

		String[] queries = new String[4];

		queries[0] = "SELECT * from requests_teacher as rt where rt.status=1"; // get
																				// all
																				// requests
		queries[1] = "SELECT * from users as u where u.uType = 3"; // get all
																	// users
																	// whom are
																	// teachers
		queries[2] = "SELECT s.SemesterID from semesters as s where s.Current=1"; // getting
																					// current
																					// semester
																					// ID
		queries[3] = "SELECT * from depts"; // gets all departement data

		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent e) {

				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				s.buildData();
			}
		});

		Thread t = new Thread(task);
		t.start();

	}

	/**
	 * updates the DB with a list of queries used to create new class with
	 * students in it, to add new student to class, to add teacher to course
	 * 
	 * @param queryList
	 * @param s
	 */
	public void updateDBAfterRequests(ArrayList<String> queryList, Object s) {

		int size = queryList.size();
		String[] queryListStr = new String[size];

		for (int i = 0; i < size; i++)
			queryListStr[i] = queryList.get(i);

		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(queryListStr);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent e) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				if (s instanceof StudentReqGuiController) {

					StudentReqGuiController tmp = (StudentReqGuiController) s;
					tmp.clearQueryList();
					tmp.getRequests();
				} else if (s instanceof TeacherReqGuiController) {

					TeacherReqGuiController tmp = (TeacherReqGuiController) s;
					tmp.clearQueryList();
					tmp.getRequests();
				} else if (s instanceof OpenNewClassGUIcontroller) {

					OpenNewClassGUIcontroller tmp = (OpenNewClassGUIcontroller) s;
					tmp.clearQueryList();
					tmp.GetInfo();
				}
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	/**
	 * gets all the necessary data for student requests
	 * 
	 * @param s
	 */
	public void getStudentRequests(StudentReqGuiController s) {

		String[] queries = new String[5];

		queries[0] = "SELECT * from requests_student as rs where rs.status = 1"; // get
																					// all
																					// requests
		queries[1] = "SELECT * from users as u where u.uType = 2"; // get all
																	// users
																	// whom are
																	// students
		queries[2] = "SELECT s.SemesterID from semesters as s where s.Current=1";
		queries[3] = "SELECT * from class_course as cc where cc.clsCrsSemID = (select s.SemesterID from semesters as s where s.Current=1) and cc.amount < 25";
		queries[4] = "SELECT * from classes as c where c.numOfStudents < 25"; // gets
																				// all
																				// information
																				// from
																				// classes
																				// which
																				// have
																				// less
																				// than
																				// 25
																				// students

		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent e) {

				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				s.buildData();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	public void getAllDetailsForNewSemester(AbstractSecretarySemesterGUI abstractSemesterGUI, String[] queries) {
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				abstractSemesterGUI.buildData();
			}
		});

		Thread t = new Thread(task);
		t.start();

	}

	/**
	 * 
	 * @param secretaryGUImenuController
	 */
	public void getPrevSemesters(SecretaryGUImenuController secretaryGUImenuController) {
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] queries = { "SELECT * FROM semesters s WHERE s.Current=0" };
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				try {
					secretaryGUImenuController.buildPrevSemesterTab();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	/**
	 * 
	 * @param semesterID
	 * @param semesterItem
	 */
	public void getSemesterDetails(String semesterID, SecretarySemesterGUIcontroller semesterItem) {
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				/*
				 * 0 - Semester's Courses. 1 - Semester's Classes. 2 -
				 * Semester's Teachers. 3 - Semester's Students. 4 - Semester's
				 * Course's Classes. 5 - Semester's Course's Class's Teacher. 6
				 * - Semester's Course's Class's Students.
				 * 
				 */
				String[] queries = {
						"SELECT c.courseID,c.courseName FROM courses c , course_semester cs WHERE cs.CouSemCourseID=c.courseID AND cs.CouSemSemesterID='"
								+ semesterID + "'",
						"SELECT c.ClassNum , c.ClassName FROM classes c , class_semester cs WHERE cs.classSemClassID=c.ClassNum AND cs.classSemClassName=c.ClassName AND cs.classSemSemID='"
								+ semesterID + "'",
						"SELECT u.uID,u.uFirstName,u.uLastName FROM users u , teacher_semester ts WHERE ts.TSemTeacherID=u.uID AND ts.TSemSemID='"
								+ semesterID + "'",
						"SELECT u.uID,u.uFirstName,u.uLastName FROM users u , student_semester ss WHERE ss.stdID=u.uID AND ss.sID='"
								+ semesterID + "'",
						"SELECT cc.clsCrsCourseID,cc.clsCrsClassID,cc.clsCrsClassName FROM class_course cc WHERE cc.clsCrsSemID='"
								+ semesterID + "'",
						"SELECT tcs.tID , tcs.cID AS ClassID , tcs.cName AS ClassName , tc.TCouCourseID AS CourseID FROM teacher_class_semester tcs , teacher_course tc  WHERE tc.semID='"
								+ semesterID + "'",
						"SELECT ss.stdID , sCourse.stdCouCourseID , sClass.stdClassClassID , sClass.stdClassClassName FROM student_semester ss , student_course sCourse, student_class sClass WHERE ss.sID='"
								+ semesterID + "'" };
				clientConsole.Queries(queries);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());

				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				semesterItem.buildPrevSemesterDetails();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	/**
	 * 
	 * @param infoAboutStudentsGUIcontroller
	 */
	public void getAllStudentDetails(InfoAboutStudentsGUIcontroller infoAboutStudentsGUIcontroller) {
		final Task<Void> task = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] queries = {
						"SELECT u.uID , u.uFirstName , u.uLastName ,sc.stdClassClassID, sc.stdClassClassName FROM users u , student_class sc WHERE sc.stdClassStudentID=u.uID",
						"SELECT sc.stdCouStudentID,sc.stdCouCourseID ,c.courseName FROM student_course sc , semesters s , courses c WHERE s.Current=1 AND c.courseID=sc.stdCouCourseID AND s.SemesterID= sc.stdCouSemesterID",
						"SELECT * FROM courses" };
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				infoAboutStudentsGUIcontroller.buildStudentsDetails();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	/**
	 * 
	 * @param infoAboutStudentsGUIcontroller
	 */
	public void getStudentDetails(InfoAboutStudentsGUIcontroller infoAboutStudentsGUIcontroller, String StudentID) {
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] queries = {
						"SELECT u.uID , u.uFirstName , u.uLastName ,sc.stdClassClassID, sc.stdClassClassName FROM users u , student_class sc WHERE sc.stdClassStudentID=u.uID AND u.uID='"
								+ StudentID + "'",
						"SELECT sc.stdCouStudentID,sc.stdCouCourseID ,c.courseName FROM student_course sc , semesters s , courses c WHERE s.Current=1 AND c.courseID=sc.stdCouCourseID AND s.SemesterID= sc.stdCouSemesterID AND sc.stdCouStudentID='"
								+ StudentID + "'",
						"SELECT * FROM courses" };
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				infoAboutStudentsGUIcontroller.buildStudentsDetails();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	/**
	 * 
	 * @param infoAboutTeacherGUIcontroller
	 */
	public void getAllTeachersDetails(InfoAboutTeacherGUIcontroller infoAboutTeacherGUIcontroller) {
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] queries = {
						"SELECT u.uID , u.uFirstName , u.uLastName ,tcs.courID ,tcs.cID , tcs.cName FROM users u , teacher_class_semester tcs , semesters s WHERE tcs.tID=u.uID AND s.SemesterID=tcs.sID AND s.Current ='1'",
						"SELECT * FROM courses" };
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				infoAboutTeacherGUIcontroller.buildTeachersDetails();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	/**
	 * 
	 * @param infoAboutTeacherGUIcontroller
	 * @param teacherID
	 */
	public void getTeacherDetails(InfoAboutTeacherGUIcontroller infoAboutTeacherGUIcontroller, String teacherID) {
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] queries = {
						"SELECT u.uID , u.uFirstName , u.uLastName ,tcs.courID ,tcs.cID , tcs.cName FROM users u , teacher_class_semester tcs , semesters s WHERE tcs.tID=u.uID AND s.SemesterID=tcs.sID AND s.Current ='1' AND tcs.tID='"
								+ teacherID + "'",
						"SELECT * FROM courses" };
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				infoAboutTeacherGUIcontroller.buildTeachersDetails();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	public void getAllMsg(SendMsgGUIcontroller sendMsgGUIcontroller) {
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				String[] Quesries = { "" };
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(Quesries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				sendMsgGUIcontroller.showMsg();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	public void getAssDetails(SendMsgGUIcontroller sendMsgGUIcontroller) {
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				String[] Quesries = { "SELECT * FROM assignments, assignment_student" };
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(Quesries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				sendMsgGUIcontroller.buildData();
				;
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	public void sendMsg(Msg newMsg, SendMsgGUIcontroller sendMsgGUIcontroller) {
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				clientConsole.sendMsg(newMsg);
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				sendMsgGUIcontroller.sendInsertQuery();
			}
		});
		Thread t = new Thread(task);
		t.start();
	}

	public void getFiles(SendMsgGUIcontroller sendMsgGUIcontroller) {
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				String[] Quesries = { "SELECT * FROM assignments" };
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(Quesries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				sendMsgGUIcontroller.buildData();
			}
		});

		Thread t = new Thread(task);
		t.start();

	}

	// ******************SQL QUERIES FOR PRINCIPAL******************

	/**
	 * Ask for Teacher Requests
	 * 
	 * @param teacherRequestsGUIcontroller
	 */
	public void getTeacherRequestsP(TeacherRequestsGUIcontroller controller) {

		ResultsList = null;
		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] TCRReq = {
						// 0 - Select Requests from ALL types
						"SELECT" + " 	RT.rID AS RequestID, " // RequestID
								+ " 	RT.rType AS RequestType, " // Type
								+ " 	RT.reqTuID AS TeacherID, " // TeacherID
								+ " 	RT.reqTWantCourseID AS CourseID, " // CourseID
								+ "		C.courseName AS CourseName, " // CourseName
								+ " 	concat(RT.reqTClassName,RT.reqTClassID) AS Class, " // ClassName
																							// +
																							// ClassNum
								+ " 	RT.date AS RequestDate, " // Date
								+ "		CONCAT(U.uFirstName,' ',U.uLastName) AS fName " // FirstName
																						// +
																						// LastName
																						// as
																						// FullName
								+ "FROM" + " 	requests_teacher AS RT, " + "		users AS U, "
								+ "		courses AS C " + "WHERE" + "	 	RT.status = 0 AND "
								+ "		RT.reqTWantCourseID = C.courseID AND " + "	 	RT.reqTuID = U.uID " };
				clientConsole.Queries(TCRReq);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				controller.buildData();

			}
		});
		Thread t = new Thread(task);
		t.start();
	}

	/**
	 * Ask for all Student Requests
	 * 
	 * @param studentRequestsGUIcontroller
	 */
	public void getStudentRequestsP(StudentRequestsGUIcontroller controller) {

		ResultsList = null;
		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] STDReq = {
						// 0 - Select ALL Student Requests
						"SELECT" + "		RS.rID AS RequestID, " // RequestID
								+ "		RS.reqStduID AS StudentID, " // StudentID
								+ "		C.courseName AS Course, " // Wanted
																	// Course
																	// Name
								+ "		C.courseID AS CourseID, " // Wanted
																	// CourseID
								+ "		RS.date AS RequestDate, " // Date
								+ "		CONCAT(U.uFirstName,' ',U.uLastName) AS fName, " // FirstName
																							// +
																							// LastName
																							// as
																							// FullName
								+ "		CONCAT(stdClassClassName, stdClassClassID) AS Class " // ClassName
																								// +
																								// ClassID
																								// as
																								// Class
								+ "FROM" + "		requests_student AS RS, " + "		users AS U, "
								+ "		courses AS C, " + "		student_class AS SC " + "WHERE"
								+ "		RS.reqStduID = U.uID AND " + "		RS.reqStdWantCourseID = C.courseID AND "
								+ " 	RS.status = 0 AND " + "		RS.reqStduID = SC.stdClassStudentID " };

				clientConsole.Queries(STDReq);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				controller.buildData();
			}
		});
		Thread t = new Thread(task);
		t.start();
	}

	public void getClassCoursesStats(
			StatisticsGradesClassCoursesGUIcontroller statisticsGradesClassCoursesGUIcontroller) {

		ResultsList = null;
		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] CLSCouSts = {
						// 0 - Select All years
						"SELECT DISTINCT" + " 	s.Year AS Year " // Year
								+ "	FROM" + " 	semesters AS s",
						// 1 - Select All semesters
						"SELECT" + "		s.SemesterID AS SemID, " // SemesterID
								+ "		s.SemesterType AS Type, " // SemesterType
								+ "		s.Year AS Year " // Year
								+ "FROM" + " 	semesters AS s ",

						// 0 - Select All years
						"SELECT DISTINCT" + " 	s.Year AS Year " // Year
								+ "	FROM" + " 	semesters AS s",
						// 1 - Select All semesters
						"SELECT" + "		s.SemesterID AS SemID, " // SemesterID
								+ "		s.SemesterType AS Type, " // SemesterType
								+ "		s.Year AS Year " // Year
								+ "FROM" + " 	semesters AS s ",
						// 2 - Select All Classes
						"SELECT" + " 	cs.classSemClassID AS ClassID, " // ClassID
								+ "		cs.classSemClassName AS ClassName, " // ClassName
								+ "		cs.classSemSemID AS SemesterID " // SemesterID
								+ "FROM" + " 	class_semester AS cs" };

				clientConsole.Queries(CLSCouSts);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				statisticsGradesClassCoursesGUIcontroller.buildYears();
			}
		});
		Thread t = new Thread(task);
		t.start();
	}

	public void getClassTeachersStats(
			StatisticsGradesClassTeachersGUIcontroller statisticsGradesClassTeachersGUIcontroller) {

		ResultsList = null;
		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] CLSTSts = new String[2];
				// 0 - Select All years
				CLSTSts[0] = "SELECT DISTINCT" + " 		s.Year AS Year " // Year
						+ "		FROM" + " 		semesters AS s";
				CLSTSts[1] = "SELECT" + "			s.SemesterID AS SemID, " // SemesterID
						+ "			s.SemesterType AS Type, " // SemesterType
						+ "			s.Year AS Year " // Year
						+ "		FROM" + " 		semesters AS s";

				/*
				 * Platform.runLater(() -> loadingScreen.showLoadingScreen());
				 * String[] CLSTSts = new String[5]; // 0 - Select All years
				 * CLSTSts[0] = "SELECT DISTINCT" + " 		s.Year AS Year " //
				 * Year + "		FROM" + " 		semesters AS s"; CLSTSts[1] =
				 * "SELECT" + "			s.SemesterID AS SemID, " // SemesterID +
				 * "			s.SemesterType AS Type, " // SemesterType +
				 * "			s.Year AS Year " // Year + "		FROM" +
				 * " 		semesters AS s"; CLSTSts[2] = "SELECT" +
				 * " 		cs.classSemClassID AS ClassID, " // ClassID +
				 * "			cs.classSemClassName AS ClassName, " //
				 * ClassName + "			cs.classSemSemID AS SemesterID " //
				 * SemesterID + "		FROM" + " 		class_semester AS CS";
				 * CLSTSts[3] = "";
				 */
				clientConsole.Queries(CLSTSts);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				statisticsGradesClassTeachersGUIcontroller.buildYears();
			}
		});
		Thread t = new Thread(task);
		t.start();
	}

	public void getTeacherClassesStats(
			StatisticsGradesTeacherClassesGUIcontroller statisticsGradesTeacherClassesGUIcontroller) {

		ResultsList = null;
		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] CLSTSts = new String[2];
				// 0 - Select All years
				CLSTSts[0] = "SELECT DISTINCT" + " 		s.Year AS Year " // Year
						+ "		FROM" + " 		semesters AS s";
				// 1 - Select Years, Semesters
				CLSTSts[1] = "SELECT" + "			s.SemesterID AS SemID, " // SemesterID
						+ "			s.SemesterType AS Type, " // SemesterType
						+ "			s.Year AS Year " // Year
						+ "		FROM" + " 		semesters AS s";

				clientConsole.Queries(CLSTSts);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				statisticsGradesTeacherClassesGUIcontroller.buildYears();
			}

		});
		Thread t = new Thread(task);
		t.start();
	}

	public void sendSelect(String[] Queries, Object GUI, int flag) {

		ResultsList = null;
		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(Queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				if (GUI instanceof StatisticsGradesClassCoursesGUIcontroller) {
					StatisticsGradesClassCoursesGUIcontroller tmp = (StatisticsGradesClassCoursesGUIcontroller) GUI;
					if (flag == 1)
						tmp.buildSemesters();
					if (flag == 2)
						tmp.buildClassesNames();
					if (flag == 3)
						tmp.buildNums();
					if (flag == 4) {
						if (flag == 4) {
							tmp.getDataForGraph();
							tmp.setStudentsInClassNum();
							tmp.getDataForChart();
							tmp.buildBarChart();
							// tmp.
						}
					}
				}

				if (GUI instanceof StatisticsGradesClassTeachersGUIcontroller) {
					StatisticsGradesClassTeachersGUIcontroller tmp1 = (StatisticsGradesClassTeachersGUIcontroller) GUI;
					if (flag == 1)
						tmp1.buildSemesters();
					if (flag == 2)
						tmp1.buildClassesNames();
					if (flag == 3)
						tmp1.buildNums();
					if (flag == 4) {
						tmp1.setStudentsInClassNum();
						tmp1.getDataForChart();
						tmp1.buildBarChart();
					}
				}

				if (GUI instanceof StatisticsGradesTeacherClassesGUIcontroller) {
					StatisticsGradesTeacherClassesGUIcontroller tmp2 = (StatisticsGradesTeacherClassesGUIcontroller) GUI;
					if (flag == 1)
						tmp2.buildSemesters();
					if (flag == 2)
						tmp2.buildTeachersNames();
					if (flag == 3) {
						tmp2.getDataForChart();
						tmp2.buildBarChart();
					}
				}

			}
		});
		Thread t = new Thread(task);
		t.start();

	}

	public void getParentChildrens(ParentSemesterGUIcontroller parentSemesterGUIcontroller) {
		ResultsList = null;
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] queries = {
						"SELECT * FROM parents p , parent_student ps , users u WHERE p.parentID = ps.parStdParentID AND u.uID=ps.parStdStudentID" };
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				parentSemesterGUIcontroller.buildChildren();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	public void getChildCourses(ParentSemesterGUIcontroller parentSemesterGUIcontroller, String StudentID) {
		ResultsList = null;
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] queries = {
						"SELECT sc.stdCouStudentID,sc.stdCouCourseID ,c.courseName FROM student_course sc , semesters s , courses c WHERE s.Current=1 AND c.courseID=sc.stdCouCourseID AND s.SemesterID= sc.stdCouSemesterID AND sc.stdCouStudentID='"
								+ StudentID + "'" };
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				parentSemesterGUIcontroller.buildChildrenCourses();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	public void getCoursesOfSemsterX(String semesterID, StudentSemestersGUIcontroller s) {

		String[] Queries = new String[1];

		// get all previous semesters
		Queries[0] = "select cs.CouSemCourseID from course_semester as cs where cs.CouSemSemesterID in (select s.SemesterID from semesters as s where s.Current = 0)";

		final Task<Void> task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				clientConsole.Queries(Queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent e) {

				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				s.buildDataDrop();
			}
		});

		Thread t = new Thread(task);
		t.start();

	}

	public void getParentChildrens(ParentSemestersGUIcontroller parentSemestersGUIcontroller) {
		ResultsList = null;
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] queries = {
						"SELECT * FROM parents p , parent_student ps , users u WHERE p.parentID = ps.parStdParentID AND u.uID=ps.parStdStudentID" };
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				parentSemestersGUIcontroller.buildChildren();
			}
		});

		Thread t = new Thread(task);
		t.start();

	}

	public void getAllSemesterCourseForChild(ParentSemestersGUIcontroller parentSemestersGUIcontroller,
			String studentID) {
		ResultsList = null;
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] queries = {
						"SELECT * FROM student_course sc , courses c , semesters s WHERE s.Current='0' AND s.SemesterID=sc.stdCouSemesterID AND c.courseID=sc.stdCouCourseID AND sc.stdCouStudentID='"
								+ studentID + "'",
						"SELECT a.assNum , a.assName , a.assSemID ,a.courseID ,ass.uploadDate,a.dueDate , ass.grade , ass.assStdStudentID FROM assignments a, assignment_student ass , semesters s WHERE a.assSemID=s.SemesterID AND s.Current='0' AND a.assNum=ass.assStdAssID AND ass.assStdStudentID='"
								+ studentID + "'" };
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				parentSemestersGUIcontroller.buildChildrenSemesterCourse();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}

	public void getAllPrevSemesterForTeacher(TeacherGUImenuController teacherGUImenuController) {
		ResultsList = null;
		final Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				setActiveStatus(true);
				Platform.runLater(() -> loadingScreen.showLoadingScreen());
				String[] queries = {
						"SELECT * FROM semesters s , teacher_course tc WHERE s.Current='0' AND s.SemesterID=tc.semID AND tc.TCouTeacherID='"
								+ Main.getUserName() + "'",
						"SELECT a.assNum , a.assName ,a.assSemID ,a.courseID , a.uploadDate ,a.dueDate , ass.assTTeacherID FROM assignments a , assignment_teacher ass , semesters s WHERE s.Current='0' AND a.assNum=ass.assTAssID AND a.assSemID=s.SemesterID AND ass.assTTeacherID='"
								+ Main.getUserName() + "'" };
				clientConsole.Queries(queries);
				waitForServerRespones();
				return null;
			}
		};

		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Platform.runLater(() -> loadingScreen.hideLoadingScreen());
				teacherGUImenuController.buildPrevSemesters();
			}
		});

		Thread t = new Thread(task);
		t.start();
	}
}
