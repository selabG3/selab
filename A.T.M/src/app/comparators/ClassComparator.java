package app.comparators;

import java.util.Comparator;
import app.common.Class;

public class ClassComparator implements Comparator<Class>{

	@Override
	public int compare(Class o1, Class o2) {
		if(o1.getClassNumber().compareTo(o2.getClassNumber())==1)
			return 1;
		if(o1.getClassNumber().compareTo(o2.getClassNumber())==-1)
			return -1;
		if(o1.getClassNumber().compareTo(o2.getClassNumber())==0)
			if(o1.getClassName().compareTo(o2.getClassName())==1)
				return 1;
		return -1;

	}
}
