package app.comparators;

import java.util.Comparator;

import app.common.Course;

/**
 * Constructor gets a boolean.
 * True --> Sort by Course name.
 * False --> Sort by Course ID.
 * @author Adi Dinner
 *
 */
public class CourseComparator implements Comparator<Course> {
	
	private boolean byName;
	
	public CourseComparator(boolean status){
		this.byName = status;
	}
	
	@Override
	public int compare(Course o1, Course o2) {
		if(byName)
		return o1.getCourseName().compareTo(o2.getCourseName());
		else
			return o1.getCourseID().compareTo(o2.getCourseID());
	}

}
