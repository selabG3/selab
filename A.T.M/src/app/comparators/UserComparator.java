package app.comparators;

import java.util.Comparator;

import app.common.User;

/**
 * Constructor get an integer.
 * 1 --> Sort by User ID.
 * 2 --> Sort by User Last name.
 * 3 --> Sort by User First name.
 * 
 * @author Adi Dinner.
 * @version 22/06/2017
 */
public class UserComparator implements Comparator<User> {
	
	private int by;
	
	public UserComparator(int status){
		this.by = status;
	}
	
	@Override
	public int compare(User o1, User o2) {
		switch (by){
		case 1:
			return o1.getID().compareTo(o2.getID());
		case 2:
			return o1.getLastName().compareTo(o2.getLastName());
		case 3:
			return o1.getFirstName().compareTo(o2.getFirstName());
		}
		return 3;
	}

}
