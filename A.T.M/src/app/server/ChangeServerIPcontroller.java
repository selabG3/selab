package app.server;


import java.net.URL;
import java.util.ResourceBundle;

import app.Main;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ChangeServerIPcontroller implements Initializable{

    @FXML
    private Button btnCancel;
    @FXML
    private Button btnOk;
    @FXML
    private TextField txtServerIP;
    @FXML
    private TextField txtServerPort;

    
    private Stage stage;
    
    
    @FXML
    private void changeServer(){
    	if((!txtServerIP.getText().isEmpty())&&(!txtServerPort.getText().isEmpty())){
    		Main.setHost(txtServerIP.getText());
    		Main.setPort(Integer.parseInt(txtServerPort.getText()));
    		
			if(Main.getDbController().isConnected())
				Main.getDbController().closeConnection();
			Main.getDbController().disconnectedFromServer();
			Main.getBottomPaneController().setNoConnection();
			if(Main.getActive())
				Main.setActive(false);
			Main.setActive(true);
			Main.connectToServerNew();	
    		stage.close();
    	}
    }
    
    @FXML
    private void cancelChnage(){
    	stage.close();
    }



	@Override
	public void initialize(URL location, ResourceBundle resources) {
		txtServerPort.setOnInputMethodTextChanged(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				if(txtServerPort.getText().contains("[a-z][A-Z]"))
					txtServerPort.setText("");
			}
		});
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}
}
