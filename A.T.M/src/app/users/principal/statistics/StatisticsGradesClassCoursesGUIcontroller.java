package app.users.principal.statistics;

import java.util.ArrayList;

import app.Main;
import app.common.Semester;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.text.Text;

public class StatisticsGradesClassCoursesGUIcontroller {

	/**
	 * FXML
	 */
    @FXML
    private Text StudentNumText;

    @FXML
    private Button clearButton;
    @FXML
    private ComboBox<String> SemesterComboBox;

    @FXML
    private ComboBox<String> ClassComboBox;

    @FXML
    private ComboBox<String> YearComboBox;

    @FXML
    private ComboBox<String> NumComboBox;

    @FXML
    private Text titleText;

    //FXML for BarChart 
    @FXML
    private BarChart<String, Number> barChart;
    @FXML
    private CategoryAxis xAxis;
    
    
    /**
     * All ObservableLists
     */                                           
    private ObservableList<String> years_OL;      
    private ObservableList<String> semesters_OL;  
    private ObservableList<String> classNames_OL; 
    private ObservableList<String> classNums_OL;  
    private ObservableList<String> courses_OL;    


    /**
     * ArrayLists
     */
    private ArrayList<Object[][]> ResultsList;    
    
    /**
     * Selected Variables that Save the Year,SemesterID, and Class that was selected
     */
    private String SelectedYear;
    private String SelectedSemesterID;
    private String SelectedClassName;
    private String SelectedClassNum;
    
    
    /**
     * General Variebles
     */
    private String year;		//string to save the year selection
    private int ClassCtr;		//Counter for amount of Class Names
    private int ClassNumCtr;	//Counter for amount of Class Nums
    private int NumOfCourses;
    private int gradesNum;    
    /**
     * Arrays
     */
    private String[] Classes, ClassesNum;		//Arrays of Classes=ClassName, ClassNum
    private Semester[] Semesters = new Semester[2];  
    private String[] amountOfStudents;
    private String[] courses;
    private int[] sumOfGrades;
    private double[] averages;

    
    public void getData(){
    	Main.getDbController().getClassCoursesStats(this);
    	years_OL = FXCollections.observableArrayList();
    	semesters_OL = FXCollections.observableArrayList();
    	classNames_OL = FXCollections.observableArrayList();
    	classNums_OL = FXCollections.observableArrayList();
    	courses_OL = FXCollections.observableArrayList();
    	
    }
    
    public void buildYears(){
    	ResultsList = Main.getDbController().getResultsList();
    	if(ResultsList.get(0)[0][0] == null)
    		return;
    	int size = ResultsList.get(0).length;
    	
    	for (int i=0; i < size; i++){
    		year = (ResultsList.get(0)[i][0].toString());
    		years_OL.add(year);
    	}
    	
    	YearComboBox.setItems(years_OL);
    }
    
    public void buildSemesters(){
    	
    	ResultsList = Main.getDbController().getResultsList();
    	
    	if(ResultsList.get(0)[0][0] == null)
    		return;
    	
    	semesters_OL.clear();
    	int size = ResultsList.get(0).length;
    	Semester sem;
    	String sType;
    	for (int i=0; i< size; i++){
    		sem = new Semester((String)ResultsList.get(0)[i][1].toString());
    		sem.setSemesterID((ResultsList.get(0)[i][0].toString()));
    		sem.setYear((ResultsList.get(0)[i][2].toString()));
    		sType = sem.getSemesterType();
    		semesters_OL.add(sType);
    		Semesters[i]=sem;
    	}
    	
    	SemesterComboBox.setItems(semesters_OL);
    }
    
    public void buildClassesNames(){
    	
    	ResultsList = Main.getDbController().getResultsList();
    	
    	if(ResultsList.get(0)[0][0] == null)
    		return;
    	
    	classNames_OL.clear();
    	ClassCtr = ResultsList.get(0).length;
    	Classes = new String[ClassCtr];
    	String clsName;
    	for (int i=0; i< ClassCtr; i++){
    		clsName = (String)ResultsList.get(0)[i][0].toString();
    		Classes[i]=clsName;
    		classNames_OL.add(clsName);
    	}
    	
    	ClassComboBox.setItems(classNames_OL);
    }
    
    /**
     * builds the Class Numbers combobox values
     */
    public void buildNums(){
    	
    	ResultsList = Main.getDbController().getResultsList();
    	
    	if(ResultsList.get(0)[0][0] == null)
    		return;
    	
    	classNums_OL.clear();
    	ClassNumCtr = ResultsList.get(0).length;
    	ClassesNum = new String[ClassNumCtr];
    	String clsNum;
    	for(int i=0; i<ClassNumCtr; i++){
    		{
    			clsNum = (String)ResultsList.get(0)[i][0].toString();
    			ClassesNum[i]=clsNum;
    			classNums_OL.add(clsNum);
    		}
    	}
    	
    	NumComboBox.setItems(classNums_OL);
    }
    
    /**
     * Gets all the data required to create the bar chart
     */
    public void getDataForChart(){
    	
    	
    	ResultsList = Main.getDbController().getResultsList();
    	
    	if(ResultsList.get(1)[0][0] == null)	 //1 - Select number of students in each course and course Name 
    		return;
    	if(ResultsList.get(2)[0][0] == null)	 //2 - Select all grades and courses of chosen class in chosen semester
    		return;
    	
    	courses_OL.clear();
    	NumOfCourses = ResultsList.get(1).length;
    	gradesNum = ResultsList.get(2).length;
    	
    	amountOfStudents = new String[NumOfCourses];
    	courses = new String[NumOfCourses];
    	sumOfGrades = new int[NumOfCourses];
    	averages = new double[NumOfCourses];
    	
    	//Loop to get student amounts in each course and course names into arrays and into courses_OL
    	for(int i=0; i<NumOfCourses; i++){
    		amountOfStudents[i] = (String)ResultsList.get(1)[i][0].toString();
    		courses[i] = (String)ResultsList.get(1)[i][1].toString();
    		courses_OL.add(courses[i]);
    	}
    	
   
		xAxis.setCategories(courses_OL); //Sets the courses recieved to the x Axis
    	
    	//Loop to get the grades of every course in an array and to get
    	for(int i=0; i< gradesNum; i++){
    		//Loop to find to which course this grade belongs to
    		for(int j=0; j<NumOfCourses; j++)
    			if(courses[j].compareTo(ResultsList.get(2)[i][1].toString()) == 0){
    				sumOfGrades[j] = sumOfGrades[j] + (int)ResultsList.get(2)[i][0];
    				break;
    			}	
    	}
    	
    	//Loop that calculates all averages for all courses
    	for(int i=0; i<NumOfCourses; i++)
    		calcAvg(i);
    }
    	
    
    /**
     * This Function builds the Bar Chart itself after we got all the necessary
     */
    public void buildBarChart(){
    	
    	if(ResultsList.get(1)[0][0] == null)	 //1 - Select number of students in each course and course Name 
    		return;
    	if(ResultsList.get(2)[0][0] == null)	 //2 - Select all grades and courses of chosen class in chosen semester
    		return;
    	
    	//sets Data
    	XYChart.Series<String, Number> series = new XYChart.Series<>();
    	
    	for (int i=0; i < averages.length; i++)
    		series.getData().add(new XYChart.Data<>(courses_OL.get(i), averages[i]));
    	
    	barChart.getData().add(series);
    }
    
    /**
     * This Function calculates the average in a course
     * @param index - the index in the 3 arrays "sumOfGrades", "amountOfStudents", "averages"
     */
    private void calcAvg(int index){
    	
    	int sum = sumOfGrades[index];
    	int amount = Integer.parseInt(amountOfStudents[index]);
    	double avg = (double) sum / amount;
    	averages[index] = avg;
    	
    }
    
    /**
     * Sends a query to the database to fetch new data.
     * @param query - the query that is sent to fetch new data from the Data Base.
     * @param flag - to determin in what stage it was called.
     */
    private void SelectSomething(String[] query, int flag){
    	Main.getDbController().sendSelect(query, this, flag);
    }
    
    /**
     * Sets the number or students in the chosen class.
     */
    public void setStudentsInClassNum(){
    	ResultsList = Main.getDbController().getResultsList();
    	if(ResultsList.get(0)[0][0] == null){
    		StudentNumText.setText("No Data To Show");
    		return;
    	}
    	StudentNumText.setText(ResultsList.get(0)[0][0].toString());
    }
    
    
    
    
    @FXML
    private void initialize(){
    	
    	clearButton.setOnAction(new EventHandler<ActionEvent>(){
    		
    		@Override
    		public void handle (ActionEvent arg0){
    			
        	StudentNumText.setText("No Data To Show");
    		barChart.getData().clear();
    		years_OL.clear();      
    		semesters_OL.clear();
    		classNames_OL.clear();
    		classNums_OL.clear();
    		courses_OL.clear();
    		
            
			SelectedYear="";        
			SelectedSemesterID="";  
			SelectedClassName="";   
			SelectedClassNum="";    
			
			
			Classes= new String[0] ;
			ClassesNum = new String[0];
			amountOfStudents= new String [0];             
			courses= new String[0];                      
			sumOfGrades=new int[0];                     
			averages=new double[0];	
			getData();
			buildYears();
    		}
    	});
    	
    	//When selecting a Year
    	YearComboBox.setOnAction(new EventHandler<ActionEvent>(){
    		
    		@Override
    		public void handle(ActionEvent arg0){
    			try{
    				SelectedYear = YearComboBox.getSelectionModel().getSelectedItem();
    				String[] query = {
    								  "SELECT"
    								+ "		s.SemesterID AS SemID, " 				 //SemesterID
    								+ "		s.SemesterType AS Type, "				 //SemesterType
    								+ "		s.Year AS Year "						 //Year
    								+ "	FROM"
    								+ " 	semesters AS s "
    								+ "	WHERE"
    								+ "		s.Year = '"+SelectedYear+"' "
    								};
    				SelectSomething(query, 1);
    			}catch (NullPointerException e) {e.printStackTrace();}
    		}
    	});
    	
    	//When Selecting a Semester
    	SemesterComboBox.setOnAction(new EventHandler<ActionEvent>(){
    		
    		@Override
    		public void handle(ActionEvent arg0){
    			try{  
    				String selection = SemesterComboBox.getSelectionModel().getSelectedItem();
    				String tempType;
    					tempType = Semesters[0].getSemesterType();
    					if(tempType.compareTo(selection) == 0)
    						SelectedSemesterID = Semesters[0].getSemesterID();
    					else SelectedSemesterID= Semesters[1].getSemesterID();
    			
    					String[] query = {
    							"SELECT DISTINCT"
    						  + "	cs.classSemClassName AS ClassName "	 //Class Name
    				  		  + "FROM"
    						  + " 	class_semester AS cs "
    						  + "WHERE"
    						  + " 	cs.classSemSemID = ' "+SelectedSemesterID+ " ' "};
    					SelectSomething(query, 2);
    			}catch (NullPointerException e) {e.printStackTrace();}
    		}
    	});
    	
    	//When selecting a Class
    	ClassComboBox.setOnAction(new EventHandler<ActionEvent>() {
    		
    		@Override
    		public void handle(ActionEvent arg0){
    			try{			
    				SelectedClassName  = ClassComboBox.getSelectionModel().getSelectedItem();
    					String[] query = {
    							"SELECT DISTINCT"
    						  + " 	cs.classSemClassID AS ClassNum "	//Class numbers that there are for this specific class
    						  + "FROM"
    						  + " 	class_semester AS cs "
    						  + "WHERE"
    						  + " 	cs.classSemSemID = '"+SelectedSemesterID+"' AND"
    						  + " cs.classSemClassName = '"+SelectedClassName+"'"	};
    					SelectSomething(query, 3);    
    			}catch (NullPointerException e) {e.printStackTrace();};

    		}
    	});
    	
    	//When selecting a Class number
    	NumComboBox.setOnAction(new EventHandler<ActionEvent>() {
    		
    		@Override
    		public void handle(ActionEvent arg0){
    			try{
    				SelectedClassNum = NumComboBox.getSelectionModel().getSelectedItem(); 
    	    		courses_OL.clear();    	
    					String[] query = {
    						  //0 - Select number of students in chosen Class
    							"SELECT"
    						  + " 	c.numOfStudents "		//Number of Students
    						  + "FROM"
    						  + " 	classes AS c "
    						  + "WHERE"
    						  + " 	c.ClassName = '"+SelectedClassName+"' AND"
    						  + " 	c.ClassNum = '"+SelectedClassNum+"'",
    						  
    						  //1 - Select number of students in each course and course Name 
    						    "SELECT"
    						  + " 	cc.amount, "	//Amount of student in a course
    						  + "	c.courseName "	//Course Name
    						  + "FROM"
    						  + " 	class_course AS cc, "
    						  + "	courses AS c "
    						  + "WHERE"
    						  + " 	cc.clsCrsClassName = '"+SelectedClassName+"' AND"
    						  + " 	cc.clsCrsClassID = '"+SelectedClassNum+"' AND"
    						  + " 	cc.clsCrsSemID = '"+SelectedSemesterID+"' AND"
    						  + " 	cc.clsCrsCourseID = c.courseID",
    						  
    						  //2 - Select all grades and courses of chosen class in chosen semester
    						  "SELECT"
    						+ " 	sc.grade, "				//Grade in a specific course
    						+ "		c.courseName "			//Course name
    						+ "FROM"
    						+ " 	student_course AS sc, "
    						+ "		courses AS c, "
    						+ "		student_class AS sCl "
    						+ "WHERE"
    						+ " 	sc.stdCouSemesterID = '"+SelectedSemesterID+"' AND "
    						+ "		sc.stdCouCourseID = c.courseID AND "
    						+ "		sc.stdCouStudentID = sCl.stdClassStudentID AND  "
    						+ "		sCl.stdClassClassID = '"+SelectedClassNum+"' AND "
    						+ "		sCl.stdClassClassName = '"+SelectedClassName+"' AND "
    						+ "		sc.grade>-'1'"
    					};
    					SelectSomething(query, 4);
    			}catch (NullPointerException e) {e.printStackTrace();};
    			
    		}   	
    	});
    	
    	
    }

	public void getDataForGraph() {
		// TODO Auto-generated method stub
		
	}

	}
   
