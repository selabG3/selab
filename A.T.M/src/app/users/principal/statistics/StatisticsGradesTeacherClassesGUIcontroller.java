package app.users.principal.statistics;

import java.util.ArrayList;

import app.Main;
import app.common.Semester;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

public class StatisticsGradesTeacherClassesGUIcontroller {

    @FXML
    private BarChart<String,Number> barChart;

    @FXML
    private CategoryAxis xAxis;

    @FXML
    private ComboBox<String> YearComboBox;

    @FXML
    private ComboBox<String> SemesterComboBox;

    @FXML
    private ComboBox<String> NameComboBox;

    @FXML
    private Button clearButton;
    
    private ArrayList<Object[][]> ResultsList;  
    
    /**
     * All ObservableLists
     */                                           
    private ObservableList<String> years_OL;      
    private ObservableList<String> semesters_OL;  
    private ObservableList<String> classes_OL; 
    private ObservableList<String> teachers_OL;
    
    /**
     * Selected Variables that Save the Year,SemesterID, and Class that was selected
     */
    private String SelectedYear;
    private String SelectedSemesterID;
    private String SelectedTeacher;
    
    /**
     * General Variebles
     */
    private String year;		//string to save the year selection
    private int TeacherCtr;		//Counter for amount of Class Names
    private int NumOfTeachers;
    private int NumOfClasses;
    private int gradesNum;
    
    /**
     * Arrays
     */
    private String[] Teachers;		
    private Semester[] Semesters = new Semester[2];  
    private String[] amountOfStudents;
    private String[] classes;
    private int[] sumOfGrades;
    private double[] averages;
    
    public void getData(){
        	Main.getDbController().getTeacherClassesStats(this);
        	years_OL = FXCollections.observableArrayList();
        	semesters_OL = FXCollections.observableArrayList();
        	classes_OL = FXCollections.observableArrayList();
        	teachers_OL = FXCollections.observableArrayList();    	
    }
    
    public void buildYears(){
    	ResultsList = Main.getDbController().getResultsList();
    	if(ResultsList.get(0)[0][0] == null)
    		return;
    	int size = ResultsList.get(0).length;
    	
    	for (int i=0; i < size; i++){
    		year = (ResultsList.get(0)[i][0].toString());
    		years_OL.add(year);
    	}
    	
    	YearComboBox.setItems(years_OL);
    }
    
		public void buildSemesters(){
	    	
	    	ResultsList = Main.getDbController().getResultsList();
	    	
	    	if(ResultsList.get(0)[0][0] == null)
	    		return;
	    	
	    	semesters_OL.clear();
	    	int size = ResultsList.get(0).length;
	    	Semester sem;
	    	String sType;
	    	for (int i=0; i< size; i++){
	    		sem = new Semester((String)ResultsList.get(0)[i][1].toString());
	    		sem.setSemesterID((ResultsList.get(0)[i][0].toString()));
	    		sem.setYear((ResultsList.get(0)[i][2].toString()));
	    		sType = sem.getSemesterType();
	    		semesters_OL.add(sType);
	    		Semesters[i]=sem;
	    	}
	    	
	    	SemesterComboBox.setItems(semesters_OL);
	    }
		
		  public void buildTeachersNames(){
		    	
		    	ResultsList = Main.getDbController().getResultsList();
		    	
		    	if(ResultsList.get(0)[0][0] == null)
		    		return;
		    	
		    	teachers_OL.clear();
		    	TeacherCtr = ResultsList.get(0).length;
		    	Teachers = new String[TeacherCtr];
		    	String clsName;
		    	for (int i=0; i< TeacherCtr; i++){
		    		clsName = (String)ResultsList.get(0)[i][0].toString();
		    		Teachers[i]=clsName;
		    		teachers_OL.add(clsName);
		    	}
		    	
		    	NameComboBox.setItems(teachers_OL);
		    }
		  
		  public void getDataForChart(){
			  
			  ResultsList = Main.getDbController().getResultsList();
			  
			  if(ResultsList.get(1)[0][0] == null)	 //1 - Select number of students in each course and teacher name 
		    		return;
		    if(ResultsList.get(2)[0][0] == null)	 //2 - Select all grades and courses of chosen class in chosen semester
		    		return;
			  
		    
		    classes_OL.clear();
		    NumOfClasses = ResultsList.get(0).length;
		    gradesNum = ResultsList.get(2).length;
		    
		    for(int i=0; i<NumOfClasses; i++){
	    		amountOfStudents[i] = (String)ResultsList.get(1)[i][0].toString();
	    		classes[i] = (String)ResultsList.get(1)[i][1].toString();
	    		classes_OL.add(classes[i]);
		    }
		    
		    
		    xAxis.setCategories(classes_OL); //Sets the classes recieved to the xAxis
		    
		  //Loop to get the grades of every course in an array and to get
	    	for(int i=0; i< gradesNum; i++){
	    		//Loop to find to which course this grade belongs to
	    		for(int j=0; j<NumOfClasses; j++)
	    			if(classes[j].compareTo(ResultsList.get(2)[i][1].toString()) == 0){
	    				sumOfGrades[j] = sumOfGrades[j] + (int)ResultsList.get(2)[i][0];
	    				break;
	    			}	
	    	}
		    
	    	//Loop that calculates all averages for all courses
	    	for(int i=0; i<NumOfClasses; i++)
	    		calcAvg(i);
		    
		  }
		  
		  
		  public void buildBarChart(){
			  
			//sets Data
		    	XYChart.Series<String, Number> series = new XYChart.Series<String, Number>();
		    	for (int i=0; i < averages.length; i++)
		    		series.getData().add(new XYChart.Data<>(classes_OL.get(i), averages[i]));
		    	
		    	barChart.getData().add(series);
			  
		  }
		  
		  /**
		     * This Function calculates the average in a course
		     * @param index - the index in the 3 arrays "sumOfGrades", "amountOfStudents", "averages"
		     */
		    private void calcAvg(int index){
		    	
		    	int sum = sumOfGrades[index];
		    	int amount = Integer.parseInt(amountOfStudents[index]);
		    	double avg = (double) sum / amount;
		    	averages[index] = avg;
		    }
		
		 /**
	     * Sends a query to the database to fetch new data.
	     * @param query - the query that is sent to fetch new data from the Data Base.
	     * @param flag - to determin in what stage it was called.
	     */
	    private void SelectSomething(String[] query, int flag){
	    	Main.getDbController().sendSelect(query, this, flag);
	    }
		
		@FXML
		private void initialize(){
			
			clearButton.setOnAction(new EventHandler<ActionEvent>(){
	    		
	    		@Override
	    		public void handle (ActionEvent arg0){
	    			
	    		barChart.getData().clear();
	    		years_OL.clear();      
	    		semesters_OL.clear();
	    		teachers_OL.clear();
	    		classes_OL.clear();
	    		
	            
				SelectedYear="";        
				SelectedSemesterID="";  
				SelectedTeacher="";   
				
				
				Teachers= new String[0] ;
				amountOfStudents= new String [0];             
				classes= new String[0];                      
				sumOfGrades=new int[0];                     
				averages=new double[0];
				getData();
				buildYears();
	    		}
	    	});
			
			//When selecting a Year
	    	YearComboBox.setOnAction(new EventHandler<ActionEvent>(){
	    		
	    		@Override
	    		public void handle(ActionEvent arg0){
	    			try{
	    				SelectedYear = YearComboBox.getSelectionModel().getSelectedItem();
	    				String[] query = {
	    								  "SELECT"
	    								+ "		s.SemesterID AS SemID, " 				 //SemesterID
	    								+ "		s.SemesterType AS Type, "				 //SemesterType
	    								+ "		s.Year AS Year "						 //Year
	    								+ "	FROM"
	    								+ " 	semesters AS s "
	    								+ "	WHERE"
	    								+ "		s.Year = '"+SelectedYear+"' "
	    								};
	    				SelectSomething(query, 1);
	    			}catch (NullPointerException e) {e.printStackTrace();}
	    		}
	    	});
	    	
	    	//When Selecting a Semester
	    	SemesterComboBox.setOnAction(new EventHandler<ActionEvent>(){
	    		
	    		@Override
	    		public void handle(ActionEvent arg0){
	    			try{  
	    				String selection = SemesterComboBox.getSelectionModel().getSelectedItem();
	    				String tempType;
	    					tempType = Semesters[0].getSemesterType();
	    					if(tempType.compareTo(selection) == 0)
	    						SelectedSemesterID = Semesters[0].getSemesterID();
	    					else SelectedSemesterID= Semesters[1].getSemesterID();
	    			
	    					String[] query = {
				    						  "SELECT DISTINCT"
				    						 +"   concat(u.uFirstName,' ', u.uLastName) as name "
				    						 +"FROM"
				    						 +"   teacher_class_semester AS tcs, "
				    						 +"   users AS u "
				    						 +"WHERE"
				    						 +"  tcs.sID = '"+SelectedSemesterID+"' AND tcs.tID = u.uID "};
	    					SelectSomething(query, 2);
	    			}catch (NullPointerException e) {e.printStackTrace();}
	    		}
	    	});
	    	
	    	NameComboBox.setOnAction(new EventHandler<ActionEvent>() {
	    		
	    		@Override
	    		public void handle(ActionEvent arg0){
	    			try{
	    				SelectedTeacher = NameComboBox.getSelectionModel().getSelectedItem(); 
	    	    		teachers_OL.clear();    	
	    					String[] query = {
	    						  //0 - Select classes that learn with this teacher
	    							 "SELECT DISTINCT "
									+"	CONCAT(tcs.cName, tcs.cID) AS class "
									+"FROM"
									+"	teacher_class_semester AS tcs, "
									+"	users AS u "
									+"WHERE"
									+" 	tcs.tID = u.uID AND "
									+"	concat(u.uFirstName,' ',u.uLastName) = '"+SelectedTeacher+"'",
	    						  
	    						  //1 - Select number of students in each course and teacher name 
									  "SELECT" 
									 +"	 cc.amount, "
									 + " CONCAT(u.uFirstName,' ',u.uLastName) AS Tname "
								     +"FROM"
								     +"	 class_course AS cc, "
								     +"	 courses AS c, "
								     +"	 teacher_class_semester AS tcs, "
								     +"	 users AS u "
								     +"WHERE"
									 +"	 cc.clsCrsSemID = '1' "
								     +"  AND cc.clsCrsCourseID = c.courseID "
								     +"  AND tcs.courID = cc.clsCrsCourseID "
								     +"  AND u.uID = tcs.tID ",
	    						  
	    						  //2 - Select all grades and courses of chosen class in chosen semester
	    						      "SELECT" 
							         +"	 sc.grade, CONCAT(tcs.cName, tcs.cID) AS CLASS "
							         +"FROM"
							         +"	 student_course AS sc, "
							         +"	 teacher_class_semester AS tcs "
							         +"WHERE"
							         +"	    sc.stdCouSemesterID = '1' "
							         +"	        AND sc.stdCouCourseID = (SELECT " 
							         +"	                                   tcs.courID "
							         +"	                                 FROM "
							         +"	                                   teacher_class_semester AS tcs "
							         +"	                                 WHERE "
							         +"	                                   tcs.tID = (SELECT "
							         +"	                                             	u.uID "
							         +"	                                         	  FROM "
							         +"	                                             	users AS u "
							         +"	                                         	  WHERE "
									 +"                   								CONCAT(uFirstName, ' ', uLastName) = 'Jayshree Dwivedi'))"
	    					};
	    					SelectSomething(query, 3);
	    			}catch (NullPointerException e) {e.printStackTrace();};
	    			
	    		}   	
	    	});
	    	
	    	
		}
	
}
