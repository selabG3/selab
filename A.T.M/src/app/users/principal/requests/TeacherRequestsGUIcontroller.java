package app.users.principal.requests;

import java.util.ArrayList;

import app.Main;
import app.common.tRequests;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;


public class TeacherRequestsGUIcontroller {
	
	//Author: Max Leshkov

	
	/*
	 * FXML
	 */
    @FXML
    private TableColumn<tRequests, String> reqTypeColumn;
    @FXML
    private TableColumn<tRequests, String> courseIDColumn;
    @FXML
    private TableColumn<tRequests, String> teacherIDColumn;
    @FXML
    private TableColumn<tRequests, String> classColumn;
    @FXML
    private TableColumn<tRequests, String> dateColumn;
    @FXML
    private Button btnApprove;
    @FXML
    private Button btnDisapprove;
    @FXML
    private TextField courseField;
    @FXML
    private TextField nameField;
    @FXML
    private ChoiceBox<String> viewTypeBox;
    @FXML
    private TableView<tRequests> teacherTable;
    @FXML
    private Text titleText;
    
    
    
    private ObservableList<String> viewTypeList = FXCollections.observableArrayList("All", "Add", "Remove");
    private ObservableList<tRequests> requests_ALL;
	private ObservableList<tRequests> requests_ADD;
	private ObservableList<tRequests> requests_REM;
	private ArrayList<Object[][]> ResultsList;
	private int typeFlag; //0 -> ALL, 1-> ADD, 2-> REMOVE
    
    /*
     * Gets the Data from the DB
     */
    public void getData(){
    	Main.getDbController().getTeacherRequestsP(this);
    	requests_ALL = FXCollections.observableArrayList();
    	requests_ADD = FXCollections.observableArrayList();
    	requests_REM = FXCollections.observableArrayList();
    }
    
    /**
     * Builds tRequests using the Data that was fetched from the DB via "getData" 
     */
    public void buildData(){
    	ResultsList = Main.getDbController().getResultsList();
    	if(ResultsList.get(0)[0][0] == null)
    		return;
    	int size = ResultsList.get(0).length; // Amount ALL of requests
    	
    	tRequests request;
    	//Loop that generates the List of Teacher Requests with the following Details.

    	for (int j=0; j < size; j++){
    		request = new tRequests((Integer)ResultsList.get(0)[j][0]); 		//Gets RequestID
    		request.setType(ResultsList.get(0)[j][1].toString());				//Gets Request Type (ADD/REMOVE)
    		request.setTeacherID(ResultsList.get(0)[j][2].toString());		//Gets TeacherID
    		request.setCourseID(ResultsList.get(0)[j][3].toString());			//Gets CourseID
    		request.setCourseName(ResultsList.get(0)[j][4].toString());		//Gets CourseName
    		request.setFullClass(ResultsList.get(0)[j][5].toString());		//Gets ClassName + ClassNum
    		request.setDate(ResultsList.get(0)[j][6].toString());				//Gets Date
    		request.setfName(ResultsList.get(0)[j][7].toString());			//Gets FullName
    		requests_ALL.add(request);
    		if(request.getType().get().compareTo("ADD")==0)
    			requests_ADD.add(request);
    		else
    			requests_REM.add(request);
    	}
    	
    	//Put Item in Table
    	teacherTable.setItems(requests_ALL);
    	
    	//set models for all cellData
    	reqTypeColumn.setCellValueFactory(cellData->cellData.getValue().getType());
    	teacherIDColumn.setCellValueFactory(cellData->cellData.getValue().getTeacherID());
    	courseIDColumn.setCellValueFactory(cellData->cellData.getValue().getCourseID());
    	dateColumn.setCellValueFactory(cellData->cellData.getValue().getDate());
    	classColumn.setCellValueFactory(cellData->cellData.getValue().getFullClass());
    			
    	//Sets the Title "Teacher Requests" with the number of new requests
    	setNum(size);	
    }
    
    /**
     * Sets the Title "Teacher Requests" with the current number of requests
     * @param Num - Number of unhandled request
     */
    public void setNum(int Num){
    	titleText.setText("Teacher Requests ("+Num+")");
    }
    
	@FXML
	public void initialize() {
	
		//Initializes the "Request View Type" ChoiceBox
				viewTypeBox.setValue("All");
				typeFlag=0;
		    	viewTypeBox.setItems(viewTypeList);
		    	
    	//Selecting a View Type
    	viewTypeBox.setOnAction(new EventHandler<ActionEvent>() {
    		
    		@Override
    		public void handle(ActionEvent arg0){
    			String selection = viewTypeBox.getSelectionModel().getSelectedItem();
    			
    			//If "Add" Type was selected - loads "ADD" type requests
    			if(selection.compareTo("Add") == 0){
    				teacherTable.setItems(requests_ADD);
    				typeFlag=1;
    				setNum(requests_ADD.size());
    			}
    			
    			//If "Remove" Type was selected - loads "REMOVE" type requests
    			if(selection.compareTo("Remove") == 0){
    				teacherTable.setItems(requests_REM);
    				typeFlag=2;
    				setNum(requests_REM.size());
    			}
    			
    			//If "All" Type was selected - loads ALL requests
    			if(selection.compareTo("All") == 0){
    				teacherTable.setItems(requests_ALL);
    				typeFlag=0;
    				setNum(requests_ALL.size());
    			}
    		}
    	});
    	
    	//Selecting an Item from the Table
    	teacherTable.setOnMousePressed(new EventHandler<Event>() {
    	
    		@Override
    		public void handle(Event arg0) {
    			try {
    				tRequests request = teacherTable.getSelectionModel().getSelectedItem();
    				if (request != null) {
    					String tCourse = request.getCourseName().get();
    					String tName = request.getfName().get();
					
    					// set teacher Full Name Field
    					nameField.setText(tName);
					
    					// set Course Name Field
    					courseField.setText(tCourse);
    				}
    			} catch (NullPointerException e) {e.printStackTrace();}
    		}
    	});
    	
    	//Pressing the Approve button after selecting an item from the table
    	btnApprove.setOnMousePressed(new EventHandler<MouseEvent>() {
    		
    		@Override
    		public void handle(MouseEvent event) {
    			try{
    				tRequests request = teacherTable.getSelectionModel().getSelectedItem();
    				if(request !=null){ 
    					String[] query  = {
    							"UPDATE requests_teacher "
    						  + "SET"
    						  + " 	status = '1' "
    						  + "WHERE"
    						  + " 	rID = ' "+request.getRequestID().get()+" ' "};
    					Main.getDbController().sendInsertAndUpdate(query, this);
    					
    					
    					
    					//if the Request Type is "ADD" - removes it from the ADD List and updates the requests number accordingly
    					if(request.getType().get().compareTo("ADD") == 0)
    						requests_ADD.remove(request);
    					
    					//if the Request Type is "REMOVE" - removes it from the REMOVE List and updates the requests number accordingly
    					if(request.getType().get().compareTo("REMOVE") == 0)
    						requests_REM.remove(request);
    						
    					requests_ALL.remove(request);
    					/*Updating the new Requests count according to the selected view type:
    					0 -> ALL, 1 -> ADD, 2 -> REMOVE*/
    					if(typeFlag==0)
    						setNum(requests_ALL.size());
    					if(typeFlag==1)
    						setNum(requests_ADD.size());
    					if(typeFlag==2)
    						setNum(requests_REM.size());
    				}
    			}
    			catch (NullPointerException e) {e.printStackTrace();}
    		}
    	});
    	
    	//Pressing the Disapprove button after selecting an item from the table
    	btnDisapprove.setOnMousePressed(new EventHandler<MouseEvent>() {
    		
    		@Override
    		public void handle(MouseEvent event) {
    			try{
    				tRequests request = teacherTable.getSelectionModel().getSelectedItem();
    				if(request !=null){
    					String[] query  = {
    							"UPDATE requests_teacher "
    						  + "SET"
    						  + " 	status = '-1' "
    						  + "WHERE"
    						  + " 	rID = ' "+request.getRequestID().get()+" ' "};
    					Main.getDbController().sendInsertAndUpdate(query, this);
    					requests_ALL.remove(request);
    					
    					//if the Request Type is "ADD" - removes it from the ADD List and updates the requests number accordingly
    					if(request.getType().get().compareTo("ADD") == 0)
    						requests_ADD.remove(request);
    					
    					//if the Request Type is "REMOVE" - removes it from the REMOVE List and updates the requests number accordingly
    					if(request.getType().get().compareTo("REMOVE") == 0)
    						requests_REM.remove(request);    					
    					
    					/*Updating the new Requests count according to the selected view type:
    					0 -> ALL, 1 -> ADD, 2 -> REMOVE*/
    					if(typeFlag==0)
    						setNum(requests_ALL.size());
    					if(typeFlag==1)
    						setNum(requests_ADD.size());
    					if(typeFlag==2)
    						setNum(requests_REM.size());
    				}
    			}catch (NullPointerException e) {e.printStackTrace();}
    		}
    	});
	}
}
