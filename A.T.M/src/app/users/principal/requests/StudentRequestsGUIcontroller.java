package app.users.principal.requests;

import java.util.ArrayList;

import app.Main;
import app.common.sRequests;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

public class StudentRequestsGUIcontroller {

	//Author: Max Leshkov
	
	/**
	 * FXML Parameters
	 */
    @FXML
    private TableColumn<sRequests, String> stdIDColumn;
    @FXML
    private TableColumn<sRequests, String> wantedColumn;
    @FXML
    private TableColumn<sRequests, String> dateColumn;
    @FXML
    private ComboBox<String> missingComboBox;
    @FXML
    private TextField classField;
    @FXML
    private TextField nameField;
    @FXML
    private Button btnApprove;
    @FXML
    private Button btnDisapprove;
    @FXML
    private TableView<sRequests> studentTable;
    @FXML
    private Text titleText;
    
    private ObservableList<sRequests> requests_ALL;
    private ObservableList<String> mCourses = FXCollections.observableArrayList("Nothing to Show");
    private ArrayList<Object[][]> ResultsList;
   // private ArrayList<Object[][]> courseList;
    
    /*
     * Gets the Data from the DB
     */
    public void getData(){
    	Main.getDbController().getStudentRequestsP(this);
    	requests_ALL = FXCollections.observableArrayList();
    }
    
    /*
     * builds sRequests using the Data that was fetched from the DB using "getData"
     */
    public void buildData(){
    	ResultsList = Main.getDbController().getResultsList();
    	if(ResultsList.get(0)[0][0] == null)
    		return;
    	int size = ResultsList.get(0).length; // Amount of requests
    	
    	sRequests request;
    	//Loop that generates the List of Student Requests with the following Details.
    	for (int j=0; j < size; j++){
    		request = new sRequests((String)ResultsList.get(0)[j][0].toString()); 		//Gets and Sets RequestID
    		request.setsID(ResultsList.get(0)[j][1].toString());						//Gets and Sets StudentID
    		request.setcName(ResultsList.get(0)[j][2].toString());						//Gets and Sets CourseName
    		request.setcID(ResultsList.get(0)[j][3].toString());						//Gets and Sets CourseID
    		request.setDate(ResultsList.get(0)[j][4].toString());						//Gets and Sets Date
    		request.setsFullName(ResultsList.get(0)[j][5].toString());					//Gets and Sets Students Full Name
    		request.setsClass(ResultsList.get(0)[j][6].toString());						//Gets and Sets ClassName + ClassNum
    		requests_ALL.add(request);
    	}
    	
    	//Put Item in Table
    	studentTable.setItems(requests_ALL);
    	
    	//set models for all cellData
    	stdIDColumn.setCellValueFactory(cellData->cellData.getValue().getsID());
    	wantedColumn.setCellValueFactory(cellData->cellData.getValue().getcName());
    	dateColumn.setCellValueFactory(cellData->cellData.getValue().getDate());
    	setNum(size);
    	missingComboBox.setItems(mCourses);
     }
    
    /**
     * Sets the Title "Student Requests" with current number of new requests
     * @param Num
     */
    public void setNum(int Num){
    	titleText.setText("Student Requests ("+Num+")");
    }
    
    /**
     * Creates the List of Missing courses of a Student
     */
    public void getMissingCourses(){
    	
    	ResultsList = Main.getDbController().getResultsList();
		mCourses.clear();
    	
    	if(ResultsList.get(0)[0][0] == null){
    		missingComboBox.setValue("None");
			return; 
		}
		
		int cAmount = ResultsList.get(0).length;
		missingComboBox.setValue(" ("+ResultsList.get(0).length+")");
		String course;
		for (int i=0; i<cAmount; i++){
			course = ResultsList.get(0)[i][0].toString();
			mCourses.add(course);
		}
    }
    
    /**
     * Sends the Select Query to the Server
     * @param query - the Select query
     */
    private void SelectSomething(String[] query){
    	Main.getDbController().sendSelect(query, this);
    }
    
    @FXML
    private void initialize() {
		
    	//Selecting an Item from the Table
    	studentTable.setOnMousePressed(new EventHandler<Event>() {

			@Override
			public void handle(Event arg0) {
				try {
					sRequests request = studentTable.getSelectionModel().getSelectedItem();
					if (request != null) {
						String sClass = request.getsClass().get();				//Gets Student Class
						String studentName = request.getsFullName().get();		//Gets Student Full Name
						String[] query = {
								"SELECT"
							  + " 	C.courseName "		//Missing Course Name
							  + "FROM"
							  + "	courses AS C "
							  + "WHERE"
							  + "	C.courseID = (SELECT"
							  + "					 CC.prevCourseID "	//Prev Courses
							  + "				  FROM"
							  + "					 course_course AS CC "
							  + "				  WHERE"
							  + "					 CC.curCourseID = '"+request.getcID().get()+"' AND "
							  + "					 CC.prevCourseID NOT IN (SELECT"
							  + "												SC.stdCouCourseID "
							  + "											 FROM"
							  + " 												student_course AS SC "
							  + "											 WHERE"
							  + "												SC.stdCouStudentID = '"+request.getsID().get()+"'))"
						};
						//Sends the Select query to the Server
    					SelectSomething(query);
    					
    					//set missing courses list
    					missingComboBox.setItems(mCourses);

						// set student Full Name Field
						nameField.setText(studentName);

						// set Class Field
						classField.setText(sClass);
					}
				} catch (NullPointerException e) {e.printStackTrace();}
			}
	    });
    	
    	//Pressing the Approve button after selecting an item from the table
    	btnApprove.setOnMousePressed(new EventHandler<MouseEvent>() {
    		
    		@Override
    		public void handle(MouseEvent event) {
    			try{
    				sRequests request = studentTable.getSelectionModel().getSelectedItem();
    				if(request !=null){
    					String[] query  = {
    							"UPDATE requests_student "
    						  + "SET"
    						  + " 	status = '1' "
    						  + "WHERE"
    						  + " 	rID = ' "+request.getrID().get()+" ' "};
    					Main.getDbController().sendInsertAndUpdate(query, this);
    					requests_ALL.remove(request);
    					setNum(requests_ALL.size());
    				}
    			}catch (NullPointerException e) {e.printStackTrace();}
    		}
    	});
    	
    	//Pressing the Disapprove button after selecting an item from the table
    	btnDisapprove.setOnMousePressed(new EventHandler<MouseEvent>() {
    		
    		@Override
    		public void handle(MouseEvent event) {
    			try{
    				sRequests request = studentTable.getSelectionModel().getSelectedItem();
    				if(request !=null){
    					String[] query = {
    							"UPDATE requests_student "
    						  + "SET"
    						  + " 	status = '-1' "
    						  + "WHERE"
    						  + " 	rID = ' "+request.getrID().get()+" ' "};
    					Main.getDbController().sendInsertAndUpdate(query, this);
    					requests_ALL.remove(request);
    					setNum(requests_ALL.size());
    				}
    			}catch(NullPointerException e) {e.printStackTrace();}
    		}
    	});
    
    }
}

    		
  
