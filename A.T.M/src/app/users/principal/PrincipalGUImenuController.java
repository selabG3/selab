package app.users.principal;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

import app.Main;
import app.users.principal.requests.StudentRequestsGUIcontroller;
import app.users.principal.requests.TeacherRequestsGUIcontroller;
import app.users.principal.statistics.StatisticsGradesClassCoursesGUIcontroller;
import app.users.principal.statistics.StatisticsGradesClassTeachersGUIcontroller;
import app.users.principal.statistics.StatisticsGradesTeacherClassesGUIcontroller;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

public class PrincipalGUImenuController implements Initializable {

	//Author: Max Leshkov
	

	/*
	 * FXML elements
	 */
    @FXML
    private Button mainTab;
    @FXML
    private MenuButton requestsOptions;
    @FXML
    private MenuItem teachersRequestOption;
    @FXML
    private MenuItem studentsRequestOption;
    @FXML
    private MenuButton statisticsOptions;
    @FXML
    private MenuItem ClassesOfTeacherStats;
    @FXML
    private MenuItem TeachersOfClassStats;
    @FXML
    private MenuItem CoursesOfClassStats;

	private TabPane mainTabPane;
	
	private HashMap<String, Tab> PrincipalTabs = new HashMap<String, Tab>();
	


	
    /**
     * Displays the Principal Menu
     * @param mainTabPane
     */
	public void setPaneTab(TabPane mainTabPane) {
		this.mainTabPane = mainTabPane;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources){
		
		this.requestsOptions.setText("Requests");
		this.studentsRequestOption.setText("Students Requests");
		this.teachersRequestOption.setText("Teacher Requests");
		
		
		this.teachersRequestOption.setOnAction(new EventHandler<ActionEvent>(){
			
			/*
			 *Handle TeacherRequest
			 */
			@Override
			public void handle(ActionEvent event){
				if( !(PrincipalTabs.containsKey("teachersRequests")))
					try{
						buildTeachersRequestsOption();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(PrincipalTabs.get("teachersRequests"));
			}
		});
		
		this.studentsRequestOption.setOnAction(new EventHandler<ActionEvent>(){
			
			/*
			 * Handle StudentRequest
			 */
			@Override
			public void handle(ActionEvent event){
				if( !(PrincipalTabs.containsKey("studentsRequest")))
					try{
						buildStudentsRequestsOption();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(PrincipalTabs.get("studentsRequest"));
			}
		});
	
		this.ClassesOfTeacherStats.setOnAction(new EventHandler<ActionEvent>(){
			
			/*
			 * Handle ClassesOfATeacherStats
			 */
			@Override
			public void handle(ActionEvent event){
				if( !(PrincipalTabs.containsKey("ClassesOfATeacherStats")))
					try{
						buildClassesOfTeacherStats();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(PrincipalTabs.get("ClassesOfATeacherStats"));
			}
		});
		
		this.TeachersOfClassStats.setOnAction(new EventHandler<ActionEvent>(){
		

			/*
			 * Handle eachersOfAClassStats
			 */
			@Override
			public void handle(ActionEvent event){
				if( !(PrincipalTabs.containsKey("TeachersOfAClassStats")))
					try{
						buildTeachersOfClassStats();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(PrincipalTabs.get("TeachersOfAClassStats"));
			}
		});
		
		this.CoursesOfClassStats.setOnAction(new EventHandler<ActionEvent>(){
			
			/*
			 * Handle StudentRequest
			 */
			@Override
			public void handle(ActionEvent event){
				if( !(PrincipalTabs.containsKey("CoursesOfAClassStats")))
					try{
						buildCoursesOfClassStats();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(PrincipalTabs.get("CoursesOfAClassStats"));
			}
		});
	}
			
	
	/*
	 * Build Teacher Request
	 */
	private void buildTeachersRequestsOption() throws IOException{
		Tab newTab = new Tab("Teachers Requests");
		FXMLLoader loader = new FXMLLoader();
		PrincipalTabs.put("teachersRequests", newTab);
		newTab.setOnClosed(new EventHandler<Event>(){
			
			@Override
			public void handle(Event event){
				PrincipalTabs.remove("teachersRequests");
			}
		});
		
		Parent newPane = loader
				.load(Main.class.getResource("users/principal/requests/TeacherRequestGUI.fxml").openStream());
		TeacherRequestsGUIcontroller teacherRequestsGUIcontroller = (TeacherRequestsGUIcontroller) loader.getController();
		teacherRequestsGUIcontroller.getData();
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
	}
	
	/*
	 * Build Student Request
	 */
	private void buildStudentsRequestsOption() throws IOException{
		Tab newTab = new Tab("Students Requests");
		FXMLLoader loader = new FXMLLoader();
		PrincipalTabs.put("studentsRequest", newTab);
		newTab.setOnClosed(new EventHandler<Event>(){
			
			@Override
			public void handle(Event event){
				PrincipalTabs.remove("studentsRequest");
			}
		});
		
		Parent newPane = loader
				.load(Main.class.getResource("users/principal/requests/StudentRequestGUI.fxml").openStream());
		StudentRequestsGUIcontroller studentRequestsGUIcontroller = (StudentRequestsGUIcontroller) loader.getController();
		studentRequestsGUIcontroller.getData();
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
	}

	/*
	 * Build Teacher Stats based on classes
	 */
	private void buildClassesOfTeacherStats() throws IOException{
		Tab newTab = new Tab("Classes Of A Teacher Stats");
		FXMLLoader loader = new FXMLLoader();
		PrincipalTabs.put("ClassesOfATeacherStats", newTab);
		newTab.setOnClosed(new EventHandler<Event>(){
			
			@Override
			public void handle(Event event){
				PrincipalTabs.remove("ClassesOfATeacherStats");
			}
		});
		
		Parent newPane = loader
				.load(Main.class.getResource("users/principal/statistics/StatisticsGradesTeacherClassesGUI.fxml").openStream());
		StatisticsGradesTeacherClassesGUIcontroller statisticsGradesTeacherClassesGUIcontroller = (StatisticsGradesTeacherClassesGUIcontroller) loader.getController();
		statisticsGradesTeacherClassesGUIcontroller.getData();
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
	}
	
	/*
	 * build class stats based on teachers
	 */
	private void buildTeachersOfClassStats() throws IOException{
		Tab newTab = new Tab("Teachers Of A Class Stats");
		FXMLLoader loader = new FXMLLoader();
		PrincipalTabs.put("TeachersOfAClassStats", newTab);
		newTab.setOnClosed(new EventHandler<Event>(){
			
			@Override
			public void handle(Event event){
				PrincipalTabs.remove("TeachersOfAClassStats");
			}
		});
		
		Parent newPane = loader
				.load(Main.class.getResource("users/principal/statistics/StatisticsGradesClassTeachersGUI.fxml").openStream());
		StatisticsGradesClassTeachersGUIcontroller statisticsGradesClassTeachersGUIcontroller = (StatisticsGradesClassTeachersGUIcontroller) loader.getController();
		statisticsGradesClassTeachersGUIcontroller.getData();
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
	}
	
	/*
	 * builds class stats based on courses
	 */
	private void buildCoursesOfClassStats() throws IOException{
		Tab newTab = new Tab("Courses Of A Class Stats");
		FXMLLoader loader = new FXMLLoader();
		PrincipalTabs.put("CoursesOfAClassStats", newTab);
		newTab.setOnClosed(new EventHandler<Event>(){
			
			@Override
			public void handle(Event event){
				PrincipalTabs.remove("CoursesOfAClassStats");
			}
		});
		
		Parent newPane = loader
				.load(Main.class.getResource("users/principal/statistics/StatisticsGradesClassCoursesGUI.fxml").openStream());
		StatisticsGradesClassCoursesGUIcontroller statisticsGradesClassCoursesGUIcontroller = (StatisticsGradesClassCoursesGUIcontroller) loader.getController();
		statisticsGradesClassCoursesGUIcontroller.getData();
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
		
	}
	
	/**
	 * Shows in the main tab the messages tab.
	 */
	@FXML
	private void messagesForm(){
		if (!(PrincipalTabs.containsKey("sendMail")))
			try {
				buildSendMailTab();
			} catch (IOException e) {
				e.printStackTrace();
			}
		mainTabPane.getSelectionModel().select(PrincipalTabs.get("sendMail"));
	}
	
	/**
	 * Builds the GUI for send mail.
	 * 
	 * @throws IOException
	 */
	private void buildSendMailTab() throws IOException {
		Tab newTab = new Tab("Send Mail");
		FXMLLoader loader = new FXMLLoader();
		PrincipalTabs.put("sendMail", newTab);
		newTab.setOnClosed(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				PrincipalTabs.remove("sendMail");
			}
		});

		Parent newPane = loader.load(Main.class.getResource("common/sendMsg/SendMsgGUI.fxml").openStream());
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
	}
}

