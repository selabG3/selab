package app.users.student;

import java.io.IOException;
import java.util.HashMap;

import app.Main;
import app.users.student.semesters.StudentSemestersGUIcontroller;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.MouseEvent;

public class StudentGUImenuController {

	@FXML
	private Button mainTab;

	@FXML
	private Button currentSemesterTab;

	@FXML
	private Button prevSemesterTab;

	@FXML
	private Button grades;

	private TabPane mainTabPane;
	private HashMap<String, Tab> StudentTabs = new HashMap<String, Tab>();

	@FXML
	/**
	 * initialize page with data
	 */
	public void initialize() {

		this.mainTab.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				mainTabPane.getSelectionModel().select(StudentTabs.get("main"));
			}
		});

		this.prevSemesterTab.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (!(StudentTabs.containsKey("prevSems")))
					try {
						buildPrevSems();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(StudentTabs.get("prevSems"));
			}
		});

	}


	@FXML
	/**
	 * build previous semester details
	 * @throws IOException
	 */
	protected void buildPrevSems() throws IOException {
		Tab newTab = new Tab("Previous Semester Info");
		FXMLLoader loader = new FXMLLoader();
		StudentTabs.put("prevSems", newTab);
		newTab.setOnClosed(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				StudentTabs.remove("prevSems");
			}
		});

		Parent newPane = loader
				.load(Main.class.getResource("users/student/semesters/StudentSemestersGUI.fxml").openStream());
		StudentSemestersGUIcontroller tmp = (StudentSemestersGUIcontroller) loader.getController();
		tmp.setMainTab(mainTabPane, StudentTabs);
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
		mainTabPane.getSelectionModel().select(newTab);
	}

	// *********PUBLIC CLASS METHODS***********

	public void setPaneTab(TabPane mainTabPane) {
		this.mainTabPane = mainTabPane;
		this.StudentTabs.put("main", mainTabPane.getTabs().get(0));
	}

}
