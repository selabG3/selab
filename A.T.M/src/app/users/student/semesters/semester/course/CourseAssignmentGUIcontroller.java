package app.users.student.semesters.semester.course;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;

public class CourseAssignmentGUIcontroller {

    @FXML
    private Text assNameTxt;

    @FXML
    private Button rmvFileBtn;

    @FXML
    private Text dueDateTxt;

    @FXML
    private Button saveAssBtn;

    @FXML
    private Button upFileBtn;

    @FXML
    private Text gradeTxt;

    @FXML
    private Text cNameTxt;
    
    
    
    public void setData(String assName,String dueDateTxtstr,String cName){
    	
    	assNameTxt.setText(assName);
    	cNameTxt.setText(cName);
    	gradeTxt.setText("Not Known");
    	dueDateTxt.setText(dueDateTxtstr);
    }

}
