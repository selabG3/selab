package app.users.student.semesters.semester;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import app.Main;
import app.common.Assignment;
import app.common.Semester;
import app.users.student.semesters.semester.course.CourseAssignmentGUIcontroller;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.text.Text;

public class StudentCourseGUIcontroller {

	private ObservableList<Assignment> TableData;
	private ArrayList<Object[][]> ResultsList;
	private TabPane mainTabPane;
	private HashMap<String, Tab> StudentTabs;
	private String AssignmentID;
	private String AssignmentName;
	private String AssignmentDueDate;
	private String AssignmentCourse;

	@FXML
	private Text txtCourseID;

	@FXML
	private TableColumn<Assignment, String> assDueDateCol;

	@FXML
	private TableView<Assignment> ttvAssView;

	@FXML
	private TableColumn<Assignment, String> assUpDateCol;

	@FXML
	private TableColumn<Assignment, String> assNameCol;

	@FXML
	private Button goToBtn;

	@FXML
	private TableColumn<Assignment, String> assIdCol;

	@FXML
	private Text txtCourseName;

	@FXML
	/**
	 * initialzie tables/page with data
	 */
	private void initialize() {

		TableData = FXCollections.observableArrayList();

		assIdCol.setCellValueFactory(cellData -> cellData.getValue().getassNumssimple());
		assNameCol.setCellValueFactory(cellData -> cellData.getValue().getAssNamesimple());
		assUpDateCol.setCellValueFactory(cellData -> cellData.getValue().getupDatessimple());
		assDueDateCol.setCellValueFactory(cellData -> cellData.getValue().getdueDatessimple());

		ttvAssView.setItems(TableData);

		getInfo();

		ttvAssView.setOnMousePressed(new EventHandler<Event>() {

			@Override
			public void handle(Event arg0) {
				try {

					AssignmentID = new String(
							ttvAssView.getSelectionModel().getSelectedItems().get(0).getAssNamesimple().get());
					AssignmentName = new String(
							ttvAssView.getSelectionModel().getSelectedItems().get(0).getAssNamesimple().get());
					AssignmentDueDate = new String(
							ttvAssView.getSelectionModel().getSelectedItems().get(0).getdueDatessimple().get());

				} catch (NullPointerException e) {
					// Empty because i don't give a shit BOOM!!
				}
			}
		});

		/**
		 * set action for button
		 */
		goToBtn.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event arg0) {
				try {

					Tab newTab = new Tab(AssignmentName);
					FXMLLoader loader = new FXMLLoader();
					StudentTabs.put(AssignmentName, newTab);
					newTab.setOnClosed(new EventHandler<Event>() {

						@Override
						public void handle(Event event) {
							StudentTabs.remove(AssignmentName);
						}
					});

					Parent newPane = loader.load(
							Main.class.getResource("users/student/semesters/semester/course/CourseAssignmentGUI.fxml")
									.openStream());
					CourseAssignmentGUIcontroller tmp = (CourseAssignmentGUIcontroller) loader.getController();
					tmp.setData(AssignmentName, AssignmentDueDate, txtCourseName.getText());
					newTab.setContent(newPane);
					mainTabPane.getTabs().add(newTab);
					mainTabPane.getSelectionModel().select(newTab);

				} catch (NullPointerException | IOException e) {
					System.out.print("y u do this to me..? :((( ");
				}
			}
		});

	}

	/**
	 * get assignments for course 00001
	 * testing
	 */
	private void getInfo() {

		Main.getDbController().getAssInfo("00001", this);

	}

	/**
	 * set data into text fields
	 * @param cID
	 * @param cName
	 */
	public void setData(String cID, String cName) {

		txtCourseID.setText(cID);
		txtCourseName.setText(cName);
	}

	/**
	 * build assignments tables
	 */
	public void buildAss() {

		ResultsList = Main.getDbController().getResultsList();

		if (ResultsList.get(0)[0][0] == null)
			return;
		
		for(int i =0; i< ResultsList.get(0).length;i++){
			
			String assNum = new String(ResultsList.get(0)[i][0].toString());
			String assName = new String(ResultsList.get(0)[i][1].toString());
			String upDate = new String(ResultsList.get(0)[i][2].toString());
			String dueDate = new String(ResultsList.get(0)[i][3].toString());
			
			Assignment e = new Assignment(assNum, assName, upDate, dueDate);
			TableData.add(e);
		}
	}
}
