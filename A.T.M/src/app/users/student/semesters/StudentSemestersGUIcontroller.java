package app.users.student.semesters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import app.Main;
import app.common.Semester;
import app.users.student.semesters.semester.StudentCourseGUIcontroller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.text.Text;

public class StudentSemestersGUIcontroller {

	private ObservableList<Semester> TableData;
	private ObservableList<String> DropDown;
	private ArrayList<Object[][]> ResultsList;
	private int flag;
	private TabPane mainTabPane;
	private HashMap<String, Tab> StudentTabs;
	
    
    @FXML
    private Text txtCourseID;

    @FXML
    private Text txtEndDate;

    @FXML
    private TableColumn<Semester, String> semIdCol;

    @FXML
    private Button showInfoBtn;
    
    @FXML
    private Button IntoCorPage;

    @FXML
    private Text txtSemesterYear;

    @FXML
    private Text txtStartDate;

    @FXML
    private TableView<Semester> ttvSemesterReview;

    @FXML
    private ComboBox<String> cbCourseList;

    @FXML
    private Text txtCourseName;
    
    @FXML
    void CurOrNotBtn(ActionEvent event) {
    	
    	if(flag == 1)
    		flag = 0;
    	else
    		flag = 1;
    	
    	getInfo(flag);

    }
    
    @FXML
    /**
     * initialize page/tables with data and variables
     */
	private void initialize() {
    	
    	TableData = FXCollections.observableArrayList();
    	DropDown = FXCollections.observableArrayList();
    	
    	semIdCol.setCellValueFactory(cellData -> cellData.getValue().getSemesterIdProperty());
    	
    	ttvSemesterReview.setItems(TableData);
    	
    	cbCourseList.setItems(DropDown);
    	
    	getInfo(flag);
    	
    	ttvSemesterReview.setOnMousePressed(new EventHandler<Event>() {

			@Override
			public void handle(Event arg0) {
				try {
					
					txtSemesterYear.setText(ttvSemesterReview.getSelectionModel().getSelectedItems().get(0).getYear());
					txtStartDate.setText(ttvSemesterReview.getSelectionModel().getSelectedItems().get(0).getStartDate());
					txtEndDate.setText(ttvSemesterReview.getSelectionModel().getSelectedItems().get(0).getEndDate());
					
					getInfoDrop();
					
					
					
				} catch (NullPointerException e) {
					// Empty because i don't give a shit BOOM!!
				}
			}
		});
    	
    	showInfoBtn.setOnMouseClicked(new EventHandler<Event>() {


			@Override
			public void handle(Event arg0) {
				try {
					
					getCourseInfo(cbCourseList.getSelectionModel().getSelectedItem());
					
					
				} catch (NullPointerException e) {
					// Empty because i don't give a shit BOOM!!
				}
			}
		});
    	
    	IntoCorPage.setOnMouseClicked(new EventHandler<Event>() {


			@Override
			public void handle(Event arg0) {
				try {
					
					Tab newTab = new Tab(txtCourseName.getText());
					FXMLLoader loader = new FXMLLoader();
					StudentTabs.put(txtCourseName.getText(), newTab);
					newTab.setOnClosed(new EventHandler<Event>() {

						@Override
						public void handle(Event event) {
							StudentTabs.remove(txtCourseName.getText());
						}
					});

					Parent newPane = loader
							.load(Main.class.getResource("users/student/semesters/semester/StudentCourseGUI.fxml").openStream());
					StudentCourseGUIcontroller tmp = (StudentCourseGUIcontroller) loader.getController();
					tmp.setData(txtCourseID.getText(), txtCourseName.getText());
					newTab.setContent(newPane);
					mainTabPane.getTabs().add(newTab);	
					mainTabPane.getSelectionModel().select(newTab);
					
				} catch (NullPointerException | IOException e) {
					System.out.print("y u do this to me..? :( ");
				}
			}
		});
    	
    }

    /**
     * get courses's info
     * @param s
     */
	protected void getCourseInfo(String s) {
		
		Main.getDbController().getInfoAboutCourse(s,this);
		
	}
	
	/**
	 * get information of previous semesters
	 * @param flag2
	 */
	private void getInfo(int flag2) {
		
		Main.getDbController().getPrevSems(flag2,this);
		
	}
	
	/**
	 * get data from drop down menu
	 */
	private void getInfoDrop(){
		
		Main.getDbController().getCoursesOfSemsterX(ttvSemesterReview.getSelectionModel().getSelectedItems().get(0).getSemesterID(),this);
		
		
	}

	/**
	 * build data into tables
	 */
	public void buildData() {
		
		ResultsList = Main.getDbController().getResultsList();
		TableData.clear();

		if (ResultsList.get(0)[0][0] == null)
			return;
		
		for(int i=0;i<ResultsList.get(0).length;i++){
			
			String id = new String(ResultsList.get(0)[i][0].toString());
			String type = new String(ResultsList.get(0)[i][1].toString());
			String sDate = new String(ResultsList.get(0)[i][2].toString());
			String eDate = new String(ResultsList.get(0)[i][3].toString());
			String year = new String(ResultsList.get(0)[i][4].toString());
			
			Semester tmp = new Semester(id, type, sDate, eDate, year);
			
			TableData.add(tmp);
		}
		
	}

	/**
	 * build data inside drop down menu
	 */
	public void buildDataDrop() {
		
		ResultsList = Main.getDbController().getResultsList();

		if (ResultsList.get(0)[0][0] == null)
			return;
		
		for(int i=0;i<ResultsList.get(0).length;i++)
			DropDown.add(ResultsList.get(0)[i][0].toString());
		
	}

	/**
	 * build data into TXT fields
	 */
	public void buildCourse() {

		ResultsList = Main.getDbController().getResultsList();
		
		if (ResultsList.get(0)[0][0] == null)
			return;
		
		txtCourseID.setText(ResultsList.get(0)[0][0].toString());
		txtCourseName.setText(ResultsList.get(0)[0][2].toString());
		
	}
	
	/**
	 * set mainTabpane instance with parent data
	 * set studentsTabs instance with parent data
	 * @param tP
	 * @param sT
	 */
	public void setMainTab(TabPane tP, HashMap<String, Tab> sT){
		
		this.mainTabPane = tP;
		this.StudentTabs = sT;
	}

}
