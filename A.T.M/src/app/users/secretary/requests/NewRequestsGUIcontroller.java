package app.users.secretary.requests;


import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.Main;
import app.common.Semester;
import app.common.sRequests;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class NewRequestsGUIcontroller implements Initializable {

	
	private ArrayList<sRequests> newStudentRequests;
	private ObservableList<sRequests> newStudentRequests_OS;
	private Semester newSemester;
	private ArrayList<String> Queries = new ArrayList<String>();
	private LocalDate Date = LocalDate.now();
	private Stage newStage;
	
    @FXML
    private TableView<sRequests> tvStudentRequests;
    @FXML
    private TableColumn<sRequests, String> tvcID;
    @FXML
    private TableColumn<sRequests, String> tvcName;
    @FXML
    private TableColumn<sRequests, String> tvcCourse;
    @FXML
    private Text txtDate;
    @FXML
    private Button btnSendSingle;
    @FXML
    private Button btnSendAll;
    @FXML
    private Button btnReturn;
    
    /**
     * 
     * @param newStudentRequests
     */
	public void setStudentsRequests(ArrayList<sRequests> newStudentRequests) {
		this.newStudentRequests = newStudentRequests;
	}

	/**
	 * 
	 * @param newSemester
	 */
	public void setNewSemester(Semester newSemester) {
		this.newSemester = newSemester;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		tvcID.setCellValueFactory(cellData-> cellData.getValue().getsID());
		tvcName.setCellValueFactory(cellData-> cellData.getValue().getsFullName());
		tvcCourse.setCellValueFactory(cellData-> cellData.getValue().getcName());
		
		txtDate.setText(Date.toString());
		
		btnSendSingle.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				sRequests request = tvStudentRequests.getSelectionModel().getSelectedItem();
				if(request!=null){
					String query = new String();
					query = "INSERT INTO requests_student (reqStduID,reqStdWantCourseID,date) VALUES ('"+request.getsID().get()+"','"+request.getsID().get()+"','"+Date.toString()+"')";
					if(!Queries.contains(query))
						Queries.add(query);
				}
			}
		});
		
		btnSendAll.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				for(sRequests request : newStudentRequests){
					String query = new String();
					query = "INSERT INTO requests_student (reqStduID,reqStdWantCourseID,date) VALUES ('"+request.getsID().get()+"','"+request.getsID().get()+"','"+Date.toString()+"')";
					if(!Queries.contains(query))
						Queries.add(query);
				}
			}
		});
		
		btnReturn.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				sendQueries();
				newStage.close();
			}
		});
	}
	
	/**
	 * 
	 */
    private void sendQueries(){
    	Main.getDbController().sendInsertAndUpdate(Queries);
    }

    /**
     * 
     * @param newStage
     */
	public void setStage(Stage newStage) {
		this.newStage = newStage;
	}
	
	/**
	 * 
	 */
	public void buildData(){
		newStudentRequests_OS = FXCollections.observableArrayList(newStudentRequests);
		tvStudentRequests.setItems(newStudentRequests_OS);
	}
}