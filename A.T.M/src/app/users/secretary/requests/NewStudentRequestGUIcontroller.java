package app.users.secretary.requests;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.Main;
import app.common.Course;
import app.common.Student;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class NewStudentRequestGUIcontroller implements Initializable{

	
	private ObservableList<Course> CourseCanTake_OL;
	private Student SelectedStudent;
	private LocalDate Date  = LocalDate.now();
    
	@FXML
    private Text txtStudentName;
    @FXML
    private Button btnSendRequest;
    @FXML
    private Text txtStudentID;
    @FXML
    private Button btnReturn;
    @FXML
    private ComboBox<Course> cbCourseList;
	private Stage stage;

	/**
	 * Shows the students details.
	 * @param selectedStudent
	 */
	public void showStudentDetails(Student selectedStudent) {
		this.SelectedStudent = selectedStudent;
		txtStudentID.setText(selectedStudent.getID());
		txtStudentName.setText(selectedStudent.getFullName());
	}

	/**
	 * Sets for the selected student his available courses.
	 * @param allCourses
	 */
	public void setCourseList(ArrayList<Course> allCourses) {
		CourseCanTake_OL = FXCollections.observableArrayList();	
		for(Course course : allCourses){
			if(!SelectedStudent.getCourses().contains(course))
				CourseCanTake_OL.add(course);
		}
		cbCourseList.setItems(CourseCanTake_OL);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		btnSendRequest.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				if(cbCourseList.getSelectionModel().getSelectedItem()!=null){
					String[] Quesries = {"INSERT INTO requests_student (requests_student.reqStduID , requests_student.reqStdWantCourseID,requests_student.date) VALUES ("+SelectedStudent.getID()+","+cbCourseList.getSelectionModel().getSelectedItem().getCourseID()+","+Date+")"};
					Main.getDbController().sendInsertAndUpdate(Quesries);
					stage.close();
				}
			}
		});
		
		btnReturn.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				stage.close();
			}
		});
	}

	/**
	 * Sets this stage.
	 * @param stage - This stage.
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}


}