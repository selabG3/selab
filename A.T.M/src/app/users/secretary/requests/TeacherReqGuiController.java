package app.users.secretary.requests;

import java.util.ArrayList;

import app.Main;
import app.common.tRequests;
import app.common.tRequests;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.text.Text;

public class TeacherReqGuiController {

	private ObservableList<tRequests> TableData;
	private ArrayList<Object[][]> ResultsList;
	private ArrayList<String> queryList = new ArrayList<String>();
	private int rID;
	private String semID;

	@FXML
	private TableView<tRequests> tView;

	@FXML
	private TableColumn<tRequests, String> typeCol;

	@FXML
	private Button getReq;

	@FXML
	private Text tNameTxt;

	@FXML
	private Text classTxt;

	@FXML
	private TableColumn<tRequests, String> tIdCol;

	@FXML
	private Text tIdTxt;

	@FXML
	private TableColumn<tRequests, String> datecol;

	@FXML
	private Text courseIdTxt;

	@FXML
	private Button applyBtn;

	@FXML
	private TableColumn<tRequests, String> askCol;

	@FXML
	private Button saveBtn;

	@FXML
	/**
	 * get's all the data needed for the operation of add/remove teacher from
	 * course
	 */
	public void getRequests() {

		TableData = FXCollections.observableArrayList();

		Main.getDbController().getTeacherRequests(this);

		// set in tView
		tIdCol.setCellValueFactory(cellData -> cellData.getValue().getTeacherID());
		typeCol.setCellValueFactory(cellData -> cellData.getValue().getType());
		askCol.setCellValueFactory(cellData -> cellData.getValue().getCourseID());
		datecol.setCellValueFactory(cellData -> cellData.getValue().getDate());

		// set all data inside tView
		tView.setItems(TableData);

		// Add Events.
		tView.setOnMousePressed(new EventHandler<Event>() {

			// ResultsList.get(i)
			// 0 - all results from requests_teacher
			// (rId,reqTuID,reqTWantCourseID,reqTClassID,reqTClassName,date,status,rType)
			// 1 - all data about Teachers
			// (uId,uFirstName,uLastName,uType=2,uGender,uPicture,uAdress,uEmail)
			// 2 - semesterID
			// 3 - all teaching unit data
			// (deptID,deptName)

			@Override
			public void handle(Event arg0) {
				try {
					if (tView.getSelectionModel().getSelectedItems().get(0) != null) {
						String TeacherID = tView.getSelectionModel().getSelectedItems().get(0).getTeacherID().get();
						String CourseID = tView.getSelectionModel().getSelectedItems().get(0).getCourseID()
								.get();
						String TName;
						String Class = tView.getSelectionModel().getSelectedItems().get(0).getClassID().get()
								+ tView.getSelectionModel().getSelectedItems().get(0).getClassName().get();

						// set student ID Text
						tIdTxt.setText(TeacherID);

						// set Course ID Text
						courseIdTxt.setText(CourseID);

						// set Class Text
						classTxt.setText(Class);

						// gets and sets Teacher Name on side panel
						TName = getTeacherName(TeacherID);
						tNameTxt.setText(TName);

						// sets rID
						rID = tView.getSelectionModel().getSelectedItems().get(0).getRequestID().get();

					}

				} catch (NullPointerException e) {
				}
			}
		});
	}

	@FXML
	public void addToQueryList() {

		String clsName, clsID;

		// create and add a query to update the request from approved to dealt
		// with
		String tmp = new String("UPDATE requests_teacher set status = 2 where rID = " + String.valueOf(rID));
		queryList.add(tmp);

		// create and add a query to add the teacher to teacher_course
		tmp = new String("INSERT teacher_course (TCouTeacherID,TCouCourseID) values (\"" + tIdTxt.getText() + "\",\""
				+ courseIdTxt.getText() + "\")");
		queryList.add(tmp);

		// create and add a query to add the teacher to teacher_class
		clsName = new String(classTxt.getText().substring(0, 1));
		clsID = new String(classTxt.getText().substring(1, 2));
		tmp = new String("INSERT teacher_class_semester (tID,cID,sID,cName,courID) values (\"" + tIdTxt.getText()
				+ "\",\"" + clsID + "\",\"" + semID + "\",\"" + clsName + "\",\"" + courseIdTxt.getText() + "\")");
		queryList.add(tmp);
	}

	@FXML
	public void UpdateDB() {

		Main.getDbController().updateDBAfterRequests(queryList, this);

	}

	/**
	 * build data in for in the Tables
	 */
	public void buildData() {
		

		ResultsList = Main.getDbController().getResultsList();
		
		if(ResultsList.get(0)[0][0] == null)
			return;
		
		int size = ResultsList.get(0).length;

		// ResultsList.get(i)
		// 0 - all results from requests_teacher
		// (rId,reqTuID,reqTWantCourseID,reqTClassID,reqTClassName,date,status,rType)
		// 1 - all data about Teachers
		// (uId,uFirstName,uLastName,uType=2,uGender,uPicture,uAdress,uEmail)
		// 2 - semesterID
		// 3 - all teaching unit data
		// (deptID,deptName)

		// save data in tRequests class for table
		for (int i = 0; i < size; i++) {

			String rID = new String(ResultsList.get(0)[i][0].toString());
			String tID = new String(ResultsList.get(0)[i][1].toString());
			String cID = new String(ResultsList.get(0)[i][2].toString());
			String clID = new String(ResultsList.get(0)[i][3].toString());
			String ClName = new String(ResultsList.get(0)[i][4].toString());
			String Date = new String(ResultsList.get(0)[i][5].toString());
			String type = new String(ResultsList.get(0)[i][7].toString());

			TableData.add(new tRequests(rID, tID, cID, Date, clID, ClName, type));

		}

		semID = new String(ResultsList.get(2)[0][0].toString());

	}

	protected String getTeacherName(String teacherID) {

		int size = ResultsList.get(1).length;

		for (int i = 0; i < size; i++)
			if (ResultsList.get(1)[i][0].equals(teacherID))
				return ResultsList.get(1)[i][1].toString() + " " + ResultsList.get(1)[i][2].toString();
		return null;
	}

	public void clearQueryList() {

		queryList.clear();

	}
}