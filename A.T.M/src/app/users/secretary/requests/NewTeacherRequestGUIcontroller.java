package app.users.secretary.requests;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.common.Course;
import app.common.Teacher;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import app.Main;
import app.common.Class;

public class NewTeacherRequestGUIcontroller implements Initializable {

	@FXML
	private Text txtTeacherName;
	@FXML
	private Button btnSendReqAdding;
	@FXML
	private Button btnSendReqRemoving;
	@FXML
	private Text txtTeacherID;
	@FXML
	private Button btnReturn;
	@FXML
	private ComboBox<Course> cbCourseList;
	@FXML
	private ComboBox<Course> cbTeacherCourses;

	private LocalDate Date = LocalDate.now();
	private ObservableList<Course> CourseCanTake_OL;
	private ObservableList<Course> TeachingCourse_OL;
	private Teacher selectedTeacher;
	private Stage stage;

	/**
	 * Shows teacher details.
	 * 
	 * @param selectedTeacher
	 */
	public void setTeacherDetails(Teacher selectedTeacher) {
		this.selectedTeacher = selectedTeacher;
		txtTeacherID.setText(selectedTeacher.getID());
		txtTeacherName.setText(selectedTeacher.getFullName());
	}

	/**
	 * Shows all available courses for the selected teacher.
	 * 
	 * @param availableCouses
	 */
	public void setCourseList(ArrayList<Course> availableCouses) {
		if (selectedTeacher != null) {
			CourseCanTake_OL = FXCollections.observableArrayList();
			TeachingCourse_OL = FXCollections.observableArrayList(selectedTeacher.getTeacheringCourses());
			for (Course course : availableCouses) {
				if (!selectedTeacher.getTeacheringCourses().contains(course))
					CourseCanTake_OL.add(course);
			}
			cbTeacherCourses.getItems().clear();
			cbCourseList.getItems().clear();
			cbCourseList.setItems(CourseCanTake_OL);
			cbTeacherCourses.setItems(TeachingCourse_OL);
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		txtTeacherID.setText("");
		txtTeacherName.setText("");
		
		btnReturn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				stage.close();
			}
		});

		btnSendReqAdding.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (cbCourseList.getSelectionModel().getSelectedItem() != null) {
					String[] Queries = {
							"INSERT INTO requests_student (requests_student.reqStduID , requests_student.reqStdWantCourseID,requests_student.date,requests_student) VALUES ("
									+ selectedTeacher.getID() + ","
									+ cbCourseList.getSelectionModel().getSelectedItem().getCourseID() + "," + Date
									+ ")",
							"INSERT INTO requests_teacher (requests_teacher.reqTuID,requests_teacher.reqTWantCourseID,requests_teacher.date,requests_teacher.status,requests_teacher.rType) VALUES ('"
									+ selectedTeacher.getID() + "','"
									+ cbCourseList.getSelectionModel().getSelectedItem().getCourseID() + "','"
									+ Date.toString() + "','" + '0' + "','" + "" + "')" };
					Main.getDbController().sendInsertAndUpdate(Queries);
					stage.close();
				}
			}
		});

		btnSendReqRemoving.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

			}
		});
	}

	/**
	 * Sets this stage.
	 * 
	 * @param stage
	 *            - This stage.
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}

}