package app.users.secretary.requests;

import java.util.ArrayList;

import app.Main;
import app.common.sRequests;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.text.Text;

public class StudentReqGuiController {

	private ObservableList<sRequests> TableData;
	private ObservableList<String> DropDown;
	private ArrayList<Object[][]> ResultsList;
	private ArrayList<String> queryList = new ArrayList<String>();
	private String rID;

	
	@FXML
	private Text sNameTxt;

	@FXML
	private TableColumn<sRequests, String> uIdT;

	@FXML
	private Text sIdTxt;

	@FXML
	private Button reqBtn;

	@FXML
	private TableColumn<sRequests, String> ansT;

	@FXML
	private TableView<sRequests> tView;

	@FXML
	private ComboBox<String> clsNameD;

	@FXML
	private ComboBox<String> clsDrop;

	@FXML
	private TableColumn<sRequests, String> dateT;

	@FXML
	private Button finBtn;

	@FXML
	private Button addBtn;

	@FXML
	private Text cIdTxt;

	@FXML
	private Text SemIdTxt;

	@FXML
	private void makeQuery() {

		 // create and add to queryList.
		//  an insert query into student_course.
		String tmp = new String("INSERT into student_course (stdCouCourseID,stdCouStudentID,grade,stdCouSemesterID) values (\""
				+ cIdTxt.getText() + "\",\"" + sIdTxt.getText() + "\",-1," + SemIdTxt.getText() + ");");
		queryList.add(tmp);
		
		 // create and add to queryList.
		//  an update query. updates the request with rID status's in requests_students
		tmp = new String("UPDATE requests_student as rs SET `status`='2' WHERE `rID`=" + rID);
		queryList.add(tmp);
		
	/*	 // create and add to queryList.
		//  a delete query from requests_student
		tmp = new String("DELETE FROM requests_student where rID =" + rID); 
		queryList.add(tmp); */

	}

	@FXML
	private void UpdateDB() {

		Main.getDbController().updateDBAfterRequests(queryList, this);
	}

	@FXML
	public void getRequests() {

		TableData = FXCollections.observableArrayList();

		Main.getDbController().getStudentRequests(this);

		// set in tView
		uIdT.setCellValueFactory(cellData -> cellData.getValue().getsID());
		ansT.setCellValueFactory(cellData -> cellData.getValue().getAnswer());
		dateT.setCellValueFactory(cellData -> cellData.getValue().getDate());

		// set all data inside tView
		tView.setItems(TableData);

		// Add Events.
		tView.setOnMousePressed(new EventHandler<Event>() {

			// ResultsList.get(i)
			// 0 - all results from requests_student
			// (rId,reqStuId,reqStdWantCourseId,date,status,rType)
			// 1 - all data about students
			// (uId,uFirstName,uLastName,uType=2,uGender,uPicture,uAdress,uEmail)
			// 2 - semesterID
			// 3 - all data from class_course(cls_id,cou_id,sem_id)
			// 4 - all data about classes

			@Override
			public void handle(Event arg0) {
				try {
					if (tView.getSelectionModel().getSelectedItems().get(0).getsID() != null) {
						String studentID = tView.getSelectionModel().getSelectedItems().get(0).getsID().get();
						String courseId = tView.getSelectionModel().getSelectedItems().get(0).getcID().get();
						String studentName;
						ArrayList<String> clsId;

						// set student ID Text
						sIdTxt.setText(studentID);

						// set Course ID Text
						cIdTxt.setText(courseId);

						// gets and sets Student Name on side panel
						studentName = getStudentName(studentID);
						sNameTxt.setText(studentName);
						
						//sets rID
						rID = tView.getSelectionModel().getSelectedItems().get(0).getrID().get(); 

						// gets and sets All available Classes in Class Name
						// drop down menu
						clsId = getsClass(courseId);
						DropDown = FXCollections.observableArrayList();
						DropDown.addAll(clsId);
						clsNameD.setItems(DropDown);

					}

				} catch (NullPointerException e) {
				}
			}

			// gets all available class to teach courseId
			private ArrayList<String> getsClass(String courseId) {

				int Isize = ResultsList.get(4).length;   // limit of for(i)
				int Osize = ResultsList.get(3).length;  // limit of for(ii)
				int cSize;							   // limit of for(iii)
				ArrayList<String> fList = new ArrayList<String>(); // final list of class to return
				ArrayList<String> list = new ArrayList<String>(); // list of all classes available
				ArrayList<String> cList = new ArrayList<String>(); // list of class which teach courseId
				
				for(int j=0; j < Isize; j++) // generates a list of all available classes. for(i)
					list.add(ResultsList.get(4)[j][0].toString() + ResultsList.get(4)[j][1].toString());
				
				for(int j=0;j < Osize; j++) // generates a list of classes which teach courseId. for(ii)
					if(ResultsList.get(3)[j][3].toString().equals(courseId))
						cList.add(ResultsList.get(3)[j][0].toString() + ResultsList.get(3)[j][1].toString());
				
				cSize = cList.size();
				
				for (int j = 0; j < cSize; j++) // goes through cList, and adds to fList if class has less than 25 students. for(iii)
					if(list.contains(cList.get(j).toString()))
							fList.add(cList.get(j).toString());
				
				return fList;
			}	

			// gets student's Name from ResultsList.get(1)
			private String getStudentName(String studentID) {

				// loop to find out the student's name
				for (int i = 0; i < ResultsList.get(1).length; i++)
					if (ResultsList.get(1)[i][0].equals(studentID))
						return ResultsList.get(1)[i][1].toString() + " " + ResultsList.get(1)[i][2].toString();

				return "No such student";

			}
		});

	}

	public void buildData() {

		ResultsList = Main.getDbController().getResultsList();
		
		if(ResultsList.get(0)[0][0] == null)
			return;

		// ResultsList.get(i)
		// 0 - all results from requests_student
		// (rId,reqStuId,reqStdWantCourseId,date,status,rType)
		// 1 - all data about students
		// (uId,uFirstName,uLastName,uType=2,uGender,uPicture,uAdress,uEmail)
		// 2 - semesterID
		// 3 - all data from class_course(cls_id,cou_id,sem_id)
		// 4 - 

		// save data in sRequests class for table
		for (int i = 0; i < ResultsList.get(0).length; i++)
			TableData.add(new sRequests(ResultsList.get(0)[i][0].toString(), ResultsList.get(0)[i][1].toString(),
					ResultsList.get(0)[i][2].toString(), ResultsList.get(0)[i][3].toString(),
					ResultsList.get(0)[i][4].toString()));

		// sets semesterId
		SemIdTxt.setText(ResultsList.get(2)[0][0].toString());

	}

	public void clearQueryList() {

		queryList.clear();

	}

}
