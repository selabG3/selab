package app.users.secretary.semesters.prev;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.Main;
import app.common.Semester;
import app.common.Student;
import app.common.Teacher;
import app.common.gui.ISemester;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import app.common.Class;
import app.common.Course;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;

public class SecretarySemesterGUIcontroller implements ISemester,Initializable{

	
	//**********
	
	
	private TabPane setMainTabPane;
	private Semester semester;
	private ObservableList<Course>CourseList_OL;
	
	//**********
	
    @FXML
    private TreeTableView<String> ttvSemesterReview;
    @FXML
    private TreeTableColumn<String, String> ttcClass;
    @FXML
    private TreeTableColumn<String, String> ttcTeacher;
    @FXML
    private TreeTableColumn<String, String> ttcStudent;
	@FXML
    private Text txtCourseID;
    @FXML
    private Text txtEndDate;
    @FXML
    private Text txtSemesterYear;
    @FXML
    private Text txtSemesterType;
    @FXML
    private Text txtStartDate;
    @FXML
    private Text txtCourseName;
    @FXML
    private ComboBox<Course> cbCourseList;

	
	
	//********PUBLIC METHODS***********
	
	
	/**
	 * 
	 * @param mainTabPane
	 */
	public void setMainTabPane(TabPane mainTabPane) {
		this.setMainTabPane = mainTabPane;
	}
	
	/**
	 * 
	 * @param semester
	 */
	public void setSemester(Semester semester) {
		this.semester = semester;
	}

	/**
	 * Builds all the information regards to the semester.
	 */
	public void buildPrevSemesterDetails() {
		/*
		 *  0 - Semester's Courses.
		 *  1 - Semester's Classes.
		 *  2 - Semester's Teachers.
		 *  3 - Semester's Students.
		 *  4 - Semester's Course's Classes.
		 *  5 - Semester's Course's Class's Teacher.
		 *  6 - Semester's Course's Class's Students.
		 *  
		 */
		ArrayList<Object[][]> ResultList = Main.getDbController().getResultsList();
		int maxRows;
		int index;
		Course semCourse;
		Class semClass;
		Teacher semTeacher;
		Student semStudent;
		
		//Builds all semester's courses.
		maxRows = ResultList.get(0).length;
		if(ResultList.get(0)[0][0]!=null)
			for(index = 0 ; index<maxRows ; index++){
				semCourse=new Course(ResultList.get(0)[index][0].toString());
				semCourse.setCourseName(ResultList.get(0)[index][1].toString());
				semester.addCourses(semCourse);
		}
		
		//Builds all semester's classes.
		maxRows = ResultList.get(1).length;
		if(ResultList.get(1)[0][0]!=null)
			for(index = 0 ; index<maxRows ; index++){
				semClass = new Class(ResultList.get(1)[index][0].toString());
				semClass.setClassNumber(ResultList.get(1)[index][1].toString());
				semester.addClass(semClass);
			}
		
		//Builds all semester's teachers.
		maxRows = ResultList.get(2).length;
		if(ResultList.get(2)[0][0]!=null)
			for(index = 0 ;index<maxRows ; index++){
				semTeacher = new Teacher(ResultList.get(2)[index][0].toString());
				semTeacher.setFirstName(ResultList.get(2)[index][1].toString());
				semTeacher.setLastName(ResultList.get(2)[index][3].toString());
				semester.addTeacher(semTeacher);
			}
		
		//Builds all semester's students.
		maxRows = ResultList.get(3).length;
		if(ResultList.get(3)[0][0]!=null)
			for(index=0 ; index<maxRows ; index++){
				semStudent = new Student(ResultList.get(3)[index][0].toString());
				semStudent.setFirstName(ResultList.get(3)[index][1].toString());
				semStudent.setLastName(ResultList.get(3)[index][3].toString());
				semester.addStudent(semStudent);
			}
		
		//Builds Semester's Course's Classes.
		maxRows = ResultList.get(4).length;
			if(ResultList.get(4)[0][0]!=null)
			for(index = 0 ; index<maxRows ; index++){
				semCourse = semester.getCourse(ResultList.get(4)[index][0].toString());
				semClass = semester.getExistClass(ResultList.get(4)[index][1].toString(),ResultList.get(4)[index][2].toString());
				semCourse.addNewClass(semClass);
			}
		
		//Builds Semester's Course's Class's Teacher.
		maxRows = ResultList.get(5).length;
		if(ResultList.get(5)[0][0]!=null)
			for(index = 0 ;index<maxRows ; index++){
				semTeacher = semester.getTeacher(ResultList.get(4)[index][0].toString());
				semClass = semester.getExistClass(ResultList.get(4)[index][1].toString(),ResultList.get(4)[index][2].toString());
				semCourse = semester.getCourse(ResultList.get(4)[index][3].toString());
				semTeacher.addClass(semClass);
				semTeacher.addCourse(semCourse);
				semClass.addTeacher(semTeacher);
				semCourse.addTeacher(semTeacher);
			}
		
		//Builds Semester's Course's Class's Students.
		maxRows = ResultList.get(6).length;
		if(ResultList.get(6)[0][0]!=null)
			for(index = 0 ; index<maxRows ; index++){
				semStudent = semester.getStudent(ResultList.get(4)[index][0].toString());
				semCourse = semester.getCourse(ResultList.get(4)[index][1].toString());
				semClass = semester.getExistClass(ResultList.get(4)[index][2].toString(),ResultList.get(4)[index][3].toString());
				semCourse.addStudent(semStudent);
				semClass.addStudent(semStudent);
			}
		buildCourseList();
	}
	


	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ttcClass.setCellValueFactory(cellData-> new SimpleStringProperty(cellData.getValue().getValue()));
		ttcTeacher.setCellValueFactory(cellData-> new SimpleStringProperty(cellData.getValue().getValue()));
		ttcStudent.setCellValueFactory(cellData-> new SimpleStringProperty(cellData.getValue().getValue()));
		
		cbCourseList.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				if(cbCourseList.getSelectionModel().getSelectedItem()!=null){
					buildTreeRootsItems(cbCourseList.getSelectionModel().getSelectedItem());
				}
			}
		});
	}


		
	
	private void buildCourseList(){
		CourseList_OL = FXCollections.observableArrayList(semester.getCoursesList());
		cbCourseList.setItems(CourseList_OL);
	}
	
	private void buildTreeRootsItems(Course selectedCourse){
		if(semester!=null){
		TreeItem<String> root = new TreeItem<String>("Course");
		TreeItem<String> classe;
		TreeItem<String> teacher;
		TreeItem<String> student;
			for(Class semesterClass : selectedCourse.getClassList()){//For each course's classes.
				classe = new TreeItem<String>(semesterClass.toString());//Make class as root's child. Make the course's class's teacher as a child of the child of the root.
					for(Teacher semesterTeacher : semesterClass.getTeachersList()){
						teacher = new TreeItem<String>(semesterTeacher.getFullName());
						classe.getChildren().add(teacher);
					}
					for(Student semesterStudent : semesterClass.getStudentList()){
						student = new TreeItem<String>(semesterStudent.getFullName());
						classe.getChildren().add(student);
					}
				root.getChildren().add(classe);
			}
			ttvSemesterReview.setRoot(root);
			ttvSemesterReview.setShowRoot(false);
			}
		/*
		for(Teacher semesterTeacher : semester.getTeachrsList()){
			teacher = new TreeItem<String>(semesterTeacher.getFullName());
			for(Class teacherClass : semesterTeacher.getTeacheringClasses()){
				classe = new TreeItem<String>(teacherClass.toString());//Make class as root's child. Make the course's class's teacher as a child of the child of the root. 
				teacher.getChildren().add(classe);
			}
			for(Course teahcerCourse : semesterTeacher.getTeacheringCourses()){
				course = new TreeItem<String>(teahcerCourse.toString());//Make course as root.
				teacher.getChildren().add(course);
			}
			root.getChildren().add(teacher);
		}
		*/

		}
	

}
