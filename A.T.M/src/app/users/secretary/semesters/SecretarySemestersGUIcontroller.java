package app.users.secretary.semesters;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.common.Semester;
import app.common.gui.ISemesters;
import app.common.gui.SemestersGUI;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Pagination;
import javafx.scene.control.TabPane;

public class SecretarySemestersGUIcontroller extends SemestersGUI implements Initializable,ISemesters{

	@FXML
	private Pagination pagination;
	
	public void buildAllSemestersForSemester(){
		super.buildAllSemestersForSemester();
	}
	
	public void setSemesterList(ArrayList<Semester> SemesterList){
		super.setSemesterList(SemesterList);
	}
	
	public void setPagination(Pagination pagination){
		super.setPagination(this.pagination);
	}
	
	public void setMainTabPane(TabPane mainTabPane) {
		super.setMainTabPane(mainTabPane);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}



}
