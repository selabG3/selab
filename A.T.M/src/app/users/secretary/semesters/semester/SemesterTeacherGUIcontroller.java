package app.users.secretary.semesters.semester;


import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.common.Class;
import app.common.Course;
import app.common.Semester;
import app.common.Teacher;
import app.comparators.ClassComparator;
import app.comparators.CourseComparator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

public class SemesterTeacherGUIcontroller implements Initializable {

	
	//*****************
	
	private AbstractSecretarySemesterGUI abstractSemesterGUI;
	private ArrayList<Course> SemesterCourses = new ArrayList<Course>();//Holds all the semester's courses.
	private ObservableList<Course>SemesterCourses_OS;//Hold for the comboBox the courses. 
	private ObservableList<Class>SemesterCourseClasses_OS;//Hold for the comboBox all the course's classes.
	private ArrayList<Teacher> Teachers = new ArrayList<Teacher>();//All the teachers.
	private ArrayList<Teacher> SemesterTeachers = new ArrayList<Teacher>();//Holds all the semester's teachers.
	private ObservableList<Teacher> AvaileableTeachers_OS;
	private ObservableList<Teacher> ClassTeachers_OS;
	private final int MaxTeachingHours = 20;
	private Course SelectedCourse;
	private Class SelectedClass;
	private Teacher SelectedTeacher;
	private Semester newSemester;
	
	//*******
	
    @FXML
    private ComboBox<Class> cbClassList;
    @FXML
    private ComboBox<Course> cbCourseList;
    @FXML
    private TableView<Teacher> tvCourseClassTeachers;
    @FXML
    private TableView<Teacher> tvAvailableTeahcers;
    @FXML
    private TableColumn<Teacher, String> tcCourseClassTheacer;
    @FXML
    private TableColumn<Teacher, String> tcAvailableTeachers;
    @FXML
    private Text txtMaxHours;
    @FXML
    private Text txtTeacherLeftHours;
    @FXML
    private Text txtCourseDeptName;
    @FXML
    private Text txtCourseHours;
    @FXML
    private Button btnAssign;
    @FXML
    private Button btnReassign;
    @FXML
    private Button btnSaveDetails;
    
  
    

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		txtMaxHours.setText(String.valueOf(MaxTeachingHours));
		txtCourseDeptName.setText("");
		txtTeacherLeftHours.setText("");
		txtCourseHours.setText("");
		
		//Set models for all the cellData.
		tcCourseClassTheacer.setCellValueFactory(cellData -> cellData.getValue().getFullNameProperty());
		tcAvailableTeachers.setCellValueFactory(cellData -> cellData.getValue().getFullNameProperty());
	
		//Set selection mode for the tables view.
		tvCourseClassTeachers.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		tvAvailableTeahcers.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

		cbCourseList.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if(cbCourseList.getSelectionModel().getSelectedItem()!=null){
					//Clear the data from the tables views.
					tvAvailableTeahcers.getItems().clear();
					tvCourseClassTeachers.getItems().clear();
					//Set new data into tables.
					SelectedCourse = cbCourseList.getSelectionModel().getSelectedItem();	
					txtCourseHours.setText(SelectedCourse.getHoursPerWeekString());
					txtCourseDeptName.setText(SelectedCourse.getCourseDeptName());
					SemesterCourseClasses_OS = FXCollections.observableArrayList(SelectedCourse.getClassList());
					cbClassList.getItems().clear();
					cbClassList.setItems(SemesterCourseClasses_OS);
				}
			}
		});
		
		cbClassList.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if(cbClassList.getSelectionModel().getSelectedItem()!=null){
					Teacher tempTeacher = null;
					SelectedClass = cbClassList.getSelectionModel().getSelectedItem();
					ClassTeachers_OS = FXCollections.observableArrayList();
					//Set new data into tables.
					AvaileableTeachers_OS = FXCollections.observableArrayList(Teachers);
					String courseDeptID = SelectedCourse.getCourseDeptID();
					//Search and remove any teacher that doesn't teach that course.
					for(Teacher teacher : Teachers){
						if(!teacher.isTeachingDept(courseDeptID))
							AvaileableTeachers_OS.remove(teacher);
					}
					//Removing and adding the teacher that already teaching the course in the selected class.
					int indexClassCourse = SelectedClass.getCoursetList().indexOf(SelectedCourse);
					if((SelectedClass.getTeachersList().size()>0)&&(indexClassCourse<SelectedClass.getTeachersList().size()))
					tempTeacher = SelectedClass.getTeachersList().get(indexClassCourse);
					if(tempTeacher!=null)
						if(Teachers.contains(tempTeacher)){
							AvaileableTeachers_OS.remove(tempTeacher);
							ClassTeachers_OS.add(tempTeacher);
						}
					
					tvAvailableTeahcers.getItems().clear();
					tvCourseClassTeachers.getItems().clear();
					tvAvailableTeahcers.setItems(AvaileableTeachers_OS);
					tvAvailableTeahcers.sort();
					tvCourseClassTeachers.setItems(ClassTeachers_OS);
					tvCourseClassTeachers.sort();
				}
			}
		});
		
		tvAvailableTeahcers.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(tvAvailableTeahcers.getSelectionModel().getSelectedItem()!=null)
				txtTeacherLeftHours.setText(String.valueOf(tvAvailableTeahcers.getSelectionModel().getSelectedItem().getAvailableTeachingHours()));
			}
		});
		
		tvCourseClassTeachers.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(tvCourseClassTeachers.getSelectionModel().getSelectedItem()!=null)
				txtTeacherLeftHours.setText(String.valueOf(tvCourseClassTeachers.getSelectionModel().getSelectedItem().getAvailableTeachingHours()));
			}
		});
				
	}

	
	//**************PRIVATE CLASS METHODS**********
	
	private void buildSemesterCourseList(){
		if(!SemesterCourses.isEmpty()){
			SemesterCourses_OS = FXCollections.observableArrayList(SemesterCourses);
			SemesterCourses_OS.sort(new CourseComparator(true));
			cbCourseList.setItems(SemesterCourses_OS);
		}
	}
	
		
	@FXML
	private void assignTeacherToCourse(){
		//If none then we can add the teacher into the class's course.
		if(!(tvCourseClassTeachers.getItems().size()>0)){
			if(tvAvailableTeahcers.getSelectionModel().getSelectedItem()!=null){
				//Gets the selected teacher.
				SelectedTeacher = tvAvailableTeahcers.getSelectionModel().getSelectedItem();
				//Adds data for the teacher.
				SelectedTeacher.getTeacheringClasses().add(SelectedClass);
				SelectedTeacher.getTeacheringCourses().add(SelectedCourse);
				SelectedTeacher.addWeeklyTeachingHours(SelectedCourse.getHoursPerWeek());
				newSemester.addTeacher(SelectedTeacher);
				txtTeacherLeftHours.setText(String.valueOf(SelectedTeacher.getAvailableTeachingHours()));
				//Adds data to the class.
				int indexClassCourse = SelectedClass.getCoursetList().indexOf(SelectedCourse);
				SelectedClass.getTeachersList().set(indexClassCourse, SelectedTeacher);
				//Adds data to the course.
				int indexCourseClass = SelectedCourse.getClassList().indexOf(SelectedClass);
				SelectedCourse.getTehacerList().set(indexCourseClass, SelectedTeacher);
				//Adds data to the semester.
				SemesterTeachers.add(SelectedTeacher);
				//Removing and adding in the lists.
				AvaileableTeachers_OS.remove(SelectedTeacher);
				ClassTeachers_OS.add(SelectedTeacher);
				tvAvailableTeahcers.refresh();
				tvAvailableTeahcers.sort();
				tvCourseClassTeachers.refresh();
				tvCourseClassTeachers.sort();
			}
		}
	}
	
	@FXML
	private void reassignTeacherFromCourse(){
		if(tvCourseClassTeachers.getItems().size()>0){
			SelectedTeacher = tvCourseClassTeachers.getSelectionModel().getSelectedItem();
			//Removes data from the teacher.
			int courseIndex = SelectedTeacher.getTeacheringCourses().indexOf(SelectedCourse);
			SelectedTeacher.getTeacheringClasses().remove(courseIndex);
			SelectedTeacher.getTeacheringCourses().remove(courseIndex);
			SelectedTeacher.removeWeeklyTeachingHours(SelectedCourse.getHoursPerWeek());
			//Removes data from the class.
			courseIndex = SelectedClass.getCoursetList().indexOf(SelectedCourse);
			SelectedClass.getTeachersList().remove(courseIndex);
			//Removes data from course.
			int classIndex = SelectedCourse.getClassList().indexOf(SelectedClass);
			SelectedCourse.getTehacerList().remove(classIndex);
			//Removing and adding in the lists.
			AvaileableTeachers_OS.add(SelectedTeacher);
			ClassTeachers_OS.remove(SelectedTeacher);
			tvAvailableTeahcers.refresh();
			tvAvailableTeahcers.sort();
			tvCourseClassTeachers.refresh();
			tvCourseClassTeachers.sort();
			newSemester.getTeachrsList().remove(SelectedTeacher);
		}
	}

	@FXML
	private void saveSemesterDetails(){
		abstractSemesterGUI.buildTreeRootsItems();
	}


	//**************PUBLIC CLASS METHODS*************
	
	public void buildLists(){
		buildSemesterCourseList();
	}
	public void setAbstractSemesterGUI(AbstractSecretarySemesterGUI abstractSemesterGUI) {
		this.abstractSemesterGUI = abstractSemesterGUI;
	}


	public void setCourses(ArrayList<Course> courses) {
		this.SemesterCourses = courses;
	}


	public void setTeachers(ArrayList<Teacher> teachers) {
		this.Teachers = teachers;
	}


	public void setNewSemester(Semester newSemester) {
		this.newSemester = newSemester;
	}
}
