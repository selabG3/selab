package app.users.secretary.semesters.semester;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import app.common.Semester;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class SemesterDetailsGUIcontroller implements Initializable {

	// Help to get date from DatePicker.
	private LocalDate date = LocalDate.now();
	private Semester newSemester;
	
	@FXML
	private ComboBox<String> cbSemesterType;
	@FXML
	private DatePicker semesterStartDate;
	@FXML
	private DatePicker semesterEndDate;
	@FXML
	private Text txtDateErr;
	@FXML
	private Text txtError;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		txtError.setText("");
		txtDateErr.setText("");
		txtDateErr.setFill(Color.RED);
		txtDateErr.setFont(new Font("System",10));
		
		// Add semester types into the combo box.
		cbSemesterType.setItems(FXCollections.observableArrayList("A", "B"));
		cbSemesterType.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				cbSemesterType.setPromptText("");
				String semesterType = cbSemesterType.getSelectionModel().getSelectedItem();
				if (semesterType != null) {
					newSemester.setSemesterType(semesterType);
				}
			}
		});
		
		semesterStartDate.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if ((semesterStartDate.getValue()!=null)&&(!semesterStartDate.getValue().isBefore(date))){
					newSemester.setStartDate(semesterStartDate.getValue().toString());
					txtDateErr.setText("");
				}
				else
					txtDateErr.setText("*Wrong date.");
			}
		});

		semesterEndDate.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if ((semesterEndDate.getValue()!=null)&&(!semesterEndDate.getValue().isBefore(semesterStartDate.getValue()))) {
					txtDateErr.setText("");
					newSemester.setEndDate(semesterEndDate.getValue().toString());
					newSemester.setYear(String.valueOf(semesterEndDate.getValue().getYear()));
				}
				else
					txtDateErr.setText("*Wrong date.");	
			}
		});

	}

	
	/**
	 * Set an text error when required fields are missing.
	 */
	protected void showError(){
		txtError.setText("Missing fields.");
	}
	
	/**
	 * Hides the error text.
	 */
	protected void hideError(){
		txtError.setText("");
	}

	protected void showSemesterDetails() {
		cbSemesterType.setPromptText(newSemester.getSemesterType());
		date = LocalDate.parse(newSemester.getStartDate());
		semesterStartDate.setValue(date);
		date = LocalDate.parse(newSemester.getEndDate());
		semesterEndDate.setValue(date);
	}

	protected void finished() {
		cbSemesterType.setDisable(true);
		semesterStartDate.setDisable(true);
		semesterEndDate.setDisable(true);
	}

	protected void setNewSemester(Semester newSemester) {
		this.newSemester = newSemester;
	}

}
