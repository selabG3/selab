package app.users.secretary.semesters.semester.open;

import java.util.ArrayList;

import app.Main;
import app.common.Class;
import app.common.Course;
import app.common.Dept;
import app.common.Semester;
import app.common.Student;
import app.common.Teacher;
import app.users.secretary.semesters.semester.AbstractSecretarySemesterGUI;
import javafx.fxml.Initializable;

public class OpenNewSemesterGUIcontroller extends AbstractSecretarySemesterGUI implements Initializable {

	private ArrayList<Object[][]> ResultsList;//All the Results from the Queries.
    private ArrayList<Course> Courses = new ArrayList<Course>();//Storing all courses.
    private ArrayList<Teacher> Teachers = new ArrayList<Teacher>();//Storing all teachers.
    private ArrayList<Class> Classes = new ArrayList<Class>();//Storing all classes.
    private ArrayList<Student> Students = new ArrayList<Student>();//String all students.
	private Semester newSemester;//Save all details for the new semester.

	/**
	 * Queries to gets all the information to open a new semester.
	 */
	private String[] queries = {"SELECT c.courseID , c.departmentID , d.deptName , c.courseName , c.studyHoursPerWeek FROM courses c , depts d WHERE c.departmentID=d.deptID"
			,"SELECT * FROM course_course"
			,"SELECT u.uID , u.uFirstName , u.uLastName FROM teachers t , users u WHERE u.uID=t.teacherID"
			,"SELECT t.teacherID , td.TDepDeptID , d.deptName FROM teachers t , teacher_dept td , depts d WHERE t.teacherID=td.TDepTeacherID AND td.TDepDeptID=d.deptID"
			,"SELECT * FROM  classes"
			,"SELECT u.uID , u.uFirstName , u.uLastName FROM users u , students s WHERE s.studentID=u.uID"
			,"SELECT sc.stdCouCourseID , s.studentID FROM students s , student_course sc WHERE s.studentID = sc.stdCouStudentID AND  sc.grade>=55"
			,"SELECT * FROM student_class sc ORDER BY sc.stdClassClassID"
			,"UPDATE semesters SET semesters.Current = 0"
			,"INSERT INTO semesters () VALUES ()"
			,"SELECT s.SemesterID FROM semesters s WHERE s.Current=1"
	};
	
	/**
	 * Ask from the Server and the Database for all the information that
	 * the user will need to be able to open a new semester.
	 */
    public void getData(){
		//Gets all the details that need to build a new semesters.
		Main.getDbController().getAllDetailsForNewSemester(this,queries);		
    }
    
    /**
	 * Build from all the Results that got from the Database
	 * a list of classes of each type.
	 * The whole details that need for a new semester.
	 * Courses , Classes , Teachers , Students...
	 */
    public void buildData(){
		ResultsList = Main.getDbController().getResultsList();
		if(ResultsList!=null)
			if(ResultsList.size()!=0){
		Course course,prevCourse;
		Teacher teacher;
		Dept dept;
		Class newClass;
		Student student;
		
		//Build Course List.
		int maxCourses = ResultsList.get(0).length;
		int index;
		for(index=0 ; index<maxCourses ; index++){
			course = new Course(ResultsList.get(0)[index][0].toString());
			course.setCourseDeptID(ResultsList.get(0)[index][1].toString());
			course.setCourseDeptName(ResultsList.get(0)[index][2].toString());
			course.setCourseName(ResultsList.get(0)[index][3].toString());
			course.setHoursPerWeek(ResultsList.get(0)[index][4].toString());
			Courses.add(course);
			}
		setCourses(Courses);

		
		//Build Previous Course's Courses.
		maxCourses = ResultsList.get(1).length;
		for(index=0 ; index<maxCourses ; index++){
			prevCourse = Courses.get(SearchCourseIndex(ResultsList.get(1)[index][1].toString()));
			int CourseIndex = SearchCourseIndex(ResultsList.get(1)[index][0].toString());
			if(CourseIndex>=0)
				Courses.get(CourseIndex).addNewPrevCourse(prevCourse);
		}
		
		
		//Build Teachers Info.
		int maxTeachers = ResultsList.get(2).length;
		for(index=0 ; index<maxTeachers ; index++){
			teacher = new Teacher(ResultsList.get(2)[index][0].toString());
			teacher.setFirstName(ResultsList.get(2)[index][1].toString());
			teacher.setLastName(ResultsList.get(2)[index][2].toString());
			Teachers.add(teacher);
		}
		setTeachers(Teachers);

		
		//Build Teacher's departments.
		int maxTeachersDept = ResultsList.get(3).length;
		for(index=0 ; index<maxTeachersDept ; index++){
			teacher = Teachers.get(SearchTeacherIndex(ResultsList.get(3)[index][0].toString()));
			dept = new Dept(ResultsList.get(3)[index][1].toString());
			dept.setDeprtName(ResultsList.get(3)[index][2].toString());
			teacher.addNewDept(dept);
		}
		
		//Build Classes.
		int maxClasses = ResultsList.get(4).length;
		for(index=0 ; index<maxClasses ; index++){
			newClass = new Class(ResultsList.get(4)[index][0].toString());
			newClass.setClassName(ResultsList.get(4)[index][1].toString());
			Classes.add(newClass);
		}
		setClasses(Classes);

		
		//Build Students.
		int maxStudents = ResultsList.get(5).length;
		for(index=0 ; index<maxStudents ; index++){
			student = new Student(ResultsList.get(5)[index][0].toString());
			student.setFirstName(ResultsList.get(5)[index][1].toString());
			student.setLastName(ResultsList.get(5)[index][2].toString());
			Students.add(student);
		}
		
		//Build Student's pass Courses.
		if(ResultsList.get(6)[0][0]!=null)
		{
			int maxStudentsCoursesPass = ResultsList.get(6).length; //All pass courses of all students.
			for(index=0 ; index<maxStudentsCoursesPass ; index++){
				prevCourse = Courses.get(SearchCourseIndex(ResultsList.get(6)[index][0].toString()));
				Students.get(SearchStudentIndex(ResultsList.get(6)[index][1].toString())).addCourse(prevCourse);
			}
		}
		setStudents(Students);

		
		//Build Class's Students.
		int maxClassesStudents = ResultsList.get(7).length;
		for(index=0 ; index<maxClassesStudents ; index++){
			int studentIndex = SearchStudentIndex(ResultsList.get(7)[index][0].toString());
			if(studentIndex!=-1){
				student = Students.get(studentIndex);
				Classes.get(SearchClassIndex(ResultsList.get(7)[index][1].toString(),ResultsList.get(7)[index][2].toString())).addStudent(student);
			}
		}
		
		//Build Semester Details
		newSemester = new Semester();
		newSemester.setSemesterID(String.valueOf(ResultsList.get(8)[0][0]));

		setSemester(newSemester);
		buildScenes();
		}
    }

    
    
	/**
	 * Search for a course if exists.
	 * @param courseID
	 * @return Course index in Courses List.
	 */
	protected int SearchCourseIndex(String courseID){
		for(Course course : Courses){
			if(course.getCourseID().compareTo(courseID)==0)
				return Courses.indexOf(course);
		}
		return -1;
	}
	
	/**
	 * Search for a teacher if exists.
	 * @param teacherID
	 * @return	Teacher index in Teachers List.
	 */
	protected int SearchTeacherIndex(String teacherID){
		for(Teacher teacher : Teachers){
			if(teacher.getID().compareTo(teacherID)==0)
				return Teachers.indexOf(teacher);
		}
		return -1;
	}
	
	/**
	 * Search for a student if exists.
	 * @param studentID
	 * @return Student index in Students List.
	 */
	protected int SearchStudentIndex(String studentID){
		for(Student student : Students){
			if(student.getID().compareTo(studentID)==0)
				return Students.indexOf(student);
		}
		return -1;
	}
}
