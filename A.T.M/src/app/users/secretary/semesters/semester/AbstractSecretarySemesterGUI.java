package app.users.secretary.semesters.semester;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.Main;
import app.common.Class;
import app.common.Course;
import app.common.Semester;
import app.common.Student;
import app.common.Teacher;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public abstract class AbstractSecretarySemesterGUI implements Initializable {

	
	private Semester newSemester;//Save all details for the new semester.
	private ArrayList<Course> Courses = new ArrayList<Course>();//Storing all courses.
	private ArrayList<Teacher> Teachers = new ArrayList<Teacher>();//Storing all teachers.
	private ArrayList<Class> Classes = new ArrayList<Class>();//Storing all classes.
	private ArrayList<Student> Students = new ArrayList<Student>();//String all students.
	private Parent DetailsPane;
	private SemesterDetailsGUIcontroller semesterDetailsGUIcontroller;
	private Parent CoursePane;
	private SemesterCourseGUIcontroller semesterCourseGUIcontroller;
	private Parent TeacherPane;
	private SemesterTeacherGUIcontroller semesterTeacherGUIcontroller;
	private Parent ReviewPane;
	private SemesterReviewGUIcontroller semesterReviewGUIcontroller;
    
    
    @FXML
    protected BorderPane openSemesterPane;
    @FXML
    private Button btnPrev;
    @FXML
    private Button btnNext;
    @FXML
    private Button btnSemesterTeacher;
    @FXML
    private Button btnSemesterCourse;
    @FXML
    private Button btnSemesterDone;
    @FXML
    private HBox topMenu;
    @FXML
    private VBox topPane;
    @FXML
    private GridPane bottoMenu;
	
    
    
//****************PRIVATE CLASS METHODS********************************
    
    
    /**
     * Builds the first scene, the semester details.
     * This scene will be in the right of the border pane.
     * This scene will be all ways in front of the GUI.
     * @throws IOException
     */
    protected void buildFirstScene() throws IOException{
    	FXMLLoader loader = new FXMLLoader();
		DetailsPane = loader.load(Main.class.getResource("users/secretary/semesters/semester/SemesterDetailsGUI.fxml").openStream());
		DetailsPane.setId("DetailsScene");
		semesterDetailsGUIcontroller = (SemesterDetailsGUIcontroller) loader.getController();
		semesterDetailsGUIcontroller.setNewSemester(newSemester);
		openSemesterPane.setRight(DetailsPane);
    }
    
    /**
     * Builds the second scene, the semester courses.
     * This scene is the first scene.
     * @throws IOException
     */
    protected void buildSecondScene() throws IOException{
    	FXMLLoader loader = new FXMLLoader();
		CoursePane = loader.load(Main.class.getResource("users/secretary/semesters/semester/SemesterCourseGUI.fxml").openStream());
		CoursePane.setId("CourseScene");
		semesterCourseGUIcontroller = (SemesterCourseGUIcontroller) loader.getController();
		semesterCourseGUIcontroller.setAbstractSemesterGUI(this);
		semesterCourseGUIcontroller.setClasses(Classes);
		semesterCourseGUIcontroller.setCourses(Courses);
		semesterCourseGUIcontroller.setNewSemester(newSemester);
		semesterCourseGUIcontroller.buildLists();
		openSemesterPane.setCenter(CoursePane);
    }
    
    /**
     * Builds the third scene, the assign teachers to class of a course.
     * @throws IOException
     */
    protected void buildThirdScene() throws IOException{
    	FXMLLoader loader = new FXMLLoader();
		TeacherPane = loader.load(Main.class.getResource("users/secretary/semesters/semester/SemesterTeacherGUI.fxml").openStream());
		TeacherPane.setId("TeacherScene");
		semesterTeacherGUIcontroller = (SemesterTeacherGUIcontroller) loader.getController();
		semesterTeacherGUIcontroller.setAbstractSemesterGUI(this);
		semesterTeacherGUIcontroller.setCourses(newSemester.getCoursesList());//Set the semester's courses.
		semesterTeacherGUIcontroller.setTeachers(Teachers);
		semesterTeacherGUIcontroller.setNewSemester(newSemester);
		semesterTeacherGUIcontroller.buildLists();
    }
    
    /**
     * Builds the fourth scene. Shows all the review for the semester.
     * @throws IOException
     */
    protected void buildFourthScene() throws IOException{
    	FXMLLoader loader = new FXMLLoader();
    	ReviewPane = loader.load(Main.class.getResource("users/secretary/semesters/semester/SemesterReviewGUI.fxml").openStream());
		ReviewPane.setId("ReviewPane");
    	semesterReviewGUIcontroller = (SemesterReviewGUIcontroller) loader.getController();
    	semesterReviewGUIcontroller.setAbstractSemesterGUI(this);
    	semesterReviewGUIcontroller.setNewSemesterDetails(newSemester);
    }
     
    
	//****************CLASS HELP METHODS********************************
    
    /**
     * 
     */
	protected void buildTreeRootsItems(){
		semesterReviewGUIcontroller.buildTreeRootsItems();
	}
    
	/**
	 * Search for a class if exists.
	 * @param classID
	 * @param className
	 * @return Class index in Classes List.
	 */
	protected int SearchClassIndex(String classID,String className){
		for(Class curClass : Classes){
			if(curClass.getClassName().matches(className))
				if(curClass.getClassNumber().matches(classID))
					return Classes.indexOf(curClass);
		}
		return -1;
	}
	
	/**
	 * Search for a course if exists.
	 * @param courseID
	 * @return Course index in Courses List.
	 */
	protected int SearchCourseIndex(String courseID){
		for(Course course : Courses){
			if(course.getCourseID().compareTo(courseID)==0)
				return Courses.indexOf(course);
		}
		return -1;
	}
	
	/**
	 * Search for a teacher if exists.
	 * @param teacherID
	 * @return	Teacher index in Teachers List.
	 */
	protected int SearchTeacherIndex(String teacherID){
		for(Teacher teacher : Teachers){
			if(teacher.getID().compareTo(teacherID)==0)
				return Teachers.indexOf(teacher);
		}
		return -1;
	}
	
	/**
	 * Search for a student if exists.
	 * @param studentID
	 * @return Student index in Students List.
	 */
	protected int SearchStudentIndex(String studentID){
		for(Student student : Students){
			if(student.getID().compareTo(studentID)==0)
				return Students.indexOf(student);
		}
		return -1;
	}

    
    
	
	//************************PUBLIC CLASS METHODS************************
    
	/**
	 * Show an error when one is missing:
	 * 1)Semester Type.
	 * 2)Semester Start Date.
	 * 3)Semester End Date.
	 */
	protected void showError(){
		semesterDetailsGUIcontroller.showError();
	}
	
	/**
	 * Hides the error from semester details.
	 */
	protected void hideError(){
		semesterDetailsGUIcontroller.hideError();
	}
	
    /**
     * 
     */
	protected void sentToThirdSceneDetails(){
		semesterTeacherGUIcontroller.setTeachers(Teachers);
		semesterTeacherGUIcontroller.buildLists();
	}
	


	/**
	 * Builds all the scenes so the user can go from each other , back and forwards.
	 */
	protected void buildScenes(){
		try {
			buildFirstScene();
			buildSecondScene();
			buildThirdScene();
			buildFourthScene();
			//Adding menus only if the main GUI show.
			topPane.getChildren().add(topMenu);
			openSemesterPane.setBottom(bottoMenu);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    
	/**
	 * Gets newSemester object.
	 * @return
	 */
	public Semester getNewSemester(){
		return newSemester;
	}
	
	/**
	 * Set new semester object.
	 * @param newSemester
	 */
	public void setNewSemester(Semester newSemester){
		this.newSemester = newSemester;
	}
	
	/**
	 * Ask from the Server and the Database for all the information that
	 * the user will need to be able to open a new semester.
	 */
    public abstract void getData();
	
    /**
     * 
     */
    public abstract void buildData();
    
    /**
     * When the user is finishing edit the new semester, all the buttons will disabled.
     */
	public void finished() {
		btnNext.setDisable(true);
		btnPrev.setDisable(true);
		btnSemesterCourse.setDisable(true);
		btnSemesterTeacher.setDisable(true);
		semesterDetailsGUIcontroller.finished();
	}
    
	
	//*********PROTECTED CLASS METHODS*************
	
	
	/**
	 * 
	 * @param courses
	 */
	protected void setCourses(ArrayList<Course> courses){
		this.Courses = courses;
	}
	
	/**
	 * 
	 * @param newSemester
	 */
	protected void setSemester(Semester newSemester) {
		this.newSemester = newSemester;
	}

	/**
	 * 
	 * @param students
	 */
	protected void setStudents(ArrayList<Student> students) {
		this.Students = students;
	}

	/**
	 * 
	 * @param classes
	 */
	protected void setClasses(ArrayList<Class> classes) {
		this.Classes = classes;
	}

	/**
	 * 
	 * @param teachers
	 */
	protected void setTeachers(ArrayList<Teacher> teachers) {
		this.Teachers = teachers;
	}
	//************************INITIALIZE****************************
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		topPane.getChildren().remove(topMenu);
		openSemesterPane.getChildren().remove(bottoMenu);
		
		//Set default colors.
		btnNext.setStyle("-fx-background-color:transparent");
		btnPrev.setStyle("-fx-background-color:transparent");
		btnSemesterCourse.setStyle("-fx-background-color:#a3c2c2");
		btnSemesterTeacher.setStyle("-fx-background-color:transparent");
		btnSemesterDone.setStyle("-fx-background-color:transparent");
		
		//Button Next
		btnNext.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				btnNext.setStyle("-fx-background-color:#a3c2c2");
				if(openSemesterPane.getCenter()==CoursePane){
					openSemesterPane.setCenter(TeacherPane);
					btnSemesterCourse.setStyle("-fx-background-color:transparent");
					btnSemesterTeacher.setStyle("-fx-background-color:#a3c2c2");
					btnSemesterDone.setStyle("-fx-background-color:transparent");
				}
				else if(openSemesterPane.getCenter()==TeacherPane){
					openSemesterPane.setCenter(ReviewPane);
					btnSemesterCourse.setStyle("-fx-background-color:transparent");
					btnSemesterTeacher.setStyle("-fx-background-color:transparent");
					btnSemesterDone.setStyle("-fx-background-color:#a3c2c2");
				}
			}
		});	
		
		btnNext.setOnMouseMoved(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				btnNext.setStyle("-fx-background-color:#e0ebeb");
			}
		});
		
		btnNext.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				btnNext.setStyle("-fx-background-color:transparent");
			}
		});

		//Button Previous
		btnPrev.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				btnPrev.setStyle("-fx-background-color:#a3c2c2");
				if(openSemesterPane.getCenter()==TeacherPane){
					openSemesterPane.setCenter(CoursePane);
					btnSemesterCourse.setStyle("-fx-background-color:#a3c2c2");
					btnSemesterTeacher.setStyle("-fx-background-color:transparent");
					btnSemesterDone.setStyle("-fx-background-color:transparent");
				}
				else if(openSemesterPane.getCenter()==ReviewPane){
					openSemesterPane.setCenter(TeacherPane);
					btnSemesterCourse.setStyle("-fx-background-color:transparent");
					btnSemesterTeacher.setStyle("-fx-background-color:#a3c2c2");
					btnSemesterDone.setStyle("-fx-background-color:transparent");
				}
			}
		});	
    	
    	btnPrev.setOnMouseMoved(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				btnPrev.setStyle("-fx-background-color:#e0ebeb");
			}
		});
		
    	btnPrev.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				btnPrev.setStyle("-fx-background-color:transparent");
			}
		});
    	
    	
		//Set colors for btnSemesterCourse
		btnSemesterCourse.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(openSemesterPane.getCenter()!=CoursePane){
					openSemesterPane.setCenter(CoursePane);
					btnSemesterCourse.setStyle("-fx-background-color:#a3c2c2");
					btnSemesterTeacher.setStyle("-fx-background-color:transparent");
					btnSemesterDone.setStyle("-fx-background-color:transparent");
				}
			}
		});
		
		btnSemesterCourse.setOnMouseMoved(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(openSemesterPane.getCenter()!=CoursePane)
					btnSemesterCourse.setStyle("-fx-background-color:#e0ebeb");
			}
		});
		
		btnSemesterCourse.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(openSemesterPane.getCenter()!=CoursePane)
					btnSemesterCourse.setStyle("-fx-background-color:transparent");
			}
		});
		
		
		//Set colors for btnSemesterTeacher.
		btnSemesterTeacher.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(openSemesterPane.getCenter()!=TeacherPane){
					openSemesterPane.setCenter(TeacherPane);
					btnSemesterCourse.setStyle("-fx-background-color:transparent");
					btnSemesterTeacher.setStyle("-fx-background-color:#a3c2c2");
					btnSemesterDone.setStyle("-fx-background-color:transparent");
				}
			}
		});
		
		btnSemesterTeacher.setOnMouseMoved(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(openSemesterPane.getCenter()!=TeacherPane)
					btnSemesterTeacher.setStyle("-fx-background-color:#e0ebeb");
			}
		});
		
		btnSemesterTeacher.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(openSemesterPane.getCenter()!=TeacherPane)
					btnSemesterTeacher.setStyle("-fx-background-color:transparent");
			}
		});
		
		
		//Set colors for btnSemesterDone.
		btnSemesterDone.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(openSemesterPane.getCenter()!=ReviewPane){
					openSemesterPane.setCenter(ReviewPane);
					btnSemesterCourse.setStyle("-fx-background-color:transparent");
					btnSemesterTeacher.setStyle("-fx-background-color:transparent");
					btnSemesterDone.setStyle("-fx-background-color:#a3c2c2");
				}
			}
		});
		
		btnSemesterDone.setOnMouseMoved(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(openSemesterPane.getCenter()!=ReviewPane)
					btnSemesterDone.setStyle("-fx-background-color:#e0ebeb");
			}
		});
		
		btnSemesterDone.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(openSemesterPane.getCenter()!=ReviewPane)
					btnSemesterDone.setStyle("-fx-background-color:transparent");
			}
		});
	}


	protected void showSemesterDetails(){
		semesterDetailsGUIcontroller.showSemesterDetails();
		semesterReviewGUIcontroller.buildTreeRootsItems();
	}

}
