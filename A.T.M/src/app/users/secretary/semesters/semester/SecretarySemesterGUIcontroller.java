package app.users.secretary.semesters.semester;

import java.net.URL;
import java.util.ResourceBundle;

import app.common.Semester;
import javafx.fxml.Initializable;
import javafx.scene.control.TabPane;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.text.Text;

public class SecretarySemesterGUIcontroller implements Initializable {

	
	//**********
	
	
	private TabPane setMainTabPane;
	private Semester semester;
	
	
	//***********FXMLS
	
	

    @FXML
    private Text txtCourseID;

    @FXML
    private Text txtEndDate;

    @FXML
    private TreeTableColumn<?, ?> ttcTeacher;

    @FXML
    private Text txtSemesterYear;

    @FXML
    private TreeTableColumn<?, ?> ttcClass;

    @FXML
    private Text txtSemesterType;

    @FXML
    private Text txtStartDate;

    @FXML
    private TreeTableView<?> ttvSemesterReview;

    @FXML
    private ComboBox<?> cbCourseList;

    @FXML
    private Text txtCourseName;

    @FXML
    private TreeTableColumn<?, ?> ttcStudent;
	
	
	//********PUBLIC METHODS***********
	
	
	/**
	 * 
	 * @param mainTabPane
	 */
	public void setMainTabPane(TabPane mainTabPane) {
		this.setMainTabPane = mainTabPane;
	}
	
	/**
	 * 
	 * @param semester
	 */
	public void setSemester(Semester semester) {
		this.semester = semester;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}


	




}
