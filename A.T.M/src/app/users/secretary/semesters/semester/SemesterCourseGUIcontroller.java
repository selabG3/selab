package app.users.secretary.semesters.semester;



import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.common.Class;
import app.common.Course;
import app.common.Semester;
import app.comparators.ClassComparator;
import app.comparators.CourseComparator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.text.Text;

public class SemesterCourseGUIcontroller implements Initializable{

	
	//**********
	
	
    private ObservableList<Course> SemesterCourses_OL_C;//Data for SemesterCourses_OL_S.
	private ArrayList<Class> Classes = new ArrayList<Class>();//Storing classes.
	private ArrayList<Course> Courses  = new ArrayList<Course>();//Storing all courses.
    private ObservableList<Class> AvailableClasses_OL;//String all available classes.
    private ObservableList<Class> CourseClasses_OL;//String all course's classes.
    private Course SelectedCourse;
    private AbstractSecretarySemesterGUI abstractSemesterGUI;
    private Semester newSemester;
    
    //***********
    

    @FXML
    private Text txtCourseNumber;
    @FXML
    private ComboBox<Course> cbCourseList;
    @FXML
    private TableView<Class> tvClasses2;
    @FXML
    private TableView<Class> tvClasses1;
    @FXML
    private TableColumn<Class, String> tcAvaileableClasses;
    @FXML
    private TableColumn<Class, String> tcCourseClasses;
    @FXML
    private Button btnAssign;
    @FXML
    private Button btnReassign;
    @FXML
    private Button btnSaveDetails;
    
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		txtCourseNumber.setText("");
		
		cbCourseList.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				SelectedCourse = cbCourseList.getSelectionModel().getSelectedItem();//Get the course object.
				txtCourseNumber.setText(SelectedCourse.getCourseID());
				AvailableClasses_OL = FXCollections.observableArrayList(Classes);
				CourseClasses_OL = FXCollections.observableArrayList(SelectedCourse.getClassList());
				for(Class existClass : CourseClasses_OL)
					AvailableClasses_OL.remove(existClass);
				
				AvailableClasses_OL.sort(new ClassComparator());
				CourseClasses_OL.sort(new ClassComparator());
				
				tvClasses1.getItems().clear();
				tvClasses2.getItems().clear();
				tvClasses1.setItems(AvailableClasses_OL);
				tvClasses1.sort();
				tvClasses2.setItems(CourseClasses_OL);
				tvClasses2.sort();
			}
		});
		
		
		//Set models for all the cellData.
		tcAvaileableClasses.setCellValueFactory(cellData-> cellData.getValue().getFullClassNameProperty());
		tcCourseClasses.setCellValueFactory(cellData-> cellData.getValue().getFullClassNameProperty());
		
		//Set selection mode for the tables view.
		tvClasses1.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		tvClasses2.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		

	}
	
	
	//********PRIVATE CLASS METHODS************
	/**
	 * Sends details for the third scene.
	 * Builds the tree table view in the fourth scene.
	 */
	@FXML
	private void saveDetailsNewSemester(){
		abstractSemesterGUI.sentToThirdSceneDetails();
		abstractSemesterGUI.buildTreeRootsItems();
	}
    
	/**
	 * Adds all selected classes from available classes in table view,
	 * into the table view of Course's classes.
	 * After that, adds the classes into the Course class,
	 * and removes from the selected classes from the available classes table view.
	 * In the end 
	 */
	@FXML
	private void addClassToCourse(){
		CourseClasses_OL.addAll(tvClasses1.getSelectionModel().getSelectedItems());
		for(Class newClass : tvClasses1.getSelectionModel().getSelectedItems()){
			SelectedCourse.addNewClass(newClass);//Adds the class.
			SelectedCourse.getTehacerList().add(null);//Save a spot for one teacher for class.
			newClass.addCourse(SelectedCourse);//Adds the course.
			newClass.getTeachersList().add(null);//Save a spot for one teacher for course.
			//Saving information into newSemester.
			newSemester.addClass(newClass);
			newSemester.addCourses(SelectedCourse);
		}
		AvailableClasses_OL.removeAll(tvClasses1.getSelectionModel().getSelectedItems());
		tvClasses1.refresh();
		tvClasses2.refresh();
		tvClasses2.sort();
	}
	
		
	/**
	 * 
	 */
	@FXML
	private void removeClassesFromCourse(){
		AvailableClasses_OL.addAll(tvClasses2.getSelectionModel().getSelectedItems());//Adds the selected classes into available class. 
		for(Class c : tvClasses2.getSelectionModel().getSelectedItems()){
			SelectedCourse.removeClass(c);
			c.getCoursetList().remove(SelectedCourse);
		}
		if(SelectedCourse.getClassList().size()==0)
			newSemester.getCoursesList().remove(SelectedCourse);
		CourseClasses_OL.removeAll(tvClasses2.getSelectionModel().getSelectedItems());//Removes selected classes.
		tvClasses2.refresh();
		tvClasses1.refresh();
		tvClasses1.sort();
	}
	
	
	/**
	 * Build list of all courses in the database for the choiceBox of courses.
	 * An ObservableList.
	 */
	private void buildCourseListString(){
		if(!Courses.isEmpty()){
			SemesterCourses_OL_C = FXCollections.observableArrayList(Courses);
			SemesterCourses_OL_C.sort(new CourseComparator(true));//True --> Sort by name | False --> Sort by ID.
			cbCourseList.setItems(SemesterCourses_OL_C);
		}
	}
	

	//********PUBLIC CLASS METHODS***********
	
	/**
	 * 
	 */
	public void buildLists(){
		//Building the list.
		buildCourseListString();
	}
	
	/**
	 * 
	 * @param classes
	 */
	public void setClasses(ArrayList<Class> classes) {
		this.Classes = classes;
	}
	
	/**
	 * 
	 * @param courses
	 */
	public void setCourses(ArrayList<Course> courses) {
		this.Courses = courses;
	}

	/**
	 * 
	 * @param openNewSemesterGUIcontroller
	 */
	public void setAbstractSemesterGUI(AbstractSecretarySemesterGUI abstractSemesterGUI) {
		this.abstractSemesterGUI = abstractSemesterGUI;
	}

	/**
	 * 
	 * @param newSemester
	 */
	public void setNewSemester(Semester newSemester) {
		this.newSemester = newSemester;
	}
}