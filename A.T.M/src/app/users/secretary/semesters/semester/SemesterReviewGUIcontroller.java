package app.users.secretary.semesters.semester;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.Main;
import app.common.Class;
import app.common.Course;
import app.common.Semester;
import app.common.Student;
import app.common.Teacher;
import app.common.sRequests;
import app.users.secretary.requests.NewRequestsGUIcontroller;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.stage.Stage;

public class SemesterReviewGUIcontroller implements Initializable{

	
	//***************
	
	
	private AbstractSecretarySemesterGUI abstractSemesterGUI;
	private Semester newSemester;
	private ArrayList<sRequests> newStudentRequests = new ArrayList<sRequests>();
	private ArrayList<String> Queries = new ArrayList<String>();
	private LocalDate date = LocalDate.now(); 
	private boolean finshedEdit = false;
	
	//*****************
	
	
    @FXML
    private TreeTableColumn<String, String> ttcCourse;
    @FXML
    private TreeTableColumn<String, String> ttcTeacher;
    @FXML
    private TreeTableColumn<String, String> ttcClass;
    @FXML
    private TreeTableView<String> ttvSemesterDetails; 
    @FXML
    private Button btnFinished;

    
    //***************CLASS PUBLIC METHODS***************************
    
	/**
	 * 
	 * @param openNewSemesterGUIcontroller
	 */
	public void setAbstractSemesterGUI (AbstractSecretarySemesterGUI abstractSemesterGUI) {
		this.abstractSemesterGUI = abstractSemesterGUI;
	}
	
	/**
	 * 
	 * @param newSemester
	 */
	public void setNewSemesterDetails(Semester newSemester) {
		this.newSemester = newSemester;
	}
    
    
	//***************CLASS PRIVATE METHODS********************
    
	
	protected void buildTreeRootsItems() {
		if (newSemester != null) {
			//Resting the root each time saving data.
			TreeItem<String> root = new TreeItem<String>("Semester");
			TreeItem<String> course;
			TreeItem<String> classe;
			TreeItem<String> teacher;
			// For each course in semester
			for (Course semesterCourse : newSemester.getCoursesList()) {
				if (semesterCourse != null) {
					// Make course as root.
					course = new TreeItem<String>(semesterCourse.toString());
					// For each course's classes.
					for (Class semesterClass : semesterCourse.getClassList()) {
						if (semesterClass != null) {
							// Add students into course for each course's class
							semesterCourse.getStudentList().addAll(semesterClass.getStudentList());
							// For each student in class
							for (Student semesterStudent : semesterClass.getStudentList())
								// Add student into new semester.
								newSemester.addStudent(semesterStudent);
							// Make class as root's child.
							classe = new TreeItem<String>(semesterClass.toString());
							// Make the course's class's teacher as a child of
							// the child of the root.
							if (semesterClass.getTeachersList()
									.get(semesterClass.getCoursetList().indexOf(semesterCourse)) != null) {
								teacher = new TreeItem<String>(semesterClass.getTeachersList()
										.get(semesterClass.getCoursetList().indexOf(semesterCourse)).toString());
								classe.getChildren().add(teacher);
							}
							course.getChildren().add(classe);
						}
					}
					root.getChildren().add(course);
				}
			}

			ttvSemesterDetails.setRoot(root);
			ttvSemesterDetails.setShowRoot(false);
		}
	}
	
	@FXML
	private void finishSemester(){
		if(newSemester!=null)
			if((newSemester.getSemesterType()!=null)||(newSemester.getStartDate()!=null)||(newSemester.getEndDate()!=null)){
				if(!finshedEdit){
					abstractSemesterGUI.hideError();
					checkStudentsForPrevTerms();
			 		buildNewSemesterQueires();
					openStudentsRequests();
					Main.getDbController().sendInsertAndUpdate(Queries);
					finshedEdit=true;
					btnFinished.setDisable(true);
				}
			//After that the cannot go back.
			abstractSemesterGUI.finished();
			}
			else
				abstractSemesterGUI.showError();
	}
	
	/**
	 * 
	 */
	private void buildNewSemesterQueires(){

		String query;
		
		/*Delete any previous data that will preventing any conflict.*/
		//course_semester
		query = new String();
		query = "DELETE FROM course_semester WHERE CouSemSemesterID='"+newSemester.getSemesterID()+"'";
		Queries.add(query);
		//class_semester
		query = new String();
		query="DELETE FROM class_semester WHERE classSemSemID='"+newSemester.getSemesterID()+"'";
		Queries.add(query);
		//teacher_semester
		query = new String();
		query="DELETE FROM teacher_semester WHERE TSemSemID='"+newSemester.getSemesterID()+"'";
		Queries.add(query);
		//student_semester
		query = new String();
		query="DELETE FROM student_semester WHERE sID='"+newSemester.getSemesterID()+"'";
		Queries.add(query);
		//student_course
		query = new String();
		query="DELETE FROM student_course WHERE stdCouSemesterID='"+newSemester.getSemesterID()+"'";
		Queries.add(query);
		//teacher_course
		query = new String();
		query="DELETE FROM teacher_course WHERE semID='"+newSemester.getSemesterID()+"'";
		Queries.add(query);
		//class_course
		query = new String();
		query="DELETE FROM class_course WHERE clsCrsSemID='"+newSemester.getSemesterID()+"'";
		Queries.add(query);
		//student_class
		query = new String();
		query="DELETE FROM student_class WHERE stdClassSemID='"+newSemester.getSemesterID()+"'";
		Queries.add(query);
		//teacher_class_semester
		query = new String();
		query="DELETE FROM teacher_class_semester WHERE sID='"+newSemester.getSemesterID()+"'";
		Queries.add(query);
		
		query = "UPDATE semesters SET SemesterType='"+newSemester.getSemesterType()+"' , SemesterStartDate='"+newSemester.getStartDate()+"',SemesterEndDate = '"+newSemester.getEndDate()+"',Year = '"+newSemester.getYear()+"' WHERE SemesterID = '"+newSemester.getSemesterID()+"'";
		Queries.add(query);
		//For each course into: course_semester
		for(Course cs : newSemester.getCoursesList()){
			query = new String();
			query = "INSERT INTO course_semester () VALUES ('"+cs.getCourseID()+"','"+newSemester.getSemesterID()+"')";
			Queries.add(query);
		}
		//For each class into: class_semester
		for(Class cs : newSemester.getClassesList()){
			query = new String();
			query ="INSERT INTO class_semester () VALUES ('"+cs.getClassNumber()+"','"+cs.getClassName()+"','"+newSemester.getSemesterID()+"')";
			Queries.add(query);
		}
		//For each teacher into: teacher_semester
		for(Teacher ts : newSemester.getTeachrsList()){
			query = new String();
			query ="INSERT INTO teacher_semester () VALUES ('"+ts.getID()+"','"+newSemester.getSemesterID()+"')";
			Queries.add(query);
		}
		//For each student into: student_semester
		for(Student ss : newSemester.getStudentsList()){
			query = new String();
			query = "INSERT INTO student_semester () VALUES ('"+ss.getID()+"','"+newSemester.getSemesterID()+"')";
			Queries.add(query);
		}
		
		for(Course sc : newSemester.getCoursesList()){
			//For each student into: student_course
			for(Student ss : newSemester.getStudentsList()){
				query = new String();
				query = "INSERT INTO student_course (stdCouCourseID,stdCouStudentID,stdCouSemesterID) VALUES ('"+sc.getCourseID()+"','"+ss.getID()+"','"+newSemester.getSemesterID()+"')";
				Queries.add(query);
			}
			//For each teacher into: teacher_course
			for(Teacher ts : newSemester.getTeachrsList()){
				query = new String();
				query ="INSERT INTO teacher_course () VALUES ('"+ts.getID()+"','"+sc.getCourseID()+"','"+newSemester.getSemesterID()+"')";
				Queries.add(query);
			}

		}
		
		for(Class semClass : newSemester.getClassesList()){
			//For each class learns a course into: class_course
			for(Course classCourse : semClass.getCoursetList()){
				query = new String();
				query ="INSERT INTO class_course () VALUES ('"+semClass.getClassNumber()+"','"+semClass.getClassName()+"','"+newSemester.getSemesterID()+"','"+classCourse.getCourseID()+"','"+classCourse.getStudentList().size()+"')";
				Queries.add(query);
			}
			//For each class his students into : student_class
			for(Student student : semClass.getStudentList()){
				query = new String();
				query = "INSTER INTO student_class () VALUES ('"+semClass.getClassNumber()+"','"+semClass.getClassName()+"','"+student.getID()+"','"+newSemester.getSemesterID()+"')";
				Queries.add(query);
			}			
		}
		
		//For each teacher's class's and the course into : teacher_class_semester
		for(Teacher teacher : newSemester.getTeachrsList()){
			int maxClasses = teacher.getTeacheringClasses().size();
			for (int index = 0; index<maxClasses ; index++){
				Course teachCourse = teacher.getTeacheringCourses().get(index);
				Class techClass = teacher.getTeacheringClasses().get(index);
				if((teachCourse!=null)&&(techClass!=null)){
					query = new String();
					query = "INSERT INTO teacher_class_semester () VALUES ('"+teacher.getID()+"','"+techClass.getClassNumber()+"','"+techClass.getClassName()+"','"+newSemester.getSemesterID()+"','"+teachCourse.getCourseID()+"')";
					Queries.add(query);
				}
			}
		}
	}

	private void openStudentsRequests() {
		if(!newStudentRequests.isEmpty())
			try {
			   	FXMLLoader loader = new FXMLLoader();
				Parent StudentRequest;
				StudentRequest = loader.load(Main.class.getResource("users/secretary/requests/NewRequestsGUI.fxml").openStream());
				NewRequestsGUIcontroller newRequestsGUIcontroller = (NewRequestsGUIcontroller) loader.getController();
				newRequestsGUIcontroller.setStudentsRequests(newStudentRequests);
				newRequestsGUIcontroller.setNewSemester(newSemester);
				newRequestsGUIcontroller.buildData();
				Scene root = new Scene(StudentRequest);
				Stage newStage = new Stage();
				newStage.setScene(root);
				newRequestsGUIcontroller.setStage(newStage);
				newStage.show();
			} catch (IOException e) {
				e.printStackTrace();
			}
	
	}

	private void checkStudentsForPrevTerms(){
		sRequests newStudentRequest;
		for(Class semesterClass : newSemester.getClassesList()){
			for(Course classCourse : semesterClass.getCoursetList()){
				for(Course preCourse : classCourse.getPrevCoursesList()){
					int maxPrevCourse = classCourse.getPrevCoursesList().size();
					int index;
					for(Student classStudent : semesterClass.getStudentList()){
						for(index = 0 ; index<maxPrevCourse ; index ++){
							if(!classStudent.getCourses().contains(preCourse)){
								newStudentRequest = new sRequests(classStudent.getID(), classCourse.getCourseID(),classCourse.getCourseName(),date.toString());
								newStudentRequest.setsFullName(classStudent.getFullName());
								newStudentRequests.add(newStudentRequest);
								//Removing from the student the course.
								classStudent.getCourses().remove(classCourse);
								classCourse.getStudentList().remove(classStudent);
							}
						}
					}
				}
			}
		}
		
	}


	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ttcCourse.setCellValueFactory(cellData-> new SimpleStringProperty(cellData.getValue().getValue()));
		ttcClass.setCellValueFactory(cellData-> new SimpleStringProperty(cellData.getValue().getValue()));
		ttcTeacher.setCellValueFactory(cellData-> new SimpleStringProperty(cellData.getValue().getValue()));
		buildTreeRootsItems();

	}

	
}
