package app.users.secretary.semesters.semester.current;

import java.util.ArrayList;

import app.Main;
import app.common.Class;
import app.common.Course;
import app.common.Dept;
import app.common.Semester;
import app.common.Student;
import app.common.Teacher;
import app.users.secretary.semesters.semester.AbstractSecretarySemesterGUI;
import javafx.fxml.Initializable;

public class CurrentSemesterGUIcontroller extends AbstractSecretarySemesterGUI implements Initializable {

	
	private ArrayList<Object[][]> ResultsList;//All the Results from the Queries.
    private ArrayList<Course> Courses = new ArrayList<Course>();//Storing all courses.
    private ArrayList<Teacher> Teachers = new ArrayList<Teacher>();//Storing all teachers.
    private ArrayList<Class> Classes = new ArrayList<Class>();//Storing all classes.
    private ArrayList<Student> Students = new ArrayList<Student>();//String all students.
	private Semester newSemester;//Save all details for the new semester.
	private String[] queries = {"SELECT c.courseID , c.departmentID , d.deptName , c.courseName , c.studyHoursPerWeek FROM courses c , depts d WHERE c.departmentID=d.deptID"
			,"SELECT * FROM course_course"
			,"SELECT u.uID , u.uFirstName , u.uLastName FROM teachers t , users u WHERE u.uID=t.teacherID"
			,"SELECT t.teacherID , td.TDepDeptID , d.deptName FROM teachers t , teacher_dept td , depts d WHERE t.teacherID=td.TDepTeacherID AND td.TDepDeptID=d.deptID"
			,"SELECT * FROM  classes"
			,"SELECT u.uID , u.uFirstName , u.uLastName FROM users u , students s WHERE s.studentID=u.uID"
			,"SELECT sc.stdCouCourseID , s.studentID FROM students s , student_course sc WHERE s.studentID = sc.stdCouStudentID AND  sc.grade>=55"
			,"SELECT * FROM student_class sc ORDER BY sc.stdClassClassID"
			,"SELECT * FROM semesters s WHERE s.Current=1"
			,"SELECT tcs.tID , tcs.cID , tcs.cName , tcs.courID FROM teacher_class_semester tcs , semesters s WHERE tcs.sID=s.SemesterID AND s.Current='1'"
			,"SELECT sc.stdCouStudentID ,sc.stdCouCourseID , stdc.stdClassClassID ,stdc.stdClassClassName  FROM student_course sc , semesters s  ,student_class stdc WHERE stdc.stdClassStudentID=sc.stdCouStudentID AND sc.stdCouSemesterID=s.SemesterID AND s.Current='1'"
	};
	
	/**
	 * Ask from the Server and the Database for all the information that
	 * the user will need to be able to open a new semester.
	 */
    public void getData(){
		//Gets all the details that need to build a new semesters.
		Main.getDbController().getAllDetailsForNewSemester(this,queries);		
    }
    

    
    /**
	 * Build from all the Results that got from the Database
	 * a list of classes of each type.
	 * The whole details that need for a new semester.
	 * Courses , Classes , Teachers , Students...
	 */
    public void buildData(){
		ResultsList = Main.getDbController().getResultsList();
		int maxRows,index;
		if(ResultsList!=null)
			if(ResultsList.size()!=0){
		Course course,prevCourse;
		Teacher teacher;
		Dept dept;
		Class newClass;
		Student student;
		
		//Build Course List.
		if(ResultsList.get(0)[0][0]!=null)
		{
		maxRows = ResultsList.get(0).length;
		for(index=0 ; index<maxRows ; index++){
			course = new Course(ResultsList.get(0)[index][0].toString());
			course.setCourseDeptID(ResultsList.get(0)[index][1].toString());
			course.setCourseDeptName(ResultsList.get(0)[index][2].toString());
			course.setCourseName(ResultsList.get(0)[index][3].toString());
			course.setHoursPerWeek(ResultsList.get(0)[index][4].toString());
			Courses.add(course);
			}
		}
		setCourses(Courses);
		
		
		//Build Previous Course's Courses.
		if(ResultsList.get(1)[0][0]!=null)
		{
		maxRows = ResultsList.get(1).length;
		for(index=0 ; index<maxRows ; index++){
			prevCourse = Courses.get(SearchCourseIndex(ResultsList.get(1)[index][1].toString()));
			int CourseIndex = SearchCourseIndex(ResultsList.get(1)[index][0].toString());
			if(CourseIndex>=0)
				Courses.get(CourseIndex).addNewPrevCourse(prevCourse);
		}
		}

		
		//Build Teachers Info.
		if(ResultsList.get(2)[0][0]!=null)
		{
		maxRows = ResultsList.get(2).length;
		for(index=0 ; index<maxRows ; index++){
			teacher = new Teacher(ResultsList.get(2)[index][0].toString());
			teacher.setFirstName(ResultsList.get(2)[index][1].toString());
			teacher.setLastName(ResultsList.get(2)[index][2].toString());
			Teachers.add(teacher);
		}
		}
		setTeachers(Teachers);
		
		
		//Build Teacher's departments.
		if(ResultsList.get(3)[0][0]!=null)
		{
		maxRows = ResultsList.get(3).length;
		for(index=0 ; index<maxRows ; index++){
			teacher = Teachers.get(SearchTeacherIndex(ResultsList.get(3)[index][0].toString()));
			dept = new Dept(ResultsList.get(3)[index][1].toString());
			dept.setDeprtName(ResultsList.get(3)[index][2].toString());
			teacher.addNewDept(dept);
		}
		}
		
		//Build Classes.
		if(ResultsList.get(4)[0][0]!=null)
		{
		int maxClasses = ResultsList.get(4).length;
		for(index=0 ; index<maxClasses ; index++){
			newClass = new Class(ResultsList.get(4)[index][0].toString());
			newClass.setClassName(ResultsList.get(4)[index][1].toString());
			Classes.add(newClass);
		}
		}
		setClasses(Classes);
		
		
		//Build Students.
		if(ResultsList.get(5)[0][0]!=null)
		{
		maxRows = ResultsList.get(5).length;
		for(index=0 ; index<maxRows ; index++){
			student = new Student(ResultsList.get(5)[index][0].toString());
			student.setFirstName(ResultsList.get(5)[index][1].toString());
			student.setLastName(ResultsList.get(5)[index][2].toString());
			Students.add(student);
		}
		}
		setStudents(Students);
		
		
		//Build Student's pass Courses.
		if(ResultsList.get(6)[0][0]!=null)
		{
			maxRows = ResultsList.get(6).length; //All pass courses of all students.
			for(index=0 ; index<maxRows ; index++){
				prevCourse = Courses.get(SearchCourseIndex(ResultsList.get(6)[index][0].toString()));
				Students.get(SearchStudentIndex(ResultsList.get(6)[index][1].toString())).addCourse(prevCourse);
			}
		}
		
		//Build Class's Students.
		if(ResultsList.get(7)[0][0]!=null)
		{
		maxRows = ResultsList.get(7).length;
		for(index=0 ; index<maxRows ; index++){
			int studentIndex = SearchStudentIndex(ResultsList.get(7)[index][2].toString());
			if(studentIndex!=-1){
				student = Students.get(studentIndex);
				Classes.get(SearchClassIndex(ResultsList.get(7)[index][0].toString(),ResultsList.get(7)[index][1].toString())).addStudent(student);
			}
		}
		}
		
		//Build Semester Details
		if(ResultsList.get(8)[0][0]!=null)
		{
			newSemester = new Semester();
			newSemester.setSemesterID(String.valueOf(ResultsList.get(8)[0][0]));
			newSemester.setSemesterType(ResultsList.get(8)[0][1].toString());
			newSemester.setStartDate(ResultsList.get(8)[0][2].toString());
			newSemester.setEndDate(ResultsList.get(8)[0][3].toString());
			newSemester.setYear(ResultsList.get(8)[0][4].toString());
		}
		
		//Build each teacher his class and the class's course.
		if(ResultsList.get(9)[0][0]!=null)
		{
			maxRows =  ResultsList.get(9).length;
			for(index = 0 ; index<maxRows ; index++){
				teacher = Teachers.get(SearchTeacherIndex(ResultsList.get(9)[index][0].toString()));
				newClass = Classes.get(SearchClassIndex(ResultsList.get(9)[index][1].toString(), ResultsList.get(9)[index][2].toString()));
				course = Courses.get(SearchCourseIndex(ResultsList.get(9)[index][3].toString()));
				//Adds details for teacher.
				teacher.addClass(newClass);
				teacher.addCourse(course);
				//Adds details for class.
				newClass.addCourse(course);
				newClass.addTeacher(teacher);
				//Adds details for course.
				course.addNewClass(newClass);
				course.addTeacher(teacher);
				//Adds details into semester.
				newSemester.addClass(newClass);
				newSemester.addCourses(course);
				newSemester.addTeacher(teacher);
			}
		}
		
		//Build each student his courses and class.
		if(ResultsList.get(10)[0][0]!=null){
			maxRows = ResultsList.get(10).length;
			for(index = 0 ; index<maxRows ; index++){
				student = Students.get(SearchStudentIndex(ResultsList.get(10)[index][0].toString()));
				course = Courses.get(SearchCourseIndex(ResultsList.get(10)[index][1].toString()));
				newClass = Classes.get(SearchClassIndex(ResultsList.get(10)[index][2].toString(), ResultsList.get(10)[index][3].toString()));
				//Adds details for student.
				student.addClass(newClass);
				student.addCourse(course);
				//Adds details for course.
				course.addStudent(student);
				course.addNewClass(newClass);
				//Adds details for class.
				newClass.addStudent(student);
				newClass.addCourse(course);
				//Adds details into semester.
				newSemester.addStudent(student);
			}
		}
		
		
		
		//Builds all the scenes for the user.
		setSemester(newSemester);
		buildScenes();
		showSemesterDetails();
		}
    }
    
	/**
	 * Search for a class if exists by Number and Name
	 * @param classID
	 * @param className
	 * @return Class index in Classes List.
	 */
    protected int SearchClassIndex(String classID,String className){
    	return super.SearchClassIndex(classID, className);
	}
	
	/**
	 * Search for a course if exists by ID.
	 * @param courseID
	 * @return Course index in Courses List.
	 */
	protected int SearchCourseIndex(String courseID){
		return super.SearchCourseIndex(courseID);
	}
	
	/**
	 * Search for a teacher if exists by ID.
	 * @param teacherID
	 * @return	Teacher index in Teachers List.
	 */
	protected int SearchTeacherIndex(String teacherID){
		return super.SearchTeacherIndex(teacherID);
	}
	
	/**
	 * Search for a student if exists by ID.
	 * @param studentID
	 * @return Student index in Students List.
	 */
	protected int SearchStudentIndex(String studentID){
		return super.SearchStudentIndex(studentID);
	}

    
}
