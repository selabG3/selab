package app.users.secretary.classes;

import java.util.ArrayList;

import app.Main;
import app.common.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class OpenNewClassGUIcontroller {

	private ObservableList<User> TableData;
	private ObservableList<User> ClassTable;
	private SortedList<User> sortedData;
	private ArrayList<Object[][]> ResultsList;
	private ArrayList<String> queries = new ArrayList<String>();;
	private String studentName, StudentID,SemID;
	private int Idx;
	// 1 - a student has been selected to be removed, 0 - a student has been
	// selected to be added
	private int RemoveORAddFlag;


	@FXML
	private TableColumn<User, String> sIdCol;

	@FXML
	private Button saveClsBtn;

	@FXML
	private Button remvSelBtn;

	@FXML
	private TextField clsNum;

	@FXML
	private TableColumn<User, String> sFullNameCol;

	@FXML
	private Button impBtn;

	@FXML
	private TableView<User> curClsTable;

	@FXML
	private TextField sidSchField;

	@FXML
	private TextField clsName;

	@FXML
	private TableColumn<User, String> sFullNameSchCol;

	@FXML
	private Button addStdBtn;


	@FXML
	private TableView<User> searchTable;

	@FXML
	private TableColumn<User, String> sIdSchCol;

	@FXML
	private Text curStudents;

	@FXML
	/**
	 * creates queries to create a new class and assign students to it
	 */
	private void AddClass(){
		
		
		//Insert query, new class into DB
		String tmp = new String("INSERT into classes (ClassNum,ClassName,numOfStudents) values"
								 + "(\"" + clsNum.getText() + "\",\"" + clsName.getText() + "\",\"" + curStudents.getText()
								 + "\")");
		queries.add(tmp);
		
		//assign students to class
		for(int i =0; i< ClassTable.size();i++){
			tmp = new String ("INSERT into student_class (stdClassClassID,stdClassClassName,stdClassStudentID,stdClassSemID) values"
							   + "(\"" + clsNum.getText() + "\",\"" + clsName.getText() + "\",\"" + ClassTable.get(0).getID() +
						       "\",\""+SemID+"\")");
			queries.add(tmp);
			}
		Main.getDbController().updateDBAfterRequests(queries, this);
	}
	
	@FXML
	/**
	 * removes a student which has been selected from the left table back into
	 * the right table
	 */
	private void RemoveStudentFromClass() {

		if (RemoveORAddFlag == 1) {
			removeStudentFromTable("ClassTable");

			int idx = studentName.indexOf(" ");
			String fName = studentName.substring(0, idx);
			String lName = studentName.substring(idx + 1);

			User tmp = new User(StudentID, fName, lName);
			TableData.add(tmp);
			curStudents.setText(String.valueOf(curClsTable.getItems().size()));
		}

	}

	@FXML
	/**
	 * adds a student which was selected from right table into the new class
	 */
	private void AddStudentToClass() {

		if (RemoveORAddFlag == 0) {
			removeStudentFromTable("TableData");

			int idx = studentName.indexOf(" ");
			String fName = studentName.substring(0, idx);
			String lName = studentName.substring(idx + 1);

			User tmp = new User(StudentID, fName, lName);
			ClassTable.add(tmp);
			curStudents.setText(String.valueOf(curClsTable.getItems().size()));
		}

	}

	@FXML
	private void initialize() {

		// Student search table
		TableData = FXCollections.observableArrayList();
		// Students in class table
		ClassTable = FXCollections.observableArrayList();

		// set in the right table(Search Table)
		sIdSchCol.setCellValueFactory(cellData -> cellData.getValue().getIdProperty());
		sFullNameSchCol.setCellValueFactory(cellData -> cellData.getValue().getFullNameProperty());

		// set in the left table(Class Table)
		sIdCol.setCellValueFactory(cellData -> cellData.getValue().getIdProperty());
		sFullNameCol.setCellValueFactory(cellData -> cellData.getValue().getFullNameProperty());

		curClsTable.setItems(ClassTable);
		//searchTable.setItems(TableData);

		// Wrap the ObservableList in a FilteredList (initially display all data).
		FilteredList<User> filteredData = new FilteredList<>(TableData, u -> true);
		
        // Set the filter Predicate whenever the filter changes.
		sidSchField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(user -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty())
                    return true;

                // Compare id with Filter
                String Filter = newValue.toString();

                if (user.getID().contains(Filter)) {
                    return true; // Filter matches
                }
                return false; // Does not match.
            });
        });
		
        // Wrap the FilteredList in a SortedList. 
        sortedData = new SortedList<>(filteredData);

        // Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(searchTable.comparatorProperty());

        // Add sorted (and filtered) data to the table.
        searchTable.setItems(sortedData);

		GetInfo();

		// Add Events.
		searchTable.setOnMousePressed(new EventHandler<Event>() {

			// ResultsList.get(i)
			// 0 - all students which aren't assigned to any class
			// 1 - current semester ID

			@Override
			public void handle(Event arg0) {
				try {
					if (searchTable.getSelectionModel().getSelectedItems().get(0).getID() != null) {
						studentName = searchTable.getSelectionModel().getSelectedItems().get(0).getFullName();
						StudentID = searchTable.getSelectionModel().getSelectedItems().get(0).getID();
						Idx = searchTable.getSelectionModel().getSelectedIndex();
						RemoveORAddFlag = 0;

					}

				} catch (NullPointerException e) {
				}
			}
		});

		curClsTable.setOnMousePressed(new EventHandler<Event>() {

			// ResultsList.get(i)
			// 0 - all students which aren't assigned to any class
			// 1 - current semester ID

			@Override
			public void handle(Event arg0) {
				try {
					if (curClsTable.getSelectionModel().getSelectedItems().get(0).getID() != null) {
						studentName = curClsTable.getSelectionModel().getSelectedItems().get(0).getFullName();
						StudentID = curClsTable.getSelectionModel().getSelectedItems().get(0).getID();
						Idx = curClsTable.getSelectionModel().getSelectedIndex();
						RemoveORAddFlag = 1;
					}

				} catch (NullPointerException e) {
				}
			}
		});
	}

	@FXML
	/**
	 * gets all info needed to create a new class from database
	 */
	public void GetInfo() {

		Main.getDbController().getNewClassInfo(this);


	}

	public void buildData() {

		ResultsList = Main.getDbController().getResultsList();

		if (ResultsList.get(0)[0][0] == null)
			return;

		// ResultsList.get(i)
		// 0 - data about students which aren't assigned to any class
		// 1 - current semester ID

		// save data in User class for table
		for (int i = 0; i < ResultsList.get(0).length; i++) {

			String sID = new String(ResultsList.get(0)[i][0].toString());
			String sFirstName = new String(ResultsList.get(0)[i][1].toString());
			String sLastName = new String(ResultsList.get(0)[i][2].toString());
			TableData.add(new User(sID, sFirstName, sLastName));
		}

		// sets SemID
		SemID = new String(ResultsList.get(1)[0][0].toString());

	}

	public void removeStudentFromTable(String Table) {

		if (Table.equals("TableData")){
			for(int i=0;i<TableData.size();i++)
				if(TableData.get(i).getID().equals(StudentID))
					TableData.remove(i);
			}
		else if (Table.equals("ClassTable"))
			ClassTable.remove(Idx);
		
		RemoveORAddFlag = -1;

	}

	public void clearQueryList() {
		
		queries.clear();
	}
}
