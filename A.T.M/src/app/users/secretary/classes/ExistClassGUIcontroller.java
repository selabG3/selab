package app.users.secretary.classes;

import java.lang.reflect.Array;
import java.util.ArrayList;

import app.Main;
import app.common.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

public class ExistClassGUIcontroller {

	private ObservableList<User> TableData;
	private ObservableList<User> TableDatatmp;
	private ObservableList<User> ClassTable;
	private SortedList<User> sortedData;
	private ArrayList<Object[][]> ResultsList;
	private ArrayList<String> queries = new ArrayList<String>();
	private ArrayList<String> rmvList = new ArrayList<>();
	private ArrayList<String> addList = new ArrayList<>();
	private String studentName, StudentID, SemID;
	private int Idx;
	private String curClsName, curClsNum;
	// 1 - a student has been selected to be removed, 0 - a student has been
	// selected to be added
	private int RemoveORAddFlag = -1;

	@FXML
	private Button schBtn;

	@FXML
	private TextField clsNameSch;

	@FXML
	private TableView<User> schStd;

	@FXML
	private Button svCls;

	@FXML
	private TextField filterField;

	@FXML
	private Button addBtn;

	@FXML
	private TableColumn<User, String> sId;

	@FXML
	private Text studsNumClass;

	@FXML
	private TableColumn<User, String> fName;

	@FXML
	private TableColumn<User, String> fNameSch;

	@FXML
	private TableColumn<User, String> sIdSch;

	@FXML
	private TextField clsNumSch;

	@FXML
	private Button rmvBtn;

	@FXML
	private TableView<User> curCls;

	@FXML
	private Text clsStat;

	// TODO : add if no class is selected can't remove or Add, use
	// removeORaddFlag ?
	@FXML
	private void InsertQuery() {

		if (RemoveORAddFlag == 0) {
			addList.add(StudentID);
			ClassTable.add(new User(StudentID, schStd.getSelectionModel().getSelectedItem().getFirstName(), schStd.getSelectionModel().getSelectedItem().getLastName()));
			for (int i = 0; i < TableDatatmp.size(); i++)
				if (TableDatatmp.get(i).getID().equals(StudentID))
					TableDatatmp.remove(i);
			studsNumClass.setText(String.valueOf(curCls.getItems().size()));
			RemoveORAddFlag = -1;
		}
	}

	@FXML
	private void RemoveQuery() {

		if (RemoveORAddFlag == 1) {
			rmvList.add(StudentID);
			TableDatatmp.add(new User(StudentID, curCls.getSelectionModel().getSelectedItem().getFirstName(), curCls.getSelectionModel().getSelectedItem().getLastName()));
			for (int i = 0; i < ClassTable.size(); i++)
				if (ClassTable.get(i).getID().equals(StudentID))
					ClassTable.remove(i);
			studsNumClass.setText(String.valueOf(curCls.getItems().size()));
			RemoveORAddFlag = -1;
		}
	}

	@FXML
	private void UpdateDB() {

		for (int i = 0; i < addList.size(); i++)
			queries.add("insert into student_class (stdClassClassID,stdClassClassName,stdClassStudentID) values ('"
					+ curClsNum + "','" + curClsName + "','" + addList.get(i) + "')");
		for (int i = 0; i < rmvList.size(); i++)
			queries.add("delete from student_class where stdClassStudentID = '" + rmvList.get(i) + "'");
		
		queries.add("update classes set numOfStudents =" + studsNumClass.getText() + " where ClassNum='" + curClsNum + "'" + " and ClassName='" + curClsName + "'");
		
		
		Main.getDbController().updateDBAfterRequests(queries, this);
		
	}
	

	@FXML
	private void SearchClass() {

		String clsNum = clsNumSch.getText();
		String clsName = clsNameSch.getText();
		String tmpNum, tmpName, tmpId, tmpStdfName, tmpStdlName;
		ArrayList<String> stdIds = new ArrayList<String>();

		for (int i = 0; i < ResultsList.get(2).length; i++) {
			tmpNum = ResultsList.get(2)[i][0].toString();
			tmpName = ResultsList.get(2)[i][1].toString();

			if (clsNum.equals(tmpNum) && clsName.equals(tmpName)) {
				ClassTable.clear();
				clsStat.setText("Class Found");
				clsStat.setFill(Color.GREEN);
				curClsName = clsName;
				curClsNum = clsNum;
				for (int j = 0; j < ResultsList.get(3).length; j++)
					if (curClsNum.equals(ResultsList.get(3)[j][1].toString())
							&& curClsName.equals(ResultsList.get(3)[j][2].toString()))
						stdIds.add(ResultsList.get(3)[j][0].toString());
				for (int j = 0; j < ResultsList.get(4).length; j++)
					if (stdIds.contains(ResultsList.get(4)[j][0].toString())) {

						tmpId = ResultsList.get(4)[j][0].toString();
						tmpStdfName = ResultsList.get(4)[j][1].toString();
						tmpStdlName = ResultsList.get(4)[j][2].toString();

						ClassTable.add(new User(tmpId, tmpStdfName, tmpStdlName));
					}
				break;
			} else {
				clsStat.setText("Class Not Found");
				clsStat.setFill(Color.RED);
				RemoveORAddFlag = -1;
			}
		}
		
		studsNumClass.setText(String.valueOf(curCls.getItems().size()));
		TableDatatmp.clear();
		for(int i=0;i< TableData.size();i++)
			TableDatatmp.add(TableData.get(i));

	}

	@FXML
	private void initialize() {

		// Student search table
		TableData = FXCollections.observableArrayList();
		TableDatatmp = FXCollections.observableArrayList();
		// Students in class table
		ClassTable = FXCollections.observableArrayList();

		// set in the right table(Search Table)
		sIdSch.setCellValueFactory(cellData -> cellData.getValue().getIdProperty());
		fNameSch.setCellValueFactory(cellData -> cellData.getValue().getFullNameProperty());

		// set in the left table(Class Table)
		sId.setCellValueFactory(cellData -> cellData.getValue().getIdProperty());
		fName.setCellValueFactory(cellData -> cellData.getValue().getFullNameProperty());

		curCls.setItems(ClassTable);
		// searchTable.setItems(TableData);

		// Wrap the ObservableList in a FilteredList (initially display all
		// data).
		FilteredList<User> filteredData = new FilteredList<>(TableDatatmp, u -> true);

		// Set the filter Predicate whenever the filter changes.
		filterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(user -> {
				// If filter text is empty, display all persons.
				if (newValue == null || newValue.isEmpty())
					return true;

				// Compare id with Filter
				String Filter = newValue.toString();

				if (user.getID().contains(Filter)) {
					return true; // Filter matches
				}
				return false; // Does not match.
			});
		});

		// Wrap the FilteredList in a SortedList.
		sortedData = new SortedList<>(filteredData);

		// Bind the SortedList comparator to the TableView comparator.
		sortedData.comparatorProperty().bind(schStd.comparatorProperty());

		// Add sorted (and filtered) data to the table.
		schStd.setItems(sortedData);

		GetInfo();

		// Add Events.
		schStd.setOnMousePressed(new EventHandler<Event>() {

			// ResultsList.get(i)
			// 0 - all students which aren't assigned to any class
			// 1 - current semester ID

			@Override
			public void handle(Event arg0) {
				try {
					if (clsStat.getText().equals("Class Found"))
						if (schStd.getSelectionModel().getSelectedItems().get(0).getID() != null) {
							studentName = schStd.getSelectionModel().getSelectedItems().get(0).getFullName();
							StudentID = schStd.getSelectionModel().getSelectedItems().get(0).getID();
							Idx = schStd.getSelectionModel().getSelectedIndex();

							RemoveORAddFlag = 0;

						}

				} catch (NullPointerException e) {
					// Empty because i don't give a shit BOOM!!
				}
			}
		});

		curCls.setOnMousePressed(new EventHandler<Event>() {

			// ResultsList.get(i)
			// 0 - all students which aren't assigned to any class
			// 1 - current semester ID

			@Override
			public void handle(Event arg0) {
				try {
					if (curCls.getSelectionModel().getSelectedItems().get(0).getID() != null) {
						studentName = curCls.getSelectionModel().getSelectedItems().get(0).getFullName();
						StudentID = curCls.getSelectionModel().getSelectedItems().get(0).getID();
						Idx = curCls.getSelectionModel().getSelectedIndex();
						RemoveORAddFlag = 1;
					}

				} catch (NullPointerException e) {
				}
			}
		});
	}

	/**
	 * gets all info needed to create a new class from database
	 */
	public void GetInfo() {

		Main.getDbController().getAllStudentsInClass(this);

	}

	public void buildData() {

		ResultsList = Main.getDbController().getResultsList();

		if (ResultsList.get(0)[0][0] == null)
			return;

		// ResultsList.get(i)
		// 0 - returns all students which aren't assigned to any class
		// (uID,uFirstName,uLastName)
		// 1 - get current semester ID
		// (semesterID)
		// 2 - all data about current classes
		// (ClassNum,ClassName,numOfStudents);
		// 3 - all students which are assigned to a class
		// (stdClassClassID,stclassClassName,stdClassStudentID)

		// save data in User class for table
		for (int i = 0; i < ResultsList.get(0).length; i++) {

			String sID = new String(ResultsList.get(0)[i][0].toString());
			String sFirstName = new String(ResultsList.get(0)[i][1].toString());
			String sLastName = new String(ResultsList.get(0)[i][2].toString());
			User tmp = new User(sID,sFirstName,sLastName);
			TableData.add(tmp);
			TableDatatmp.add(tmp);
			
		}

		// sets SemID
		SemID = new String(ResultsList.get(1)[0][0].toString());

	}

	public void clearLists() {

		addList.clear();
		rmvList.clear();
		clsNumSch.clear();
		clsNameSch.clear();
		clsStat.setText("Class Found/Not");
		clsStat.setFill(Color.BLACK);
		RemoveORAddFlag = -1;
		ClassTable.clear();
		studsNumClass.setText("0");
		TableData.clear();
		TableDatatmp.clear();
		
		GetInfo();
	}
}
