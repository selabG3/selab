package app.users.secretary;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

import app.Main;
import app.common.Semester;
import app.users.secretary.semesters.SecretarySemestersGUIcontroller;
import app.users.secretary.semesters.semester.current.CurrentSemesterGUIcontroller;
import app.users.secretary.semesters.semester.open.OpenNewSemesterGUIcontroller;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.MouseEvent;

public class SecretaryGUImenuController implements Initializable {

	// *************FXML*************

	@FXML
	private Button newSemesterTab;
	@FXML
	private Button mainTab;
	@FXML
	private Button currentSemesterTab;
	@FXML
	private Button prevSemesterTab;
	@FXML
	private Button btnSendMail;
	@FXML
	private MenuItem requestsTab;
	@FXML
	private MenuItem tRequestsTab;
	@FXML
	private MenuItem teacherInfoTab;
	@FXML
	private MenuItem studentInfoTab;
    @FXML
    private MenuItem newClass;
    @FXML
    private MenuItem modExstClass;
	@FXML
	private MenuButton infoAboutTab;

	// ************

	private ArrayList<Semester> SemestersList = new ArrayList<Semester>();
	private TabPane mainTabPane;
	private HashMap<String, Tab> SecretaryTabs = new HashMap<String, Tab>();

	// *******INITIALIZE**********

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		this.mainTab.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				mainTabPane.getSelectionModel().select(SecretaryTabs.get("main"));
			}
		});
		
		this.newClass.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (!(SecretaryTabs.containsKey("newClass")))
					try {
						buildNewClassTab();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(SecretaryTabs.get("newClass"));
			}
		});
		
		this.modExstClass.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (!(SecretaryTabs.containsKey("ModClass")))
					try {
						buildModifyExistingClassTab();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(SecretaryTabs.get("ModClass"));
			}
		});

		this.newSemesterTab.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				if (!(SecretaryTabs.containsKey("newSemester")))
					try {
						buildNewSemesterTab();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(SecretaryTabs.get("newSemester"));
			}
		});

		this.requestsTab.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (!(SecretaryTabs.containsKey("sRequest")))
					try {
						buildStRequest();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(SecretaryTabs.get("sRequest"));
			}
		});

		this.tRequestsTab.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (!(SecretaryTabs.containsKey("tRequest")))
					try {
						buildTcRequest();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(SecretaryTabs.get("tRequest"));
			}
		});

		this.btnSendMail.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				if (!(SecretaryTabs.containsKey("sendMail")))
					try {
						buildSendMailTab();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(SecretaryTabs.get("sendMail"));
			}
		});

		this.teacherInfoTab.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (!(SecretaryTabs.containsKey("teacherInfo")))
					try {
						buildTeacherInfoTab();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(SecretaryTabs.get("teacherInfo"));
			}
		});

		this.studentInfoTab.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (!(SecretaryTabs.containsKey("studentInfo")))
					try {
						buildStudentInfoTab();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(SecretaryTabs.get("studentInfo"));
			}
		});

		this.currentSemesterTab.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (!(SecretaryTabs.containsKey("currentSemester")))
					try {
						buildCurrentSemesterTab();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(SecretaryTabs.get("currentSemester"));
			}
		});
	}

	// ***********PRIVATE CLASS METHODS*************
	
	/**
	 * load modify existing class gui FXML
	 * @throws IOException
	 */
	@FXML
	protected void buildModifyExistingClassTab() throws IOException {
		Tab newTab = new Tab("Modify Existing Class");
		FXMLLoader loader = new FXMLLoader();
		SecretaryTabs.put("ModClass", newTab);
		newTab.setOnClosed(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				SecretaryTabs.remove("ModClass");
			}
		});

		Parent newPane = loader
				.load(Main.class.getResource("users/secretary/classes/ExistClassGUI.fxml").openStream());
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
		mainTabPane.getSelectionModel().select(newTab);
	}

	/**
	 * load new class gui FXML
	 * @throws IOException
	 */
	@FXML
	private void buildNewClassTab() throws IOException {
		Tab newTab = new Tab("Create New Class");
		FXMLLoader loader = new FXMLLoader();
		SecretaryTabs.put("newClass", newTab);
		newTab.setOnClosed(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				SecretaryTabs.remove("newClass");
			}
		});

		Parent newPane = loader
				.load(Main.class.getResource("users/secretary/classes/OpenNewClassGUI.fxml").openStream());
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
		mainTabPane.getSelectionModel().select(newTab);
	}

	@FXML
	private void prevSemesters() {
		if (!(SecretaryTabs.containsKey("prevSemester")))
			Main.getDbController().getPrevSemesters(this);
		mainTabPane.getSelectionModel().select(SecretaryTabs.get("prevSemester"));
	}

	/**
	 * Builds the GUI for send mail.
	 * 
	 * @throws IOException
	 */
	private void buildSendMailTab() throws IOException {
		Tab newTab = new Tab("Send Mail");
		FXMLLoader loader = new FXMLLoader();
		SecretaryTabs.put("sendMail", newTab);
		newTab.setOnClosed(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				SecretaryTabs.remove("sendMail");
			}
		});

		Parent newPane = loader.load(Main.class.getResource("common/sendMsg/SendMsgGUI.fxml").openStream());
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
	}

	/**
	 * Builds the GUI for any information regards to a student or students.
	 * 
	 * @throws IOException
	 */
	private void buildStudentInfoTab() throws IOException {
		Tab newTab = new Tab("Student Info");
		FXMLLoader loader = new FXMLLoader();
		SecretaryTabs.put("studentInfo", newTab);
		newTab.setOnClosed(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				SecretaryTabs.remove("studentInfo");
			}
		});

		Parent newPane = loader
				.load(Main.class.getResource("users/secretary/infoAbout/InfoAboutStudentGUI.fxml").openStream());
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
	}

	/**
	 * Builds the GUI for any information regards to a teacher or students.
	 * 
	 * @throws IOException
	 */
	private void buildTeacherInfoTab() throws IOException {
		Tab newTab = new Tab("Teacher Info");
		FXMLLoader loader = new FXMLLoader();
		SecretaryTabs.put("teacherInfo", newTab);
		newTab.setOnClosed(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				SecretaryTabs.remove("teacherInfo");
			}
		});

		Parent newPane = loader
				.load(Main.class.getResource("users/secretary/infoAbout/InfoAboutTeacherGUI.fxml").openStream());
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
	}

	/**
	 * build student request review tab
	 * 
	 * @throws IOException
	 */
	private void buildStRequest() throws IOException {
		Tab newTab = new Tab("Student Request");
		FXMLLoader loader = new FXMLLoader();
		SecretaryTabs.put("sRequest", newTab);
		newTab.setOnClosed(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				SecretaryTabs.remove("sRequest");
			}
		});

		Parent newPane = loader
				.load(Main.class.getResource("users/secretary/requests/StudentReviewRequestsGUI.fxml").openStream());
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
		mainTabPane.getSelectionModel().select(newTab);
	}

	/**
	 * build teacher request review tab
	 * @throws IOException
	 */
	private void buildTcRequest() throws IOException {
		Tab newTab = new Tab("Teacher Request");
		FXMLLoader loader = new FXMLLoader();
		SecretaryTabs.put("tRequest", newTab);
		newTab.setOnClosed(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				SecretaryTabs.remove("tRequest");
			}
		});

		Parent newPane = loader
				.load(Main.class.getResource("users/secretary/requests/TeacherReviewRequestsGUI.fxml").openStream());
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
		mainTabPane.getSelectionModel().select(newTab);
	}

	/**
	 * Builds the GUI to presents all the previous semester that exists.
	 * 
	 * @throws IOException
	 */
	public void buildPrevSemesterTab() throws IOException {
		ArrayList<Object[][]> ResultList = Main.getDbController().getResultsList();
		int maxSemester = ResultList.get(0).length;
		Semester prevSemester;
		for (int i = 0; i < maxSemester; i++) {
			prevSemester = new Semester();
			prevSemester.setSemesterID(ResultList.get(0)[i][0].toString());
			prevSemester.setSemesterType(ResultList.get(0)[i][1].toString());
			prevSemester.setStartDate(ResultList.get(0)[i][2].toString());
			prevSemester.setEndDate(ResultList.get(0)[i][3].toString());
			prevSemester.setYear(ResultList.get(0)[i][4].toString());
			SemestersList.add(prevSemester);
		}

		Tab newTab = new Tab("Previous Semester");
		FXMLLoader loader = new FXMLLoader();
		SecretaryTabs.put("prevSemester", newTab);
		newTab.setOnClosed(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				SecretaryTabs.remove("prevSemester");
			}
		});

		Parent newPane = loader
				.load(Main.class.getResource("users/secretary/semesters/SecretarySemestersGUI.fxml").openStream());
		SecretarySemestersGUIcontroller secretarySemesterGUIcontroller = (SecretarySemestersGUIcontroller) loader
				.getController();
		secretarySemesterGUIcontroller.setSemesterList(SemestersList);
		secretarySemesterGUIcontroller.setMainTabPane(mainTabPane);
		secretarySemesterGUIcontroller.setPagination(null);
		secretarySemesterGUIcontroller.buildAllSemestersForSemester();
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
		mainTabPane.getSelectionModel().select(SecretaryTabs.get("prevSemester"));
	}

	/**
	 * Builds the GUI with all the information regards to open a new semester.
	 * 
	 * @throws IOException
	 */
	private void buildNewSemesterTab() throws IOException {
		Tab newTab = new Tab("New Semester");
		FXMLLoader loader = new FXMLLoader();
		SecretaryTabs.put("newSemester", newTab);
		newTab.setOnClosed(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				SecretaryTabs.remove("newSemester");
			}
		});

		Parent newPane = loader.load(
				Main.class.getResource("users/secretary/semesters/semester/open/OpenNewSemesterGUI.fxml").openStream());
		OpenNewSemesterGUIcontroller openNewSemesterGUIcontroller = (OpenNewSemesterGUIcontroller) loader
				.getController();
		openNewSemesterGUIcontroller.getData();
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
	}

	/**
	 * Builds the GUI with all the information regards to current semester.
	 * 
	 * @throws IOException
	 */
	private void buildCurrentSemesterTab() throws IOException {
		Tab newTab = new Tab("Current Semester");
		FXMLLoader loader = new FXMLLoader();
		SecretaryTabs.put("currentSemester", newTab);
		newTab.setOnClosed(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				SecretaryTabs.remove("currentSemester");
			}
		});

		Parent newPane = loader.load(Main.class
				.getResource("users/secretary/semesters/semester/current/CurrentSemesterGUI.fxml").openStream());
		CurrentSemesterGUIcontroller currentSemesterGUIcontroller = (CurrentSemesterGUIcontroller) loader
				.getController();
		currentSemesterGUIcontroller.getData();
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
	}

	// *********PUBLIC CLASS METHODS***********

	public void setPaneTab(TabPane mainTabPane) {
		this.mainTabPane = mainTabPane;
		this.SecretaryTabs.put("main", mainTabPane.getTabs().get(0));
	}
}
