package app.users.secretary.infoAbout;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.Main;
import app.common.Class;
import app.common.Course;
import app.common.Teacher;
import app.users.secretary.requests.NewTeacherRequestGUIcontroller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class InfoAboutTeacherGUIcontroller implements Initializable {

	private Teacher SelectedTeacher;
	private ArrayList<Course> AllCourses = new ArrayList<Course>();
	private ObservableList<Course> TeacherCourses_OL;
	private ObservableList<Class> TeacherClasses_OL;
	private ObservableList<Teacher> Teachers_OL;
	private ArrayList<Course> AvailableCouses;
	private NewTeacherRequestGUIcontroller newTeacherRequestGUIcontroller;
	
	// *********FXML

	@FXML
	private TableColumn<Teacher, String> tcName;
	@FXML
	private Text txtID;
	@FXML
	private TextField idSearchTxt;
	@FXML
	private Button btnNewRequest;
	@FXML
	private Text txtFname;
	@FXML
	private TableView<Teacher> tvTeachers;
	@FXML
	private Button searchBtn;
	@FXML
	private TableColumn<Teacher, String> tcID;
	@FXML
	private ComboBox<Course> cbCoursesList;
	@FXML
	private ComboBox<Class> cbClassList;
	@FXML
	private Text txtLname;

	// *************PRIVATE CALSS METHODS************

	// ***********INITIALIZE*************

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		tcID.setCellValueFactory(cellData -> cellData.getValue().getIdProperty());
		tcName.setCellValueFactory(cellData -> cellData.getValue().getFullNameProperty());
		btnNewRequest.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					SelectedTeacher=tvTeachers.getSelectionModel().getSelectedItem();
					if (SelectedTeacher != null) {
						buildAvailableCourses();
						buildNewRequestForm();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		});

		tvTeachers.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (tvTeachers.getSelectionModel().getSelectedItem() != null) {
					SelectedTeacher = tvTeachers.getSelectionModel().getSelectedItem();
					txtID.setText(SelectedTeacher.getID());
					txtFname.setText(SelectedTeacher.getFirstName());
					txtLname.setText(SelectedTeacher.getLastName());
					TeacherClasses_OL = FXCollections.observableArrayList(SelectedTeacher.getTeacheringClasses());
					cbClassList.getItems().clear();
					cbClassList.setItems(TeacherClasses_OL);
					showTeacherDetails();
				}
			}
		});

		tvTeachers.setOnKeyReleased(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				if ((event.getCode() == KeyCode.DOWN) || (event.getCode() == KeyCode.UP)) {
					if (tvTeachers.getSelectionModel().getSelectedItem() != null) {
						SelectedTeacher = tvTeachers.getSelectionModel().getSelectedItem();
						txtID.setText(SelectedTeacher.getID());
						txtFname.setText(SelectedTeacher.getFirstName());
						txtLname.setText(SelectedTeacher.getLastName());
						TeacherCourses_OL = FXCollections.observableArrayList(SelectedTeacher.getTeacheringCourses());
						cbCoursesList.getItems().clear();
						cbCoursesList.setItems(TeacherCourses_OL);
						showTeacherDetails();
					}
				}
			}
		});

		cbClassList.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				Class SelectedClass = cbClassList.getSelectionModel().getSelectedItem();
				if(SelectedClass!=null){
					TeacherCourses_OL = FXCollections.observableArrayList(SelectedClass.getCoursetList());
					cbCoursesList.getItems().clear();
					cbCoursesList.setItems(TeacherCourses_OL);
				}
			}
		});
	}

	private void buildNewRequestForm() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		Parent tRequestItem = loader
				.load(Main.class.getResource("users/secretary/requests/NewTeacherRequestGUI.fxml").openStream());
		newTeacherRequestGUIcontroller = (NewTeacherRequestGUIcontroller) loader
				.getController();
		showTeacherDetails();
		newTeacherRequestGUIcontroller.setCourseList(AvailableCouses);
		Scene scene = new Scene(tRequestItem);
		Stage stage = new Stage();
		newTeacherRequestGUIcontroller.setStage(stage);
		stage.setScene(scene);
		stage.show();
	}

	@FXML
	private void searchPersons() {
		if (idSearchTxt.getText().isEmpty())
			Main.getDbController().getAllTeachersDetails(this);
		else
			Main.getDbController().getTeacherDetails(this, idSearchTxt.getText());
	}

	private void buildAvailableCourses() {
		AvailableCouses = new ArrayList<Course>();
		for (Course course : AllCourses) {
			if (!SelectedTeacher.getTeacheringCourses().contains(course))
				AvailableCouses.add(course);
		}
	}

	public void buildTeachersDetails() {
		/*
		 * 0 - Gets teachers details.
		 * 1 - Gets teachers's courses in the current
		 * semester.
		 */
		ArrayList<Object[][]> ResultsList = Main.getDbController().getResultsList();
		Teachers_OL = FXCollections.observableArrayList();

		Teacher teacher;
		Course course;
		Class teacherClass;
		int maxRows, index;

		if (ResultsList != null) {

			// Builds all courses.
			if (ResultsList.get(1)[0][0] != null) {
				maxRows = ResultsList.get(1).length;
				for (index = 0; index < maxRows; index++) {
					course = new Course(ResultsList.get(1)[index][0].toString());
					course.setCourseName(ResultsList.get(1)[index][2].toString());
					AllCourses.add(course);
				}
			}

			// Builds teachers.
			if (ResultsList.get(0)[0][0] != null) {
				maxRows = ResultsList.get(0).length;
				for (index = 0; index < maxRows; index++) {
					teacher = SearchTeacher(ResultsList.get(0)[index][0].toString());
					if (teacher == null) {
						teacher = new Teacher(ResultsList.get(0)[index][0].toString());
						teacher.setFirstName(ResultsList.get(0)[index][1].toString());
						teacher.setLastName(ResultsList.get(0)[index][2].toString());
					}
					course = SearchCourse(ResultsList.get(0)[index][3].toString());
					teacherClass = new Class(ResultsList.get(0)[index][4].toString(),
							ResultsList.get(0)[index][5].toString());
					teacherClass.addCourse(course);
					teacher.addCourse(course);
					teacher.getTeacheringClasses().add(teacherClass);
					if(!Teachers_OL.contains(teacher))
						Teachers_OL.add(teacher);
				}

			}
		}

		// presents the details in the table view.
		tvTeachers.setItems(Teachers_OL);
	}

	private Teacher SearchTeacher(String teacherID) {
		for (Teacher teacher : Teachers_OL) {
			if (teacher.getID().compareTo(teacherID) == 0)
				return teacher;
		}
		return null;
	}

	private Course SearchCourse(String courseID) {
		for (Course course : AllCourses) {
			if (course.getCourseID().compareTo(courseID) == 0)
				return course;
		}
		return null;
	}
	
	private void showTeacherDetails(){
		if(newTeacherRequestGUIcontroller!=null){
			newTeacherRequestGUIcontroller.setTeacherDetails(tvTeachers.getSelectionModel().getSelectedItem());
		}

	}

}
