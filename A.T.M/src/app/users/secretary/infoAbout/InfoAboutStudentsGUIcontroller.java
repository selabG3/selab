package app.users.secretary.infoAbout;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.Main;
import app.common.Class;
import app.common.Course;
import app.common.Student;
import app.users.secretary.requests.NewStudentRequestGUIcontroller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
public class InfoAboutStudentsGUIcontroller implements Initializable {

	
	// **********
	private Student SelectedStudent;
	private ArrayList<Course>AllCourses = new ArrayList<Course>();
	private ArrayList<Student>Students = new ArrayList<Student>();
	private ObservableList<Course> StudentCourses_OL;
	private ObservableList<Student> Studnts_OL;
	private ArrayList<Course> AvailableCouses;
	private NewStudentRequestGUIcontroller newStudentRequestGUIcontroller;
	// *********FXML

	
	@FXML
	private Text txtClass;
	@FXML
	private Text txtID;
	@FXML
	private TextField idSearchTxt;
	@FXML
	private Text txtFname;
	@FXML
	private Button searchBtn;
	@FXML
	private Button btnNewRequest;
	@FXML
	private ComboBox<Course> cbCourseList;
	@FXML
	private Text txtLname;
    @FXML
    private TableView<Student> tvStudents;
    @FXML
    private TableColumn<Student, String> tcID;
    @FXML
    private TableColumn<Student, String> tcName;

    
	// *************PRIVATE CALSS METHODS************

	
	@FXML
	private void searchPersons() {
		if(idSearchTxt.getText().isEmpty())
			Main.getDbController().getAllStudentDetails(this);
		else
			Main.getDbController().getStudentDetails(this,idSearchTxt.getText());
	}

	private void buildNewRequestForm() throws IOException{
		FXMLLoader loader = new FXMLLoader();
		Parent sRequestItem = loader.load(Main.class.getResource("users/secretary/requests/NewStudentRequestGUI.fxml").openStream());
		newStudentRequestGUIcontroller = (NewStudentRequestGUIcontroller) loader.getController();
		newStudentRequestGUIcontroller.showStudentDetails(SelectedStudent);
		newStudentRequestGUIcontroller.setCourseList(AvailableCouses);
		Scene scene = new Scene(sRequestItem);
		Stage stage = new Stage();
		newStudentRequestGUIcontroller.setStage(stage);
		stage.setScene(scene);
		stage.show();
	}
	

	/**
	 * Search for a student if exists.
	 * @param studentID
	 * @return Student index in Students List.
	 */
	private int SearchStudentIndex(String studentID){
		for(Student student : Students){
			if(student.getID().compareTo(studentID)==0)
				return Students.indexOf(student);
		}
		return -1;
	}
	
	private void buildAvailableCourses(){
		AvailableCouses = new ArrayList<Course>();
		for(Course course : AllCourses){
			if(!SelectedStudent.getCourses().contains(course))
				AvailableCouses.add(course);
		}
	}
	// ***********INITIALIZE*************

	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		btnNewRequest.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
					try {
						if(SelectedStudent!=null){
							buildAvailableCourses();
							buildNewRequestForm();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				
			}
		});
		
		tvStudents.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(tvStudents.getSelectionModel().getSelectedItem()!=null){
					SelectedStudent = tvStudents.getSelectionModel().getSelectedItem();
					txtID.setText(SelectedStudent.getID());
					txtFname.setText(SelectedStudent.getFirstName());
					txtLname.setText(SelectedStudent.getLastName());
					txtClass.setText(SelectedStudent.getStudentClasses().get(0).getFullClassName());
					StudentCourses_OL = FXCollections.observableArrayList(SelectedStudent.getCourses());
					cbCourseList.getItems().clear();
					cbCourseList.setItems(StudentCourses_OL);
					sendStudentDetails();
				}
			}
		});
		
		tvStudents.setOnKeyReleased(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				if((event.getCode()==KeyCode.DOWN)||(event.getCode()==KeyCode.UP)){
					if(tvStudents.getSelectionModel().getSelectedItem()!=null){
						SelectedStudent = tvStudents.getSelectionModel().getSelectedItem();
						txtID.setText(SelectedStudent.getID());
						txtFname.setText(SelectedStudent.getFirstName());
						txtLname.setText(SelectedStudent.getLastName());
						txtClass.setText(SelectedStudent.getStudentClasses().get(0).getFullClassName());
						StudentCourses_OL = FXCollections.observableArrayList(SelectedStudent.getCourses());
						cbCourseList.getItems().clear();
						cbCourseList.setItems(StudentCourses_OL);
						sendStudentDetails();
					}
				}
			}
		});
		
		tcID.setCellValueFactory(cellData -> cellData.getValue().getIdProperty());
		tcName.setCellValueFactory(cellData -> cellData.getValue().getFullNameProperty());
	}
	
	// *************PUBLIC CALSS METHODS************
	
	public void buildStudentsDetails(){
		/*
		 * 0 - Gets students details.
		 * 1 - Gets student's courses in the current semester.
		 */
		ArrayList<Object[][]>ResultsList = Main.getDbController().getResultsList();
		Student student;
		Course course;
		Class studentClass;
		
		int maxRows,index;
		if(ResultsList!=null){
			//Builds students.
			if(ResultsList.get(0)[0][0]!=null){
				maxRows = ResultsList.get(0).length;
				for(index = 0 ; index<maxRows ; index++){
					student = new Student(ResultsList.get(0)[index][0].toString());
					student.setFirstName(ResultsList.get(0)[index][1].toString());
					student.setLastName(ResultsList.get(0)[index][2].toString());
					studentClass = new Class(ResultsList.get(0)[index][3].toString());
					studentClass.setClassName(ResultsList.get(0)[index][4].toString());
					student.addClass(studentClass);
					Students.add(student);
				}
			}
			
			//Builds student's courses in the current semester.
			if(ResultsList.get(1)[0][0]!=null){
			maxRows = ResultsList.get(1).length;
			for (index = 0 ; index<maxRows ; index++){
				student = Students.get(SearchStudentIndex(ResultsList.get(1)[index][0].toString()));
				course = new Course(ResultsList.get(1)[index][1].toString());
				course.setCourseName(ResultsList.get(1)[index][2].toString());
				student.addCourse(course);
			}
			}
		}
		
		//Builds all courses.
		if(ResultsList.get(2)[0][0]!=null){
			maxRows = ResultsList.get(2).length;
			for(index = 0 ; index<maxRows ; index++){
				course = new Course(ResultsList.get(2)[index][0].toString());
				course.setCourseName(ResultsList.get(2)[index][1].toString());
				AllCourses.add(course);
			}
		}
		
		//presents the details in the table view.
		
		Studnts_OL = FXCollections.observableArrayList(Students);
		tvStudents.getItems().clear();
		tvStudents.setItems(Studnts_OL);
	}
	
	protected void sendStudentDetails(){
		if(newStudentRequestGUIcontroller!=null){
			newStudentRequestGUIcontroller.showStudentDetails(tvStudents.getSelectionModel().getSelectedItem());
		}
	}
	
}
