package app.users.teacher.semesters.semester;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import app.Main;
import app.common.Assignment;
import app.common.Course;
import app.common.gui.AbstractCourseSemesterGUI;
import app.users.teacher.semesters.semester.course.CourseAssignmentGUIcontroller;
import common.AssignmentFile;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class TeacherCourseGUIcontroller extends AbstractCourseSemesterGUI implements Initializable {

    @FXML
    private Button btnNewAss;
    @FXML
    private Button btnChangeAss;
    @FXML
    private TableView<Assignment> tvAss;
    @FXML
    private TableColumn<Assignment, String> tcDueDate;
    @FXML
    private BorderPane assPane;
    @FXML
    private Text txtCourseName;
    @FXML
    private TableColumn<Assignment, String> tcAssName;
    @FXML
    private TableColumn<Assignment, String> tcUpDate;

	private ObservableList<Assignment>CourseAss_OL = FXCollections.observableArrayList();
	private Course Course;
	private TabPane mainTabPane;
	private Stage assStage;
	private CourseAssignmentGUIcontroller courseAssignmentGUIcontroller;
	
	@Override
	public void setMainTabPane(TabPane mainTabPane) {
		this.mainTabPane = mainTabPane;
	}

	@Override
	public void setCourse(Course Course) {
		this.Course = Course;
		txtCourseName.setText(Course.getCourseName());
		CourseAss_OL.addAll(Course.getAssignments());
		tvAss.setItems(CourseAss_OL);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		txtCourseName.setText("");
		
		tcAssName.setCellValueFactory(cellData->cellData.getValue().getAssNameProperty());
		tcDueDate.setCellValueFactory(cellData->cellData.getValue().getDueDateProperty());
		tcUpDate.setCellValueFactory(cellData->cellData.getValue().getUploadDateProperty());
	}

	
	@FXML
	private void newAssForm(){
		if(assStage==null){
		try {
			FXMLLoader loader = new FXMLLoader();
			Parent newAss = loader.load(Main.class.getResource("users/teacher/semesters/semester/course/CourseAssignmentGUI.fxml").openStream());
			courseAssignmentGUIcontroller = (CourseAssignmentGUIcontroller)loader.getController();
			courseAssignmentGUIcontroller.setCourseDetails(Course);
			courseAssignmentGUIcontroller.setCourseAss(CourseAss_OL);
			Scene scene = new Scene(newAss);
			assStage = new Stage();
			assStage.setScene(scene);
			assStage.show();
			courseAssignmentGUIcontroller.setStage(assStage);
 		} catch (IOException e) {
			e.printStackTrace();
		}
		}
		else assStage.show();
	}
	
	@FXML
	private void changeAss(){
		Assignment SelectedAss = tvAss.getSelectionModel().getSelectedItem();
		if(SelectedAss!=null){
			if(assStage!=null){
				courseAssignmentGUIcontroller.setCourseDetails(Course);
				courseAssignmentGUIcontroller.setAssDetails(SelectedAss);
				assStage.show();
			}else{
				newAssForm();
				courseAssignmentGUIcontroller.setAssDetails(SelectedAss);
			}
		}
	}
}
