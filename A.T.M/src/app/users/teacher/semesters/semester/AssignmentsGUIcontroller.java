package app.users.teacher.semesters.semester;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import app.common.Assignment;
import app.common.Course;
import common.AssignmentFile;

public class AssignmentsGUIcontroller {

	   @FXML
	   private Text assFileLbl;

	   @FXML
	   private TextField dueDateText;
	   
	   @FXML
	   private Hyperlink assFileLink;

	   @FXML
	   private Button uploadAFileBtn;

	   @FXML
	   private Text dueDateLbl;

	   @FXML
	   private TextField assNameText;

	   @FXML
	   private Text courseNameLbl;

	   @FXML
	   private TextField cNameText;

	   @FXML
	   private Text assNameLbl;

	   @FXML
	   private Button saveBtn;
	   
	   private Assignment ass;
	   private Course course;

	   
	   
	   @FXML
	   public void setCourseName(ActionEvent event)
	   {
		   cNameText.setText(course.getCourseName());
	   }
	   
	  //all the methods for getting the info by the teacher
	   @FXML
	   public void setAssName(ActionEvent event)
	   {
		   assNameText.setText(ass.getAssName());
	   }
	   
	   @FXML
	   public void setDueDate(ActionEvent event) 
	   {
		   DateFormat format=new SimpleDateFormat("dd/MM/yyyy");
		   String temp=format.format(ass.getDueDate());
		   dueDateText.setText(temp);
	   }
}
	   
	   /*//*
	   @FXML
	   void saveChanges(ActionEvent event) {

	   }
	   */
	   
	


