package app.users.teacher.semesters.semester.course;

import java.io.File;
import java.time.LocalDate;

import app.Main;
import app.common.Assignment;
import app.common.Course;
import common.AssignmentFile;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class CourseAssignmentGUIcontroller {

	@FXML
	private TextArea txtNote;

	@FXML
	private Button btnUploadFile;

	@FXML
	private TextField txtAssName;

	@FXML
	private Button btnNewAss;

	@FXML
	private DatePicker dpDueDate;

	@FXML
	private Text txtCourseName;

	@FXML
	private Button btnRemoveFile;

	private Course course;
	private Assignment SelectedAss;
	private File myFile;
	private LocalDate Date = LocalDate.now();

	private ObservableList<Assignment> courseAss_OL;

	private Stage assStage;
	
	public void setCourseDetails(Course course) {
		this.course = course;
		txtCourseName.setText(course.getCourseName());
	}

	public void setAssDetails(Assignment selectedAss) {
		this.SelectedAss = selectedAss;
		txtAssName.setText(selectedAss.getAssName());
		txtCourseName.setText(course.getCourseName());
	}

	@FXML
	private void newAssCreate() {
		if (myFile != null) {
			AssignmentFile assFile = new AssignmentFile(myFile.getName(), Main.getUserName(), course.getSemesterID(), course.getCourseID(), Date.toString(),
					dpDueDate.getValue().toString(), txtAssName.getText(), 3);
			Main.getDbController().sendFile(assFile, myFile);
			Assignment newAss = new Assignment();
			newAss.setAssName(txtAssName.getText());
			newAss.setUpDate(Date.toString());
			newAss.setDueDate(dpDueDate.getValue().toString());
			courseAss_OL.add(newAss);
			assStage.hide();
		}
	}

	@FXML
	private void uploadFile() {
		// Creates a file chooser.
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Open File");
		myFile = chooser.showOpenDialog(new Stage());
	}

	@FXML
	private void removeFile() {
		myFile = null;
	}

	public void setCourseAss(ObservableList<Assignment> courseAss_OL) {
		this.courseAss_OL = courseAss_OL;
	}

	public void setStage(Stage assStage) {
		this.assStage = assStage;
	}

}