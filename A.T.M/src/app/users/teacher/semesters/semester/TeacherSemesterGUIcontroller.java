package app.users.teacher.semesters.semester;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.Main;
import app.common.Course;
import app.common.gui.SemesterGUI;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Pagination;
import javafx.scene.control.TabPane;

public class TeacherSemesterGUIcontroller extends SemesterGUI{

	@FXML
	private Pagination pagination;
	private TabPane mainTabPane;

	public void setPagination(Pagination pagination){
		super.setPagination(this.pagination);
	}

	public void buildAllCoursesForSemester(){
		super.buildAllCoursesForSemester();
	}
	
	public void setCourseList(ArrayList<Course> CourseList){
		super.setCourseList(CourseList);
	}
	

	public void setMainTabPane(TabPane mainTabPane) {
		super.setMainTabPane(mainTabPane);
		this.mainTabPane = mainTabPane;
	}
}
