package app.users.teacher;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

import app.Main;
import app.common.Assignment;
import app.common.Class;
import app.common.Course;
import app.common.Semester;
import app.users.secretary.semesters.SecretarySemestersGUIcontroller;
import app.users.teacher.semesters.semester.TeacherSemesterGUIcontroller;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

public class TeacherGUImenuController implements Initializable {

	private TabPane mainTabPane;
	private ArrayList<Object[][]> ResultsList;
	private ArrayList<Course> CourseList = new ArrayList<Course>();
	private HashMap<String, Tab> TeacherTabs = new HashMap<String, Tab>();
	private ArrayList<Semester> PrevSemesters = new ArrayList<Semester>();

	@FXML
	private Button mainTab;
	@FXML
	private Button currentSemesterTab;
	@FXML
	private Button checkAssTab;
	@FXML
	private Button prevSemesterTab;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	@FXML
	private void currentSemester() {
		Main.getDbController().getCurrentSemesterForTeacher(this);
	}

	@FXML
	private void prevSemester() {
		Main.getDbController().getCurrentSemesterForTeacher(this);
	}

	public void setPaneTab(TabPane mainTabPane) {
		this.mainTabPane = mainTabPane;
	}

	public void buildData() {
		if (!TeacherTabs.containsValue("currentSemester")) {
			try {
				ResultsList = Main.getDbController().getResultsList();
				Course course;
				Assignment newAss;
				int maxCourses = ResultsList.get(0).length;
				int index;
				if (ResultsList.get(0)[0][0] != null)
					if (ResultsList.size() != 0) {
						for (index = 0; index < maxCourses; index++) {
							course = new Course(ResultsList.get(0)[index][1].toString());
							course.setSemesterID(ResultsList.get(0)[index][2].toString());
							course.setCourseName(ResultsList.get(0)[index][5].toString());
							CourseList.add(course);

						}
					}
				if (ResultsList.get(1) != null) {
					if (ResultsList.get(1)[0][0] != null) {
						int maxRows = ResultsList.get(1).length;
						for (index = 0; index < maxRows; index++) {
							newAss = new Assignment();
							newAss.setAssNum(ResultsList.get(1)[index][0].toString());
							newAss.setAssName(ResultsList.get(1)[index][1].toString());
							newAss.setSemesterID(ResultsList.get(1)[index][2].toString());
							newAss.setCourseID(ResultsList.get(1)[index][3].toString());
							newAss.setUpDate(ResultsList.get(1)[index][4].toString());
							newAss.setDueDate(ResultsList.get(1)[index][5].toString());
							course = SearchCoure(ResultsList.get(1)[index][3].toString());
							if (course != null)
								course.getAssignments().add(newAss);
						}
					}
				}

				Tab newTab = new Tab("Current Semester");
				FXMLLoader loader = new FXMLLoader();
				Parent newPane;
				newPane = loader.load(Main.class.getResource("users/teacher/semesters/semester/TeacherSemesterGUI.fxml")
						.openStream());
				TeacherSemesterGUIcontroller teacherSemesterGUIcontroller = (TeacherSemesterGUIcontroller) loader
						.getController();
				teacherSemesterGUIcontroller.setMainTabPane(mainTabPane);
				teacherSemesterGUIcontroller.setPagination(null);
				teacherSemesterGUIcontroller.setCourseList(CourseList);
				teacherSemesterGUIcontroller.buildAllCoursesForSemester();
				newTab.setContent(newPane);
				mainTabPane.getTabs().add(newTab);
				mainTabPane.getSelectionModel().select(newTab);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else
			mainTabPane.getSelectionModel().select(TeacherTabs.get("currentSemester"));

	}

	private Course SearchCoure(String CourseID) {
		for (Course course : CourseList)
			if (course.getCourseID().compareTo(CourseID) == 0)
				return course;
		return null;
	}

	public void buildPrevSemesters() {
		if (!TeacherTabs.containsValue("prevSemester")) {
			try {
				ResultsList = Main.getDbController().getResultsList();
				Course course;
				Assignment newAss;
				Semester prevSemester = null;

				if (ResultsList != null) {
					int index;
					int maxRows;
					if (ResultsList.get(0) != null) {
						maxRows = ResultsList.get(0).length;
						if (ResultsList.get(0)[0][0] != null) {
							for(index =0 ; index<maxRows ; index++)
							prevSemester = SearchSemester(ResultsList.get(0)[0][0].toString());
							if (prevSemester == null) {
								prevSemester = new Semester();
								prevSemester.setSemesterID(ResultsList.get(0)[0][0].toString());
							}
						}
					}
				}

				int maxCourses = ResultsList.get(0).length;
				int index;
				if (ResultsList.get(0)[0][0] != null)
					if (ResultsList.size() != 0) {
						for (index = 0; index < maxCourses; index++) {
							course = new Course(ResultsList.get(0)[index][1].toString());
							course.setSemesterID(ResultsList.get(0)[index][2].toString());
							course.setCourseName(ResultsList.get(0)[index][5].toString());
							CourseList.add(course);

						}
					}
				if (ResultsList.get(1) != null) {
					if (ResultsList.get(1)[0][0] != null) {
						int maxRows = ResultsList.get(1).length;
						for (index = 0; index < maxRows; index++) {
							newAss = new Assignment();
							newAss.setAssNum(ResultsList.get(1)[index][0].toString());
							newAss.setAssName(ResultsList.get(1)[index][1].toString());
							newAss.setSemesterID(ResultsList.get(1)[index][2].toString());
							newAss.setCourseID(ResultsList.get(1)[index][3].toString());
							newAss.setUpDate(ResultsList.get(1)[index][4].toString());
							newAss.setDueDate(ResultsList.get(1)[index][5].toString());
							course = SearchCoure(ResultsList.get(1)[index][3].toString());
							if (course != null)
								course.getAssignments().add(newAss);
						}
					}
				}

				Tab newTab = new Tab("Previous Semesters");
				FXMLLoader loader = new FXMLLoader();
				Parent newPane;
				newPane = loader.load(
						Main.class.getResource("users/teacher/semesters/TeacherSemestersGUI.fxml.fxml").openStream());
				TeacherSemesterGUIcontroller teacherSemesterGUIcontroller = (TeacherSemesterGUIcontroller) loader
						.getController();
				teacherSemesterGUIcontroller.setMainTabPane(mainTabPane);
				teacherSemesterGUIcontroller.setPagination(null);
				teacherSemesterGUIcontroller.setCourseList(CourseList);
				teacherSemesterGUIcontroller.buildAllCoursesForSemester();
				newTab.setContent(newPane);
				mainTabPane.getTabs().add(newTab);
				mainTabPane.getSelectionModel().select(newTab);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else
			mainTabPane.getSelectionModel().select(TeacherTabs.get("prevSemester"));

	}

	private Semester SearchSemester(String semesterID) {
		for (Semester sem : PrevSemesters)
			if (sem.getSemesterID().compareTo(semesterID) == 0)
				return sem;
		return null;
	}

}
