package app.users.view;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.Main;
import app.users.parent.ParentGUImenuController;
import app.users.principal.PrincipalGUImenuController;
import app.users.secretary.SecretaryGUImenuController;
import app.users.student.StudentGUImenuController;
import app.users.teacher.TeacherGUImenuController;
import common.AssignmentFile;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class UsersViewGUIcontroller implements Initializable{

    private File file;
    
    @FXML
    private ImageView imgV;
    @FXML
    private Tab mainTab;
    @FXML
    private TabPane mainTabPane;
    @FXML
    private BorderPane PaneView;
    @FXML
    private Text txtTitle;
    @FXML
    private Text txtText;

    
	public void setUserType(int UserType) throws IOException {
		switch (UserType) {
		case 1:
			viewParentMenu();
			break;
		case 2:
			viewStudentMenu();
			break;
		case 3:
			viewTeacherMenu();
			break;
		case 4:
			viewSecretarytMenu();
			break;
		case 5:
			viewPrincipalMenu();
			break;
		}
	}
	
	
	
	private void viewParentMenu() {
		try {
			FXMLLoader loader = new FXMLLoader();
			Parent mainItems;
			mainItems = loader.load(Main.class.getResource("users/parent/ParentGUImenu.fxml").openStream());
			ParentGUImenuController parentGUImenuController = (ParentGUImenuController) loader.getController();
			parentGUImenuController.setPaneTab(this.mainTabPane);
			PaneView.setLeft(mainItems);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void viewStudentMenu() {
		try {
			FXMLLoader loader = new FXMLLoader();
			Parent mainItems;
			mainItems = loader.load(Main.class.getResource("users/student/StudentGUImenu.fxml").openStream());
			StudentGUImenuController studentGUImenuController = (StudentGUImenuController) loader.getController();
			studentGUImenuController.setPaneTab(this.mainTabPane);
			PaneView.setLeft(mainItems);
		} catch (IOException e) {}

	}
	
	private void viewTeacherMenu() {
		try {
			FXMLLoader loader = new FXMLLoader();
			Parent mainItems;
			mainItems = loader.load(Main.class.getResource("users/teacher/TeacherGUImenu.fxml").openStream());
			TeacherGUImenuController teacherGUImenuController = (TeacherGUImenuController) loader.getController();
			teacherGUImenuController.setPaneTab(this.mainTabPane);
			PaneView.setLeft(mainItems);
		} catch (IOException e) {e.printStackTrace();}

	}
	
	private void viewSecretarytMenu() {
		try {
			FXMLLoader loader = new FXMLLoader();
			Parent mainItems;
			mainItems = loader.load(Main.class.getResource("users/secretary/SecretaryGUImenu.fxml").openStream());
			SecretaryGUImenuController secretaryGUImenuController = (SecretaryGUImenuController) loader.getController();
			secretaryGUImenuController.setPaneTab(mainTabPane);
			PaneView.setLeft(mainItems);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	private void viewPrincipalMenu() {
		try {
			FXMLLoader loader = new FXMLLoader();
			Parent mainItems;
			mainItems = loader.load(Main.class.getResource("users/principal/PrincipalGUImenu.fxml").openStream());
			PrincipalGUImenuController principalGUImenuController = (PrincipalGUImenuController) loader.getController();
			principalGUImenuController.setPaneTab(this.mainTabPane);
			PaneView.setLeft(mainItems);
		} catch (IOException e) {}

	}
	
	
	
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mainTab.setId("MainTab");
	}
	
	
	
	
	
	
	
	
	
	
	
	

	/*
	 * THIS IS A TEST
	 
	private void doStuff(){

		try{
			locateFile();
			AssignmentFile assignmentFile = new AssignmentFile(file.getName());
			assignmentFile.setDescription(file.getPath());	
			byte [] arrayOfBytes  = new byte [(int)file.length()];
			assignmentFile.initArray(arrayOfBytes.length);
			assignmentFile.setSize(arrayOfBytes.length);
			assignmentFile.setArrayOfBytes(arrayOfBytes);
			ArrayList<Object[]> test = new ArrayList<Object[]>();
			Object[] o = new Object[3];
			o[0] = new String("FILE");
			o[1] = new String("UPDATE sw_lab.temp1 SET sw_lab.temp1.temp1col = ? WHERE sw_lab.temp1.idtemp1="+Integer.parseInt(Main.getUserName()));
			o[2] = assignmentFile;
			test.add(o);
		
			
		}catch(Exception e)
		{
			e.printStackTrace();
		};
	}
	

	private void doStuff2(){

		try{
			locateFile();
			AssignmentFile assFile = new AssignmentFile(file.getName());
			assFile.setDescription(file.getPath());
			byte [] arrayOfBytes  = new byte [(int)file.length()];
			assFile.initArray(arrayOfBytes.length);
			assFile.setSize(arrayOfBytes.length);
			assFile.setArrayOfBytes(arrayOfBytes);
			
			ArrayList<Object[]> test = new ArrayList<Object[]>();
			Object[] o = new Object[3];
			o[0] = new String("FILE");
			o[1] = new String("INSERT INTO sw_lab.temp1 (sw_lab.temp1.temp1col,sw_lab.temp1.idtemp1) value (?,"+Integer.parseInt(Main.getUserName())+")");
			o[2] = assFile;
			test.add(o);
		
			
		}catch(Exception e)
		{
			e.printStackTrace();
		};
	}

	//Get a file from database and save it in a file.
	@FXML
	private void doStuff3(){
		ArrayList<Object[][]> test = new ArrayList<Object[][]>();
		//Main.getDbController().getFile(Main.getUserName());
		test = Main.getDbController().getResultsList();
		
		AssignmentFile readWriteFile;
		ByteArrayInputStream bis = new ByteArrayInputStream((byte[])test.get(0)[0][0]);
		ObjectInput in = null;
		try {
		  in = new ObjectInputStream(bis);
		  readWriteFile = (AssignmentFile) in.readObject(); 
		  System.out.print(readWriteFile.getFileName()+"\t"+readWriteFile.getDescription());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
		  try {
		    if (in != null) {
		      in.close();
		      saveFile();
		    }
		  } catch (IOException e) {
				e.printStackTrace();
		  }
		}
		
		
	}
	
	@FXML 
	private void locateFile() {
	    FileChooser chooser = new FileChooser();
	    chooser.setTitle("Open File");
	    file = chooser.showOpenDialog(new Stage());
	}
	
	@FXML
	private void saveFile(){
			try {
				FileChooser chooser = new FileChooser();
				chooser.setTitle("Save File");
				File dest  = chooser.showSaveDialog(new Stage());
				if (dest !=null)
				Files.copy(file.toPath(), dest.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	 * END OF TEST
	 */


}
