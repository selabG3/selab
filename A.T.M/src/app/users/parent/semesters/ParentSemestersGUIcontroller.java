package app.users.parent.semesters;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.Main;
import app.common.Assignment;
import app.common.Course;
import app.common.Semester;
import app.common.Student;
import app.common.gui.ISemesters;
import app.common.gui.SemestersGUI;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Pagination;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;

public class ParentSemestersGUIcontroller extends SemestersGUI implements ISemesters,Initializable{

    @FXML
    private Pagination pagination;
    @FXML
    private ComboBox<Student> cbChild;
    @FXML
    private BorderPane prevSemBorderPane;
    
	private ObservableList<Student> ParentChildrens_OL;
	private ArrayList<Object[][]> ResultsList;
	private Student SelectedChild;
	private ArrayList<Semester>SemesterList = new ArrayList<Semester>();
	
	@Override
	public void setMainTabPane(TabPane mainTabPane) {
		super.setMainTabPane(mainTabPane);
	}

	public void getData() {
		Main.getDbController().getParentChildrens(this);
	}

	public void buildChildren() {
		ResultsList = Main.getDbController().getResultsList();
		if (ResultsList != null)
			if (ResultsList.get(0)[0][0] != null) {
				// Build children details.
				int maxRows = ResultsList.get(0).length;
				int index;
				Student child;
				for (index = 0; index < maxRows; index++) {
					child = new Student(ResultsList.get(0)[index][3].toString());
					child.setFirstName(ResultsList.get(0)[index][4].toString());
					child.setLastName(ResultsList.get(0)[index][5].toString());
					ParentChildrens_OL.add(child);
				}
			}
		cbChild.setItems(ParentChildrens_OL);		
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		prevSemBorderPane.getChildren().remove(pagination);
		ParentChildrens_OL = FXCollections.observableArrayList();
	}
	
	@FXML
	private void buildPrevSemesterCourse() {
		SelectedChild = cbChild.getSelectionModel().getSelectedItem();
		if (SelectedChild != null)
			if (SelectedChild.getCourses().size() == 0)
				Main.getDbController().getAllSemesterCourseForChild(this, SelectedChild.getID());
			else
				showCourses();
	}


	/**
	 * 
	 */
	private void showCourses(){
		setPagination(pagination);
		setSemesterList(SemesterList);
		setStudent(SelectedChild);
		buildAllSemestersForSemester();
		prevSemBorderPane.setCenter(pagination);
	}

	public void buildChildrenSemesterCourse() {
		ArrayList<Object[][]> ResultsList = Main.getDbController().getResultsList();
		Course course;
		Assignment newAss;
		Semester newSemester;
		int maxRows, index;
		if (ResultsList != null) {
			// Builds for a child his courses in the current semester.
			if (ResultsList.get(0)[0][0] != null) {
				maxRows = ResultsList.get(0).length;
				for (index = 0; index < maxRows; index++) {
					newSemester = SearchSemester(ResultsList.get(0)[index][3].toString());
					if(newSemester==null){
						newSemester = new Semester();
						newSemester.setSemesterID(ResultsList.get(0)[index][3].toString());
						newSemester.setSemesterType(ResultsList.get(0)[index][10].toString());
						newSemester.setStartDate(ResultsList.get(0)[index][11].toString());
						newSemester.setEndDate(ResultsList.get(0)[index][12].toString());
						newSemester.setYear(ResultsList.get(0)[index][13].toString());
						SemesterList.add(newSemester);
					}
					course = new Course(ResultsList.get(0)[index][0].toString());
					course.setCourseName(ResultsList.get(0)[index][6].toString());
					newSemester.addCourses(course);
					SelectedChild.addCourse(course);
				}

				//Builds all child's courses.
				if(ResultsList.get(1)[0][0]!=null){
					maxRows = ResultsList.get(1).length;
					for(index = 0 ; index<maxRows ; index++){
						course = SearchCourse(ResultsList.get(1)[index][3].toString(),SearchChild(ResultsList.get(1)[index][7].toString()));
						if(course!=null){
							newAss = new Assignment();
							newAss.setAssNum(ResultsList.get(1)[index][0].toString());
							newAss.setAssName(ResultsList.get(1)[index][1].toString());
							newAss.setUpDate(ResultsList.get(1)[index][4].toString());
							newAss.setDueDate(ResultsList.get(1)[index][5].toString());
							newAss.setSemesterID(ResultsList.get(1)[index][2].toString());
							newAss.setCourseID(ResultsList.get(1)[index][3].toString());
							newAss.setStudentID(ResultsList.get(1)[index][7].toString());
							newAss.setGrade(ResultsList.get(1)[index][6].toString());
							course.getAssignments().add(newAss);
							if(!SelectedChild.getCourses().contains(course))
								SelectedChild.addCourse(course);
						}
					}
				}
				
				
			}
			if(SelectedChild.getCourses().size()>0)
				showCourses();
		}		
	}

	private Semester SearchSemester(String semesterID) {
		for(Semester sem : SemesterList)
			if(sem.getSemesterID().compareTo(semesterID)==0)
				return sem;
		return null;
	}

	/**
	 * 
	 * @param childID
	 * @return
	 */
	private Student SearchChild(String childID) {
		for(Student student: ParentChildrens_OL)
			if(student.getID().compareTo(childID)==0){
				SelectedChild = student;
				return student;
			}
		return null;
	}

	/**
	 * 
	 * @param CourseID
	 * @param Child
	 * @return
	 */
	private Course SearchCourse(String CourseID, Student Child) {
		for(Course course : Child.getCourses())
			if(course.getCourseID().compareTo(CourseID)==0)
				return course;
		return null;
	}

}
