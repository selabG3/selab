package app.users.parent.semesters.semester;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import app.Main;
import app.common.Assignment;
import app.common.Course;
import app.common.Student;
import app.common.gui.ISemester;
import app.common.gui.SemesterGUI;
import common.AssignmentFile;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Pagination;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;

public class ParentSemesterGUIcontroller extends SemesterGUI implements ISemester, Initializable {

	@FXML
	private Pagination pagination;
	@FXML
	private ComboBox<Student> cbChild;
    @FXML
    private BorderPane currSemBorderPane;
    
	private ObservableList<Student> ParentChildrens_OL;
	private ArrayList<Object[][]> ResultsList;
	private Student SelectedChild;
	private TabPane mainTabPane;
	
	// *********PRIVATE CLASS METHODS

	@FXML
	private void getSelectedChildDetails() {
		SelectedChild = cbChild.getSelectionModel().getSelectedItem();
		if (SelectedChild != null)
			if (SelectedChild.getCourses().size() == 0)
				Main.getDbController().getChildCourses(this, SelectedChild.getID());
			else
				showCourses();
	}
	
	public void setPagination(Pagination pagination){
		super.setPagination(this.pagination);
	}


	// *********PUBLIC CLASS METHODS

	/**
	 * Gets all parent children.
	 */
	public void getData() {
		Main.getDbController().getParentChildrens(this);
	}

	@Override
	public void setMainTabPane(TabPane mainTabPane) {
		super.setMainTabPane(mainTabPane);
		this.mainTabPane = mainTabPane;
	}

	/**
	 * Builds a list of the parent's children.
	 */
	public void buildChildren() {
		ResultsList = Main.getDbController().getResultsList();
		if (ResultsList != null)
			if (ResultsList.get(0)[0][0] != null) {
				// Build children details.
				int maxRows = ResultsList.get(0).length;
				int index;
				Student child;
				Course course;
				Assignment newAss;
				
				for (index = 0; index < maxRows; index++) {
					child = new Student(ResultsList.get(0)[index][3].toString());
					child.setFirstName(ResultsList.get(0)[index][4].toString());
					child.setLastName(ResultsList.get(0)[index][5].toString());
					ParentChildrens_OL.add(child);
				}

			}
		cbChild.setItems(ParentChildrens_OL);
	}

	
	/**
	 * 
	 * @param childID
	 * @return
	 */
	private Student SearchChild(String childID) {
		for(Student student: ParentChildrens_OL)
			if(student.getID().compareTo(childID)==0){
				SelectedChild = student;
				return student;
			}
		return null;
	}

	/**
	 * 
	 * @param CourseID
	 * @param Child
	 * @return
	 */
	private Course SearchCourse(String CourseID, Student Child) {
		for(Course course : Child.getCourses())
			if(course.getCourseID().compareTo(CourseID)==0)
				return course;
		return null;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ParentChildrens_OL = FXCollections.observableArrayList();
		currSemBorderPane.getChildren().remove(pagination);
	}

	/**
	 * Builds for a child his courses.
	 */
	public void buildChildrenCourses() {
		ArrayList<Object[][]> ResultsList = Main.getDbController().getResultsList();
		Course course;
		Assignment newAss;
		int maxRows, index;
		if (ResultsList != null) {
			// Builds for a child his courses in the current semester.
			if (ResultsList.get(0)[0][0] != null) {
				maxRows = ResultsList.get(0).length;
				for (index = 0; index < maxRows; index++) {
					course = new Course(ResultsList.get(0)[index][1].toString());
					course.setCourseName(ResultsList.get(0)[index][2].toString());
					SelectedChild.addCourse(course);
				}
				System.out.print(false);
				//Builds all child's courses.
				if(ResultsList.get(1)[0][0]!=null){
					maxRows = ResultsList.get(1).length;
					for(index = 0 ; index<maxRows ; index++){
						course = SearchCourse(ResultsList.get(1)[index][5].toString(),SearchChild(ResultsList.get(1)[index][6].toString()));
						if(course!=null){
							newAss = new Assignment();
							newAss.setAssNum(ResultsList.get(1)[index][0].toString());
							newAss.setAssName(ResultsList.get(1)[index][1].toString());
							newAss.setUpDate(ResultsList.get(1)[index][2].toString());
							newAss.setDueDate(ResultsList.get(1)[index][3].toString());
							newAss.setSemesterID(ResultsList.get(1)[index][4].toString());
							newAss.setCourseID(ResultsList.get(1)[index][5].toString());
							newAss.setStudentID(ResultsList.get(1)[index][6].toString());
							newAss.setGrade(ResultsList.get(1)[index][7].toString());
							course.getAssignments().add(newAss);
							if(!SelectedChild.getCourses().contains(course))
								SelectedChild.addCourse(course);
						}
					}
				}
				
				
			}
			if(SelectedChild.getCourses().size()>0)
				showCourses();
		}
	}
	
	/**
	 * 
	 */
	private void showCourses(){
		setCourseList(SelectedChild.getCourses());
		setStudent(SelectedChild);
		buildAllCoursesForSemester();
		show();
		}

	public void show(){
		currSemBorderPane.setCenter(pagination);
	}

}
