package app.users.parent.semesters.semester;

import java.net.URL;
import java.util.ResourceBundle;

import app.Main;
import app.common.Assignment;
import app.common.Course;
import app.common.Semester;
import app.common.Student;
import app.common.gui.AbstractCourseSemesterGUI;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class ParentCourseGUIcontroller extends AbstractCourseSemesterGUI implements Initializable{


    @FXML
    private Text txtAvg;

    @FXML
    private TableView<Assignment> tvAss;

    @FXML
    private TableColumn<Assignment, String> tcAssGrade;

    @FXML
    private Text txtCoursreName;

    @FXML
    private TableColumn<Assignment, String> tcAssName;
    
	private TabPane mainTabPane;
	private Course Course;
	private Semester Semester;
	private Student SelectedChild;
    private ObservableList<Assignment>ass_ol;
	
	
	@Override
	public void setMainTabPane(TabPane mainTabPane) {
		this.mainTabPane = mainTabPane;
	}

	@Override
	public void setCourse(Course Course) {
		this.Course = Course;
		txtCoursreName.setText(Course.getCourseName());
	}

	public void buildCourseDetails() {
		int maxAss =0;
		float avg = 0;
		for(Assignment ass : Course.getAssignments())
			if(ass.getStudentID().compareTo(SelectedChild.getID())==0){
				ass_ol.add(ass);
				maxAss++;
				avg+= Double.parseDouble(ass.getGrade());
			}
		avg = avg/maxAss;
		txtAvg.setText(String.valueOf(avg));
		tvAss.setItems(ass_ol);
		
		
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		txtCoursreName.setText("");
		tcAssName.setCellValueFactory(cellData->cellData.getValue().getAssNameProperty());
		tcAssGrade.setCellValueFactory(cellData->cellData.getValue().getGradeProperty());
		ass_ol = FXCollections.observableArrayList();
	}

	public void setSelectedChild(Student selectedChild) {
		this.SelectedChild = selectedChild;
	}



	
	

}
