package app.users.parent;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

import app.Main;
import app.common.Semester;
import app.users.parent.semesters.ParentSemestersGUIcontroller;
import app.users.parent.semesters.semester.ParentSemesterGUIcontroller;
import app.users.secretary.semesters.SecretarySemestersGUIcontroller;
import app.users.secretary.semesters.semester.current.CurrentSemesterGUIcontroller;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.MouseEvent;

public class ParentGUImenuController implements Initializable {

	private TabPane mainTabPane;
	private HashMap<String, Tab> ParentTabs = new HashMap<String, Tab>();
	private ArrayList<Semester> SemestersList = new ArrayList<Semester>();

	@FXML
	private Button mainTab;
	@FXML
	private Button currentSemesterTab;
	@FXML
	private Button prevSemesterTab;

	//***********PRIVATE METHODS CLASS
	
	/**
	 * Builds the GUI with all the information regards to current semester.
	 * 
	 * @throws IOException
	 */
	private void buildCurrentSemesterTab() throws IOException {
		Tab newTab = new Tab("Current Semester");
		FXMLLoader loader = new FXMLLoader();
		ParentTabs.put("currentSemester", newTab);
		newTab.setOnClosed(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				ParentTabs.remove("currentSemester");
			}
		});

		Parent newPane = loader
				.load(Main.class.getResource("users/parent/semesters/semester/ParentSemesterGUI.fxml").openStream());
		ParentSemesterGUIcontroller parentSemesterGUIcontroller = (ParentSemesterGUIcontroller) loader
				.getController();
		parentSemesterGUIcontroller.getData();
		parentSemesterGUIcontroller.setMainTabPane(mainTabPane);
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
	}
	
	/**
	 * Builds the GUI to presents all the previous semester that exists.
	 * 
	 * @throws IOException
	 */
	@FXML
	private void buildPrevSemesterTab() throws IOException {
		Tab newTab = new Tab("Previous Semester");
		FXMLLoader loader = new FXMLLoader();
		ParentTabs.put("prevSemester", newTab);
		newTab.setOnClosed(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				ParentTabs.remove("prevSemester");
			}
		});

		Parent newPane = loader
				.load(Main.class.getResource("users/parent/semesters/ParentSemestersGUI.fxml").openStream());
		ParentSemestersGUIcontroller parentSemestersGUIcontroller = (ParentSemestersGUIcontroller) loader
				.getController();
		parentSemestersGUIcontroller.getData();
		parentSemestersGUIcontroller.setMainTabPane(mainTabPane);
		newTab.setContent(newPane);
		mainTabPane.getTabs().add(newTab);
	}


	//**************PUBLIC METHODS CLASS
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mainTab.setOnAction(new EventHandler<ActionEvent>() {
	
			@Override
			public void handle(ActionEvent event) {
				mainTabPane.getSelectionModel().select(ParentTabs.get("main"));
			}
		});
	
		this.currentSemesterTab.setOnMouseClicked(new EventHandler<MouseEvent>() {
	
			@Override
			public void handle(MouseEvent event) {
				if (!(ParentTabs.containsKey("currentSemester")))
					try {
						buildCurrentSemesterTab();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(ParentTabs.get("currentSemester"));
			}
		});
	
		this.prevSemesterTab.setOnMouseClicked(new EventHandler<MouseEvent>() {
	
			@Override
			public void handle(MouseEvent event) {
				if (!(ParentTabs.containsKey("prevSemester")))
					try {
						buildPrevSemesterTab();
					} catch (IOException e) {
						e.printStackTrace();
					}
				mainTabPane.getSelectionModel().select(ParentTabs.get("prevSemester"));
			}
		});
	}



	public void setPaneTab(TabPane mainTabPane) {
		this.mainTabPane = mainTabPane;
		this.ParentTabs.put("main", mainTabPane.getTabs().get(0));
	}

}
