package serverMain;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import serverClient.dbConnection;

public class ServerLoginGUIcontroller implements Initializable {

	@FXML
	private PasswordField txtPass;
	@FXML
	private Button btnLogin;
	@FXML
	private TextField txtUser;
	@FXML
	private Button btnExit;

	private ServerMain serverMain;
	private Stage stage;
	private dbConnection DB_CONNECTION;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		btnExit.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				System.exit(1);
			}
		});

		btnLogin.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				tryToLogin();
			}
		});

		txtPass.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				if (event.getCode() == KeyCode.ENTER)
					tryToLogin();
			}
		});

		txtUser.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				if (event.getCode() == KeyCode.ENTER)
					tryToLogin();
			}
		});
	}

	private void tryToLogin() {
		if ((txtUser.getText().length() > 0) && (txtPass.getText().length() > 0)) {
			if (DB_CONNECTION.openConnection(txtUser.getText(), txtPass.getText(), serverMain)) {
				serverMain.showMsg("System Manager Login.");
				serverMain.setResultList(DB_CONNECTION.sendSelectQuery("SELECT uID FROM users"));
				stage.close();
				serverMain.getPrimaryStage().show();
			}
		}
	}

	public void setServerMain(ServerMain serverMain) {
		this.serverMain = serverMain;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public void setDbConnection(dbConnection DB_CONNECTION) {
		this.DB_CONNECTION = DB_CONNECTION;
	}

}
