package serverMain;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

import common.AssignmentFile;
import common.Msg;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import serverClient.EchoServer;
import serverClient.dbConnection;
import serverMain.course.ExistCourseGUIcontroller;
import serverMain.course.NewCourseGUIcontroller;
import serverMain.sendmail.NewMailGUIcontroller;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class ServerMain extends Application implements Initializable {

	// ******************FXMLS

	@FXML
	private TableColumn<Msg, String> tcText;
	@FXML
	private Text txtServerStatus;
	@FXML
	private Button btnConnect;
	@FXML
	private Button btnDisconnect;
	@FXML
	private TableColumn<Msg, String> tcSubject;
	@FXML
	private TableColumn<Msg, String> tcForm;
	@FXML
	private Button btnMsg;
	@FXML
	private TableView<Msg> tvMsg;
	@FXML
	private Button btnRestart;
	@FXML
	private TextArea consoleDisplay;
	@FXML
	private Button btnQuery;
	@FXML
	private Button btnExit;
	@FXML
	private MenuItem btnExistCourse;
	@FXML
	private MenuItem btnNewCourse;
	@FXML
	private MenuButton btnCourse;

	// ********************

	private boolean foldersCreated = false;
	private static String[] Args;
	private static EchoServer echoServer;
	private static Stage PrimaryStage;
	private static DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	private static Date date;
	private static Parent mainLayout;
	private static dbConnection DB_CONNECTION = new dbConnection();
	private static Object[][] ResultsList;
	private static Stage loginStage;
	private static ServerLoginGUIcontroller serverLoginGUIcontroller;
	private static ServerMain serverMain;
	private static ObservableList<Msg> MsgsList_OL;
	private NewMailGUIcontroller newMailGUIcontroller;
	private Stage msgStage;
	private Stage existCourse;
	private Stage newCourse;

	// ***********************PUBLIC METHODS

	public static void main(String[] args) {
		Args = args;
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) {
		PrimaryStage = primaryStage;
		
		primaryStage.setOnHiding(new EventHandler<WindowEvent>() {
			
			@Override
			public void handle(WindowEvent event) {
				disConnectEchoServer();
			}
		});
		
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent arg0) {
				System.exit(1);
			}
		});

		showServerManagement();
		loginView();
	}

	/**
	 * Shows the main view for the Server Management.
	 */
	public static void showServerManagement() {
		try {
			FXMLLoader loader0 = new FXMLLoader();
			mainLayout = loader0.load(ServerMain.class.getResource("ServerMain.fxml").openStream());
			serverMain = (ServerMain) loader0.getController();
			Scene scene = new Scene(mainLayout);
			PrimaryStage.setTitle("Server - High School Info System");
			PrimaryStage.initStyle(StageStyle.DECORATED);
			PrimaryStage.setScene(scene);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public dbConnection getDB_CONNECTION() {
		return DB_CONNECTION;
	}

	public void setResultList(Object[][] ResultList) {
		this.ResultsList = ResultList;
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		MsgsList_OL = FXCollections.observableArrayList();
		txtServerStatus.setText("");
		tcForm.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFromName()));
		tcSubject.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSubject()));
		tcText.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getHisMsg()));
		
		disableMenu();

		tvMsg.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				setMsg();
			}
		});

		tvMsg.setOnKeyTyped(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				if (event.getCode() == KeyCode.UP)
					setMsg();
				if (event.getCode() == KeyCode.DOWN)
					setMsg();
			}
		});
	}

	/**
	 * Shows a message in the TextArea - Console Display.
	 * 
	 * @param msg
	 *            - the message that will show.
	 */
	public void showMsg(String msg) {
		date = new Date();
		consoleDisplay.appendText(dateFormat.format(date) + ">" + msg + "\n");
	}
	
	public boolean sendMsgToClient(Msg msg){
		return echoServer.sendToClientReply(msg);
	}

	// ***********************PROTECTED METHODS

	protected Stage getPrimaryStage() {
		return PrimaryStage;
	}
	

	// ***********************PRIVATE METHODS

	/**
	 * Try to Disconnected for the EchoServer and Exit from the System.
	 */
	@FXML
	private void exit() {
		disConnectEchoServer();
		System.exit(1);
	}

	/**
	 * Try to Connect to the EchoServer and if need creates folders for all
	 * users for saving files.
	 */
	@FXML
	private void connectEchoServer() {
		if (!foldersCreated) {
			buildFoldersForUsers();
			foldersCreated = true;
		}
		if (echoServer == null) {
			int port = 0; // Port to listen on

			try {
				port = Integer.parseInt(Args[0]); // Get port from command line
			} catch (Throwable t) {
				port = EchoServer.DEFAULT_PORT; // Set port to 5555
			}

			echoServer = new EchoServer(port);
			echoServer.setServerMain(this);
			enableMenu();
			try {
				echoServer.listen(); // Start listening for connections
				setStatusOnine();
			} catch (Exception ex) {
				showMsg("ERROR - Could not listen for clients!");

			}
		} else {
			showMsg("Server already open.");
		}
	}

	/**
	 * Try to Disconnect from the EchoServer.
	 */
	@FXML
	private void disConnectEchoServer() {
		if (echoServer != null)
			try {
				echoServer.closeAllConnectionToDB();
				echoServer.close();
				echoServer = null;
				setStatusOfline();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	/**
	 * Shows the login view for the server.
	 */
	private static void loginView() {
		try {
			loginStage = new Stage();
			FXMLLoader loader2 = new FXMLLoader();
			Parent loginItems = loader2.load((ServerMain.class.getResource("ServerLoginGUI.fxml").openStream()));
			serverLoginGUIcontroller = (ServerLoginGUIcontroller) loader2.getController();
			serverLoginGUIcontroller.setServerMain(serverMain);
			serverLoginGUIcontroller.setDbConnection(DB_CONNECTION);
			serverLoginGUIcontroller.setStage(loginStage);
			Scene scene = new Scene(loginItems);
			loginStage.setScene(scene);
			loginStage.setTitle("Server - High School Info System - Login");
			loginStage.initStyle(StageStyle.UNDECORATED);
			loginStage.setScene(scene);
			loginStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Change the status text of the server for Online.
	 */
	private void setStatusOnine() {
		txtServerStatus.setText("Server Is Online");
		txtServerStatus.setFill(Color.GREEN);
	}

	/**
	 * Change the status text of the server to offline.
	 */
	private void setStatusOfline() {
		txtServerStatus.setText("Server Is Offline");
		txtServerStatus.setFill(Color.RED);
	}

	/**
	 * Shows the stage for send messages and to show the text - Only if not
	 * already opened.
	 */
	@FXML
	private void showMessage() {
		if(!MsgsList_OL.isEmpty())
		if (msgStage == null)
			try {
				FXMLLoader loader3 = new FXMLLoader();
				Parent MsgItem = loader3.load(ServerMain.class.getResource("sendmail/NewMailGUI.fxml").openStream());
				newMailGUIcontroller = (NewMailGUIcontroller) loader3.getController();
				newMailGUIcontroller.setServerMain(this);
				setMsg();
				Scene scene = new Scene(MsgItem);
				msgStage = new Stage();
				newMailGUIcontroller.setStage(msgStage);
				msgStage.setScene(scene);
				msgStage.setTitle("Server - High School Info System");
				msgStage.show();
				msgStage.setOnHidden(new EventHandler<WindowEvent>() {
					
					@Override
					public void handle(WindowEvent event) {
						msgStage=null;
					}
				});
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	/**
	 * Waiting for 2 seconds - synchronized.
	 * 
	 * @throws InterruptedException
	 */
	private synchronized void waitAwhile() throws InterruptedException {
		wait(2000);
	}

	/**
	 * Builds for each user (Student/Teacher) a folders for their files.
	 * 
	 * @return
	 */
	private void buildFoldersForUsers() {
		Task<Void> folderTask = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				showMsg("Starts to build folders for users...");
				waitAwhile();
				try {
					int maxRows = ResultsList.length;
					int index;
					Path path;
					for (index = 0; index < maxRows; index++) {
						path = Paths.get("D:\\Server\\" + ResultsList[index][0]);
						// if directory exists?
						if (!Files.exists(path))
							Files.createDirectories(path);
					}
					showMsg("Finished to build folders for users.");
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}
		};
		
		folderTask.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			
			@Override
			public void handle(WorkerStateEvent arg0) {
				getsAllMsgs();
			}
		});
		Thread t = new Thread(folderTask);
		t.start();
	}

	/**
	 * Try to Restart the ExhoServer. Disconnected and then Connect.
	 */
	@FXML
	private void restartServer() {
		if (echoServer != null) {
			showMsg("Restarting the Server....");
			disConnectEchoServer();
			connectEchoServer();
		}
	}

	/**
	 * Shows the stage for new Course - Only if not already opened.
	 */
	@FXML
	private void newCourse() {
		try {
			FXMLLoader loader1 = new FXMLLoader();
			Parent item = loader1.load(ServerMain.class.getResource("course/NewCourseGUI.fxml").openStream());
			NewCourseGUIcontroller newCourseGUIcontroller = (NewCourseGUIcontroller) loader1.getController();
			String query = "SELECT c.courseID,c.courseName,c.studyHoursPerWeek , d.deptID ,d.deptName FROM courses c , depts d WHERE c.departmentID=d.deptID";
			ResultsList = DB_CONNECTION.sendSelectQuery(query);
			newCourseGUIcontroller.setResultList(ResultsList);
			newCourseGUIcontroller.setServerMain(this);
			newCourseGUIcontroller.buildLists();
			Scene scene = new Scene(item);
			newCourse = new Stage();
			newCourse.setScene(scene);
			newCourse.setTitle("Server - High School Info System");
			newCourse.show();
			newCourse.setOnHidden(new EventHandler<WindowEvent>() {
				
				@Override
				public void handle(WindowEvent event) {
					newCourse=null;
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Shows the stage for exist Course - Only if not already opened.
	 */
	@FXML
	private void existCourse() {
		try {
			String query = "SELECT c.courseID,c.courseName,c.studyHoursPerWeek , d.deptID ,d.deptName FROM courses c , depts d WHERE c.departmentID=d.deptID";
			ResultsList = DB_CONNECTION.sendSelectQuery(query);
			FXMLLoader loader2 = new FXMLLoader();
			Parent msg = loader2.load(ServerMain.class.getResource("course/ExistCourseGUI.fxml").openStream());
			ExistCourseGUIcontroller existCourseGUIcontroller = (ExistCourseGUIcontroller) loader2.getController();
			existCourseGUIcontroller.setResultList(ResultsList);
			existCourseGUIcontroller.buildLists();
			existCourseGUIcontroller.setMainServer(this);
			Scene scene = new Scene(msg);
			existCourse = new Stage();
			existCourse.setScene(scene);
			existCourse.setTitle("Server - High School Info System");
			existCourse.show();
			existCourse.setOnHidden(new EventHandler<WindowEvent>() {
				
				@Override
				public void handle(WindowEvent event) {
					existCourse = null;
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	
	@FXML
	private void sendQuery() {

	}

	/**
	 * Sets the message that selected into the newMailGUIcontroller.
	 */
	private void setMsg() {
		if (newMailGUIcontroller != null){
			Msg msg = tvMsg.getSelectionModel().getSelectedItem();
			if (msg != null)
				newMailGUIcontroller.setMsg(tvMsg.getSelectionModel().getSelectedItem());
	}
	}

	/**
	 * Disables the menu.
	 */
	private void disableMenu() {
		btnRestart.setDisable(true);
		btnNewCourse.setDisable(true);
		btnMsg.setDisable(true);
		btnQuery.setDisable(true);
		btnCourse.setDisable(true);

	}

	/**
	 * Enables the menu.
	 */
	private void enableMenu() {
		btnRestart.setDisable(false);
		btnNewCourse.setDisable(false);
		btnMsg.setDisable(false);
		btnQuery.setDisable(false);
		btnCourse.setDisable(false);
	}
	
	/**
	 * Gets all unanswered messages.
	 */
	private void getsAllMsgs(){
		ResultsList = DB_CONNECTION.sendSelectQuery("SELECT * FROM messages m WHERE m.mStatus = '0'");
		if((ResultsList!=null)&&(ResultsList[0][0]!=null)){
			Msg newMsg;
			int maxRows = ResultsList.length;
			for (int index = 0 ; index<maxRows ; index++){
				newMsg = new Msg();
				newMsg.setmID(ResultsList[index][0].toString());
				newMsg.setHisMsg(ResultsList[index][1].toString());
				newMsg.setFromID(ResultsList[index][3].toString());
				newMsg.setSubject(ResultsList[index][5].toString());
				newMsg.setFromName(ResultsList[index][6].toString());
				MsgsList_OL.add(newMsg);
			}
			tvMsg.setItems(MsgsList_OL);
		}
	}

	/**
	 * Removes the message from the list.
	 * @param msg - The message.
	 */
	public void removeMsg(Msg msg) {
		MsgsList_OL.remove(msg);
	}

	public void addNewMsg(Msg msg) {
		MsgsList_OL.add(msg);
	}
}
