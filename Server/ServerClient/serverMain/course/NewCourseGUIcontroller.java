package serverMain.course;

import common.Course;
import common.Dept;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import serverMain.ServerMain;

public class NewCourseGUIcontroller {

	@FXML
	private TextField txtCourseID;
	@FXML
	private Button btnNewCourse;
	@FXML
	private TextField txtName;
	@FXML
	private ComboBox<Course> cbPrevCourse;
	@FXML
	private Text txtErr;
	@FXML
	private TextField txtHrs;
	@FXML
	private ComboBox<Course> cbAllCourse;
	@FXML
	private Button btnAddCourse;
	@FXML
	private ComboBox<Dept> cbDepts;
	@FXML
	private Button btnRemoveCourse;

	private Object[][] ResultsList;
	private ServerMain serverMain;
	private ObservableList<Course> AllCourses_OL;
	private ObservableList<Course> PrevCourses_OL;
	private ObservableList<Dept> AllDept_OL;

	public void setResultList(Object[][] resultsList) {
		this.ResultsList = resultsList;
	}

	@FXML
	private void createNewCourse() {
		if (txtCourseID.getText().length() == 5) {
			if (txtName.getText().length() > 0) {
				if (cbDepts.getSelectionModel().getSelectedItem() != null) {
					if (!searchCourseByID(txtCourseID.getText(), txtName.getText())) {
						if ((txtHrs.getText().length() > 0) && (isNumeric(txtHrs.getText()))) {
							if (!searchCourseByID(txtCourseID.getText(), txtName.getText())) {
								serverMain.getDB_CONNECTION().sendInsertQuery("INSERT INTO () VALUES ('"
										+ txtCourseID.getText() + "','" + txtName.getText() + "','" + txtHrs.getText());
								for (Course course : PrevCourses_OL)
									serverMain.getDB_CONNECTION()
											.sendInsertQuery("INSERT INTO () course_course VALUES ('"
													+ txtCourseID.getText() + "','" + course.getCourseID() + "')");
								disableError();
							}
						} else
							showError();
					} else
						showError();
				} else
					showError();
			} else
				showError();
		} else
			showError();
	}

	private void disableError() {
		txtErr.setText("");
	}

	@FXML
	private void addPrevCourse() {
		Course SelesctedCourse = cbAllCourse.getSelectionModel().getSelectedItem();
		if (SelesctedCourse != null) {
			PrevCourses_OL.add(SelesctedCourse);

			AllCourses_OL.remove(SelesctedCourse);
		}
	}

	@FXML
	private void removePrevCourse() {
		Course SelesctedCourse = cbPrevCourse.getSelectionModel().getSelectedItem();
		if (SelesctedCourse != null) {
			AllCourses_OL.add(SelesctedCourse);
			PrevCourses_OL.remove(SelesctedCourse);
		}
	}

	private boolean searchCourseByID(String CourseID, String CourseName) {
		int maxRows = ResultsList.length;
		for (int index = 0; index < maxRows; index++) {
			if ((ResultsList[index][0].toString().compareTo(CourseID) == 0)
					|| (ResultsList[index][1].toString().compareTo(CourseName) == 0))
				return true;
		}
		return false;
	}

	private boolean isNumeric(String str) {
		try {
			int hours = Integer.parseInt(str);
			if((hours>72)||(hours<=0))
				return false;
		} catch (NumberFormatException e) {
			showError();
			return false;
		}
		return true;
	}

	private void showError() {
		txtErr.setFill(Color.RED);
		txtErr.setText("Error , check fields.");
	}

	public void setServerMain(ServerMain serverMain) {
		this.serverMain = serverMain;
	}

	public void buildLists() {

		if (ResultsList[0][0] != null) {
			AllCourses_OL = FXCollections.observableArrayList();
			PrevCourses_OL = FXCollections.observableArrayList();
			AllDept_OL = FXCollections.observableArrayList();
			Course course;
			Dept dept;
			int index;
			int maxRows = ResultsList.length;
			for (index = 0; index < maxRows; index++) {
				course = new Course(ResultsList[index][0].toString());
				course.setCourseName(ResultsList[index][1].toString());
				course.setHoursPerWeek(ResultsList[index][2].toString());
				course.setCourseDeptID(ResultsList[index][3].toString());
				course.setCourseDeptName(ResultsList[index][4].toString());
				dept = new Dept(ResultsList[index][3].toString());
				dept.setDeprtName(ResultsList[index][4].toString());
				AllCourses_OL.add(course);
				if (!SearchDeptExist(dept))
					AllDept_OL.add(dept);
			}

		}
		cbAllCourse.setItems(AllCourses_OL);
		cbPrevCourse.setItems(PrevCourses_OL);
		cbDepts.setItems(AllDept_OL);

	}

	private boolean SearchDeptExist(Dept dept) {
		for (Dept d : AllDept_OL) {
			if (d.getDeptID().compareTo(dept.getDeptID()) == 0)
				return true;
		}
		return false;
	}
}
