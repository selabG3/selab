package serverMain.course;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import common.Course;
import common.Dept;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import serverMain.ServerMain;

public class ExistCourseGUIcontroller {

	@FXML
	private TextField txtCourseID;
	@FXML
	private Button btnSave;
	@FXML
	private Button btnSearch;
	@FXML
	private TextField txtName;
	@FXML
	private ComboBox<Course> cbPrevCourse;
	@FXML
	private Text txtErr;
	@FXML
	private TextField txtHrs;
	@FXML
	private ComboBox<Course> cbAllCourse;
	@FXML
	private Button btnAddCourse;
	@FXML
	private ComboBox<Dept> cbDepts;
	@FXML
	private Button btnRemoveCourse;
	@FXML
	private Text txtCurrDept;

	private Object[][] ResultsList;
	private ArrayList<Course> AllCourses = new ArrayList<Course>();
	private ObservableList<Course> AllCourses_OL;
	private ObservableList<Course> PrevCourses_OL;
	private ObservableList<Dept> AllDept_OL;
	private ServerMain serverMain;

	
	@FXML
	private void saveCourse() {
		if (txtCourseID.getText().length() > 0)
			if (txtName.getText().length() > 0)
				if ((txtHrs.getText().length() > 0) && (isNumeric(txtHrs.getText())))
					if (!searchCourseByID(txtCourseID.getText(), txtName.getText())) {
						serverMain.getDB_CONNECTION().sendInsertQuery("INSERT INTO () VALUES ('" + txtCourseID.getText()
								+ "','" + txtName.getText() + "','" + txtHrs);
						for(Course course : PrevCourses_OL)
							serverMain.getDB_CONNECTION().sendInsertQuery("INSERT INTO () course_course VALUES ('"+txtCourseID.getText()+"','"+course.getCourseID()+"')");
					}
	}
	
	
	@FXML
	private void addPrevCourse() {
		Course SelesctedCourse = cbAllCourse.getSelectionModel().getSelectedItem();
		if (SelesctedCourse != null) {
			PrevCourses_OL.add(SelesctedCourse);

			AllCourses_OL.remove(SelesctedCourse);
		}
	}

	@FXML
	private void removePrevCourse() {
		Course SelesctedCourse = cbPrevCourse.getSelectionModel().getSelectedItem();
		if (SelesctedCourse != null) {
			AllCourses_OL.add(SelesctedCourse);
			PrevCourses_OL.remove(SelesctedCourse);
		}
	}



	public void buildLists() {
		Course course;
		Dept dept;
		int index, maxRows;
		if (ResultsList[0][0] != null) {
			AllCourses_OL = FXCollections.observableArrayList();
			AllDept_OL = FXCollections.observableArrayList();
			maxRows = ResultsList.length;
			for (index = 0; index < maxRows; index++) {
				course = new Course(ResultsList[index][0].toString());
				course.setCourseName(ResultsList[index][1].toString());
				course.setHoursPerWeek(ResultsList[index][2].toString());
				course.setCourseDeptID(ResultsList[index][3].toString());
				course.setCourseDeptName(ResultsList[index][4].toString());
				dept = new Dept(ResultsList[index][3].toString());
				dept.setDeprtName(ResultsList[index][4].toString());
				AllCourses.add(course);
				AllCourses_OL.add(course);
				if (!SearchDeptExist(dept))
					AllDept_OL.add(dept);
			}

		}
		cbAllCourse.setItems(AllCourses_OL);
		cbDepts.setItems(AllDept_OL);
		PrevCourses_OL = FXCollections.observableArrayList();
	}

	@FXML
	private void searchCourse() {
		Course course;
		Dept dept;
		int index, maxRows;
		//Get course details.
		String query = "SELECT c.courseID,c.courseName,c.studyHoursPerWeek , d.deptID ,d.deptName FROM courses c , depts d WHERE c.departmentID=d.deptID AND c.courseID= '"
				+ txtCourseID.getText() + "'";
		ResultsList = serverMain.getDB_CONNECTION().sendSelectQuery(query);
		if (ResultsList[0][0] != null) {
			txtName.setText(ResultsList[0][1].toString());
			txtHrs.setText(ResultsList[0][2].toString());
			txtCurrDept.setText(ResultsList[0][4].toString());
			AllCourses_OL.setAll(AllCourses);
		}
		//Get course previous courses.
		query = "SELECT * FROM course_course cc WHERE cc.curCourseID='"+txtCourseID.getText()+"';";
		ResultsList = null;
		ResultsList = serverMain.getDB_CONNECTION().sendSelectQuery(query);
		if(ResultsList[0][0]!=null){
			maxRows = ResultsList.length;
			for(index = 0 ; index<maxRows ; index++){
				course = SearchCourseExist(ResultsList[0][1].toString());
				if(course!=null){
					PrevCourses_OL.add(course);
					AllCourses_OL.remove(course);
				}
			}
		}
		cbPrevCourse.setItems(PrevCourses_OL);
	}

	private boolean SearchDeptExist(Dept dept) {
		for (Dept d : AllDept_OL) {
			if (d.getDeptID().compareTo(dept.getDeptID()) == 0)
				return true;
		}
		return false;
	}

	private boolean SearchCourseExist(Course course) {
		for (Course c : AllCourses_OL) {
			if (c.getCourseID().compareTo(course.getCourseID()) == 0)
				return true;
		}
		return false;
	}
	
	private Course SearchCourseExist(String courseID) {
		for (Course c : AllCourses_OL) {
			if (c.getCourseID().compareTo(courseID) == 0)
				return c;
		}
		return null;
	}
	
	private boolean searchCourseByID(String CourseID, String CourseName) {
		int maxRows = ResultsList.length;
		for (int index = 0; index < maxRows; index++) {
			if ((ResultsList[index][0].toString().compareTo(CourseID) == 0)
					|| (ResultsList[index][1].toString().compareTo(CourseName) == 0))
				return true;
		}
		return false;
	}

	private boolean isNumeric(String str) {
		try {
			int hours = Integer.parseInt(str);
		} catch (NumberFormatException e) {
			showError();
			return false;
		}
		return true;
	}

	private void showError() {
		txtErr.setFill(Color.RED);
		txtErr.setText("Error , check fields.");
	}

	public void setResultList(Object[][] resultList) {
		this.ResultsList = resultList;
	}

	public void setMainServer(ServerMain serverMain) {
		this.serverMain = serverMain;
	}



}
