package serverMain.sendmail;

import java.net.URL;
import java.util.ResourceBundle;

import common.Msg;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import serverMain.ServerMain;

public class NewMailGUIcontroller implements Initializable {

	// ******************FXMLS

	@FXML
	private Text txtTo;
	@FXML
	private TextArea txtMsg;
	@FXML
	private Button btnMsg;
	@FXML
	private Text txtSub;
	// *****************

	private Msg msg;
	private ServerMain serverMain;
	private Stage msgStage;

	// *****************PUBLIC METHODS

	/**
	 * Sets the name of the sender.
	 * 
	 * @param msg
	 *            - instance of a message.
	 */
	public void setMsg(Msg msg) {
		this.msg = msg;
		if ((txtTo != null) && (txtSub != null)) {
			txtTo.setText(msg.getFromName());
			txtSub.setText(msg.getSubject());
		}
	}

	/**
	 * Sets a instance of ServerMain.
	 * 
	 * @param serverMain
	 */
	public void setServerMain(ServerMain serverMain) {
		this.serverMain = serverMain;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	// *********************PRIVATE METHODS

	/**
	 * Sent a message to the client.
	 */
	@FXML
	private void sendReply() {
		if(txtMsg.getText().length()>0){
		serverMain.getDB_CONNECTION().sendInsertQuery("UPDATE messages SET mAnswer='" + txtMsg.getText()
				+ "' , mStatus='1' WHERE mID='" + msg.getmID() + "'");
		msg.setAnsMsg(txtMsg.getText());
		if(!serverMain.sendMsgToClient(msg))
			serverMain.showMsg("Client will get the message later.");
		serverMain.removeMsg(msg);
		msgStage.close();
		}
	}

	public void setStage(Stage msgStage) {
		this.msgStage = msgStage;
	}

}
