package common;

import javafx.beans.property.SimpleStringProperty;

public class Dept {

	
	private SimpleStringProperty DeptID;
	private SimpleStringProperty DeprtName;
	
	
	public Dept(){
		
	}
	
	public Dept(String deptID){
		DeptID = new SimpleStringProperty(deptID);
	}
	
	/**
	 * 
	 */
	public String toString(){
		return DeprtName.get()+" "+DeptID.get();
	}
	
	
	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getDeptIdProperty(){
		return DeptID;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getDeptID(){
		return DeptID.get();
	}
	
	/**
	 * 
	 * @param deptID
	 */
	public void setDeptID (String deptID){
		DeptID = new SimpleStringProperty(deptID);
	}
	
	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getDeprtNameProperty(){
		return DeprtName;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getDeprtName(){
		return DeprtName.get();
	}
	
	/**
	 * 
	 * @param deptName
	 */
	public void setDeprtName (String deptName){
		DeprtName = new SimpleStringProperty(deptName);
	}
}
