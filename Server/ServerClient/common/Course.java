package common;

import java.util.ArrayList;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Course {

	private SimpleStringProperty CourseID;
	private SimpleStringProperty CourseName;
	private SimpleStringProperty CourseDeptID;
	private SimpleStringProperty CourseDeptName;
	private SimpleIntegerProperty HoursPerWeek;
	private ArrayList<Course> PrevCourses = new ArrayList<Course>();

	// ****CONSTRACTURS

	public Course() {

	}

	public Course(String CourseID) {
		this.CourseID = new SimpleStringProperty(CourseID);
	}

	// ****CLASS METHODS

	/**
	 * 
	 */
	public String toString() {
		return CourseName.get() + " " + CourseID.get();
	}

	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getCourseIDProperty() {
		return CourseID;
	}

	/**
	 * 
	 * @return
	 */
	public String getCourseID() {
		return CourseID.get();
	}

	/**
	 * 
	 * @param courseID
	 */
	public void setCourseID(String courseID) {
		this.CourseID = new SimpleStringProperty(courseID);
	}

	/**
	 * 
	 * @return
	 */
	public String getCourseName() {
		return CourseName.get();
	}

	/**
	 * 
	 * @return
	 */
	public SimpleStringProperty getCourseNameProperty() {
		return CourseName;
	}

	/**
	 * 
	 * @param courseName
	 */
	public void setCourseName(String courseName) {
		this.CourseName = new SimpleStringProperty(courseName);
	}

	/**
	 * Return the course dept id property.
	 * 
	 * @return
	 */
	public SimpleStringProperty getCourseDeptIdProperty() {
		return CourseDeptID;
	}

	/**
	 * Return the course dept id.
	 * 
	 * @return
	 */
	public String getCourseDeptID() {
		return CourseDeptID.get();
	}

	/**
	 * 
	 * @param dept
	 */
	public void setCourseDeptID(String deptID) {
		this.CourseDeptID = new SimpleStringProperty(deptID);
	}

	/**
	 * Return the course dept name property.
	 * 
	 * @return
	 */
	public SimpleStringProperty getCourseDeptNameProperty() {
		return CourseDeptName;
	}

	/**
	 * Return the course dept name.
	 * 
	 * @return
	 */
	public String getCourseDeptName() {
		return CourseDeptName.get();
	}

	/**
	 * 
	 * @param dept
	 */
	public void setCourseDeptName(String deptName) {
		this.CourseDeptName = new SimpleStringProperty(deptName);
	}

	/**
	 * Set the cpurse's teaching hours in a week.
	 * 
	 * @param maxHoursPerWeek
	 */
	public void setHoursPerWeek(int hoursPerWeek) {
		this.HoursPerWeek = new SimpleIntegerProperty(hoursPerWeek);
	}

	/**
	 * 
	 * @param hoursPerWeek
	 */
	public void setHoursPerWeek(String hoursPerWeek) {
		this.HoursPerWeek = new SimpleIntegerProperty((Integer.parseInt(hoursPerWeek)));
	}

	/**
	 * Returns the cpurse's teaching hours in a week property.
	 * 
	 * @return
	 */
	public SimpleIntegerProperty getHoursPerWeekProperty() {
		return HoursPerWeek;
	}

	/**
	 * Returns the cpurse's teaching hours in a week.
	 * 
	 * @return
	 */
	public int getHoursPerWeek() {
		return HoursPerWeek.get();
	}

	/**
	 * 
	 * @return
	 */
	public String getHoursPerWeekString() {
		return String.valueOf(this.HoursPerWeek.get());
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Course> getPrevCoursesList() {
		return PrevCourses;
	}

	/**
	 * 
	 * @param prevCourse
	 */
	public void addNewPrevCourse(Course prevCourse) {
		if (!checkClassRegisteration(prevCourse))
			PrevCourses.add(prevCourse);
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public Course getCourse(int index) {
		return PrevCourses.get(index);
	}

	/**
	 * 
	 * @param course
	 * @return
	 */
	public boolean checkClassRegisteration(Course course) {
		if (PrevCourses.contains(course))
			return true;
		return false;
	}

	/**
	 * 
	 * @param course
	 * @return
	 */
	public boolean removePrevCourse(Course course) {
		if (checkClassRegisteration(course)) {
			PrevCourses.remove(course);
			return true;
		}
		return false;
	}

}
