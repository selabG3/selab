package serverClient;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import common.AssignmentFile;
import javafx.application.Platform;
import javafx.scene.control.TextArea;
import serverMain.ServerMain;

public class dbConnection {

	private ResultSet results = null;
	private Connection connection = null;
	private Statement statement = null;
	private int userType;
	private ServerMain serverMain;

	public boolean openConnection(String UserName, String Password, ServerMain serverMain) {
		try {
			this.serverMain = serverMain;
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sw_lab?useSSL=false", UserName,
					Password);
			statement = connection.createStatement();
			if(UserName.compareTo("root")!=0){
				String sql = "SELECT uType FROM sw_lab.users WHERE uID='" + UserName + "'";
				results = statement.executeQuery(sql);
				results.next();
				userType = results.getInt("uType");
			}
			return true;
		} catch (SQLException e) {
			serverMain.showMsg("SQL Problem - dbConnection - openConnection.\n" + e.getMessage());
			userType = 99;
		} catch (ClassNotFoundException e) {
			serverMain.showMsg("ClassNotFoundException - dbConnection - openConnection.");
			userType = 99;
		} catch (Exception e) {
			serverMain.showMsg(e.getStackTrace() + "\n" + e.getMessage());
			userType = 99;
		}
		return false;
	}

	public Object[][] sendSelectQuery(String query) {
		Object[][] TableResults = null;
		try {
			serverMain.showMsg(query);
			statement = connection.createStatement();
			results = statement.executeQuery(query);
			int maxCols = results.getMetaData().getColumnCount();
			int maxRows = getTotalRows() + 1;
			int currentRow = 0;
			TableResults = new Object[maxRows][maxCols];

			results.first();
			do {
				for (int j = 0; j < maxCols; j++) {
					if (results.getObject(j + 1) != null) {
						TableResults[currentRow][j] = results.getObject(j + 1);
					} else
						TableResults[currentRow][j] = "";
				}
				if(new File(results.getObject(maxCols).toString()).isFile())
					TableResults[currentRow][maxCols-1] = getFileBytes(results.getObject(maxCols).toString());
					//if(TableResults[currentRow][maxCols-1]==null)
					//	TableResults[currentRow][maxCols-1]=results.getObject(maxCols).toString();
				currentRow++;
			} while (results.next());

		} catch (SQLException e) {
			serverMain.showMsg("SQL Problem - dbConnection - sendSelectQuery.\n" + e.getMessage());
		} catch (NullPointerException e) {
			serverMain.showMsg("Null - dbConnection - sendSelectQuery.");
		}

		return TableResults;
	}

	public void sendDeleteQuery(String query) {
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
		} catch (SQLException e) {
			serverMain.showMsg("SQL Problem - dbConnection - sendDeleteQuery.\n" + e.getMessage());
		}

	}

	public void sendUpdateQuery(String query) {
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
		} catch (SQLException e) {
			serverMain.showMsg("SQL Problem - dbConnection - sendUpdateQuery.\n" + e.getMessage());
		}
	}

	public int sendInsertQuery(String query) {
		try {
			Integer number = 0;
			Integer result = -1;
			number = statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
				result = rs.getInt(1);
			}
			return result;
			// statement = connection.createStatement();
			// statement.executeUpdate(query);
		} catch (SQLException e) {
			serverMain.showMsg("SQL Problem - dbConnection - sendInsertQuery.\n" + e.getMessage());
		}
		return 0;
	}


	public void sendUpdateOrInsertFileQuery(Object[] objects) {
		try {
			String updateSQL = (String) objects[1];
			PreparedStatement pstmt = connection.prepareStatement(updateSQL);
			AssignmentFile readWriteFile = (AssignmentFile) objects[2];

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutput out = null;
			try {
				out = new ObjectOutputStream(bos);
				out.writeObject(readWriteFile);
				pstmt.setBinaryStream(1, new ByteArrayInputStream(bos.toByteArray()));
				pstmt.executeUpdate();
				out.flush();
			} catch (IOException e) {
			} finally {
				try {
					bos.close();
				} catch (IOException e) {
					serverMain.showMsg("IO Problem - dbConnection - sendUpdateOrInsertFileQuery.\n" + e.getMessage());
				}
			}
		} catch (SQLException e) {
			serverMain.showMsg("SQL Problem - dbConnection - sendUpdateOrInsertFileQuery.\n" + e.getMessage());
		}
	}

	public void closeConnection() {
		try {
			this.connection.close();
			this.statement.close();
			this.results.close();
		} catch (SQLException e) {
			serverMain.showMsg("SQL Problem - dbConnection - closeConnection.\n" + e.getMessage());
		}
	}

	public int getType() {
		return userType;
	}

	/**
	 * 
	 * @return
	 */
	private int getTotalRows() {
		int totalRows = 0;
		try {
			results.first();
			while (results.next())
				totalRows++;
			results.first();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return totalRows;
	}

	public void resetResults() {
		try {
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private byte[] getFileBytes(String fileDest) {
		byte[] mybytearray = null;
		try {
			File newFile = new File(fileDest);
			if(newFile.isFile()){
			mybytearray = new byte[(int) newFile.length()];
			
			FileInputStream fis = new FileInputStream(newFile);
		    BufferedInputStream bis = new BufferedInputStream(fis);	
			bis.read(mybytearray);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mybytearray;
	}

	public void sendInsertUpdateWithFile(String str, String extension, String dest) {
		try {
			PreparedStatement pstmt = connection.prepareStatement(str);
			pstmt.setString(1, extension);
			pstmt.setString(2, dest);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
