package serverClient;
// This file contains material supporting section 3.7 of the textbook:

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

// "Object Oriented Software Engineering" and is issued under the open-source

// license found at www.lloseng.com 
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


import common.AssignmentFile;
import common.Msg;
import javafx.application.Platform;
import javafx.scene.control.TextArea;
import ocsf.server.AbstractServer;
import ocsf.server.ConnectionToClient;
import serverMain.ServerMain;

/**
 * This class overrides some of the methods in the abstract superclass in order
 * to give more functionality to the server.
 *
 * @author Dr Timothy C. Lethbridge
 * @author Dr Robert Lagani&egrave;re
 * @author Fran&ccedil;ois B&eacute;langer
 * @author Paul Holden
 * @version July 2000
 */
public class EchoServer extends AbstractServer {
	// Class variables *************************************************
	private static dbConnection dbConnection;
	private ServerMain serverMain;
	
	/**
	 * The default port to listen on.
	 */
	final public static int DEFAULT_PORT = 5555;

	// Constructors ****************************************************

	/**
	 * Constructs an instance of the echo server.
	 *
	 * @param port
	 *            The port number to connect on.
	 */
	public EchoServer(int port) {
		super(port);
	}

	// Instance methods ************************************************

	/**
	 * This method handles any messages received from the client.
	 *
	 * @param msg
	 *            The message received from the client.
	 * @param client
	 *            The connection from which the message originated.
	 */
	@SuppressWarnings("unchecked")
	public void handleMessageFromClient(Object msg, ConnectionToClient client) {

		if (msg instanceof String) {
			String stringMsg = (String) msg;
			serverMain.showMsg("Message received:client " + client);
			if (client.getInfo("dbConnection") == null) {
				dbConnection = new dbConnection();
				String[] newStringMsg = stringMsg.split(" ");
				if (newStringMsg[0].matches("Username")) {
					if (dbConnection.openConnection(newStringMsg[1], newStringMsg[3], serverMain))
						client.setInfo("dbConnection", dbConnection);
						client.setInfo("Username", newStringMsg[1]);
						serverMain.showMsg("Message send: " + dbConnection.getType() + " to " + client);
					this.sendToClient(dbConnection.getType(), client);
				}
			} else if (stringMsg.compareTo("closeConnection") == 0) {
				dbConnection = (dbConnection) client.getInfo("dbConnection");
				if (stringMsg.compareTo("closeConnection") == 0) {
					dbConnection.closeConnection();
					client.setInfo("dbConnection", null);
				}
			}
		} else if (msg instanceof String[]) {
			int maxIndex = 0;
			ArrayList<Object[][]> obj = new ArrayList<Object[][]>();
			String[] al = (String[]) msg;
			maxIndex = al.length;
			for (int index = 0; index < maxIndex; index++) {
				if (((al[index]).contains("UPDATE"))||(al[index]).contains("update"))
					dbConnection.sendUpdateQuery(al[index]);
				else if (((al[index]).contains("SELECT"))||(al[index]).contains("select"))
					obj.add(dbConnection.sendSelectQuery(al[index]));
				else if (((al[index]).contains("INSERT"))||(al[index]).contains("insert"))
					dbConnection.sendInsertQuery(al[index]);
				else if (((al[index]).contains("DELETE"))||(al[index]).contains("delete"))
					dbConnection.sendDeleteQuery(al[index]);
			}
			this.sendToClient(obj, client);
		} else if (msg instanceof ArrayList<?>) {
			ArrayList<Object[][]> obj = new ArrayList<Object[][]>();
			ArrayList<String> queryList = (ArrayList<String>) msg;
			int maxIndex = queryList.size();
			// transform list of queries from ArrayList type into String[]
			for (int i = 0; i < queryList.size(); i++)
				if(queryList.get(i)!=null){
					 final int index = i;
					 serverMain.showMsg("Message send: " + queryList.get(index) + " to  MySQL.");
				if ((queryList.get(i)).contains("UPDATE") || ((queryList.get(i)).contains("update")))
					dbConnection.sendUpdateQuery(queryList.get(i));
				else if ((queryList.get(i)).contains("SELECT") || ((queryList.get(i)).contains("select"))){
					obj.add(dbConnection.sendSelectQuery(queryList.get(i)));
				}
				else if (((queryList.get(i)).contains("INSERT") || ((queryList.get(i)).contains("insert"))))
					dbConnection.sendInsertQuery((queryList.get(i)));
				else if ((queryList.get(i)).contains("DELETE") || ((queryList.get(i)).contains("delete")))
					dbConnection.sendDeleteQuery((queryList.get(i)));
			}
			this.sendToClient(obj, client);
		}else if(msg instanceof AssignmentFile){
			AssignmentFile assFile = (AssignmentFile)msg;
			//if((assFile.getQuery().compareTo("INSERT")==0)||(assFile.getQuery().compareTo("insert")==0)){
				serverMain.showMsg("File resived.");
				File dest= new File("D:\\Server\\"+assFile.getUserName()+"\\"+assFile.getSemesterID()+"\\"+assFile.getCourseID()+"\\"+assFile.getFileName());
				Path destPath = Paths.get("D:\\Server\\"+assFile.getUserName()+"\\"+assFile.getSemesterID()+"\\"+assFile.getCourseID()+"\\");
				String query;
				try {
					Files.createDirectories(destPath);
					Files.deleteIfExists(dest.toPath());
		            FileOutputStream fout=new FileOutputStream(dest);    
		            BufferedOutputStream bout=new BufferedOutputStream(fout);    
		            byte b[]=assFile.getMybytearray();
		            bout.write(b);    
		            bout.flush();    
		            bout.close();    
		            fout.close();  
		            String extension = "";
		            int i = dest.toString().lastIndexOf('.');
		            int p = Math.max(dest.toString().lastIndexOf('/'), dest.toString().lastIndexOf('\\'));
		            if (i > p) {
		                extension = dest.toString().substring(i+1);
		            }
					switch(assFile.getUserType()){
					case 2:
						if(!assFile.isReplace()){
							query = "INSERT INTO assignment_student () VALUES ('"+assFile.getAssNum()+"','"+assFile.getUserName()+"','-1','"+assFile.getUploadDate()+"',?,?)";
							dbConnection.sendInsertUpdateWithFile(query,extension,dest.toString());
						}
						else{
							query = "UPDATE assignment_student SET grade='"+assFile.getAssGrade()+"' , fileExc=? , file=? WHERE assStdAssID='"+assFile.getAssNum()+"' AND assStdStudentID='"+assFile.getStudentID()+"'";
							dbConnection.sendInsertUpdateWithFile(query,extension,dest.toString());
						}
						break;
					case 3:{
						if(assFile.isSetGrade()){
							query = "UPDATE assignment_student SET grade='"+assFile.getAssGrade()+"' , fileExc=? , file=? WHERE assStdAssID='"+assFile.getAssNum()+"' AND assStdStudentID='"+assFile.getStudentID()+"'";
							dbConnection.sendInsertUpdateWithFile(query,extension,dest.toString());
						}
						else if(assFile.isReplace()){
							query = "UPDATE assignments SET assName='"+assFile.getAssName()+"', dueDate='"+assFile.getDueDate()+"' , assSemID='"+assFile.getSemesterID()+"' , courseID='"+assFile.getCourseID()+"' , status='0' , fileExc=? ,file=? WHERE assNum='"+assFile.getAssNum()+"'";
							dbConnection.sendInsertUpdateWithFile(query,extension,dest.toString());
						}
						else{
							int assID  = dbConnection.sendInsertQuery("INSERT INTO assignments () VALUES ()");
							query = "UPDATE assignments SET assName='"+assFile.getAssName()+"' , uploadDate='"+assFile.getUploadDate()+"' , dueDate='"+assFile.getDueDate()+"' , assSemID='"+assFile.getSemesterID()+"' , courseID='"+assFile.getCourseID()+"' , status='0' , fileExc=? ,file=? WHERE assNum='"+String.valueOf(assID)+"'";
							dbConnection.sendInsertUpdateWithFile(query,extension,dest.toString());
							dbConnection.sendInsertQuery("INSERT INTO assignment_teacher () VALUES ('"+String.valueOf(assID)+"','"+assFile.getUserName()+"')");
						}
						break;
					}
					}
				} catch (IOException e) {
				}
				this.sendToClient("FileSaved", client);
			//}
		}else if(msg instanceof Msg){
			serverMain.addNewMsg((Msg)msg);
		}
	} // end of handleMessageFromClient method

	public void closeAllConnectionToDB() {

		Thread[] clientThreadList = getClientConnections();

		for (int i = 0; i < clientThreadList.length; i++) {
			try {
				dbConnection = (dbConnection) ((ConnectionToClient) clientThreadList[i]).getInfo("dbConnection");
				dbConnection.closeConnection();
				((ConnectionToClient) clientThreadList[i]).setInfo("dbConnection", null);
			} catch (Exception ex) {
			}
		}
	}
	
	
	public boolean sendToClientReply(Msg msg) {

		Thread[] clientThreadList = getClientConnections();

		for (int i = 0; i < clientThreadList.length; i++) {
			try {
				String Username = (String) ((ConnectionToClient) clientThreadList[i]).getInfo("Username");
				if(Username.compareTo(msg.getFromID())==0)
					((ConnectionToClient) clientThreadList[i]).sendToClient(msg);
				return true;
			} catch (Exception ex) {
				return false;
			}
		}
		return false;
	}
	

	

	/**
	 * This method overrides the one in the superclass. Called when the server
	 * starts listening for connections.
	 */
	protected void serverStarted() {
		serverMain.showMsg("Server listening for connections on port " + getPort());
	}

	/**
	 * This method overrides the one in the superclass. Called when the server
	 * stops listening for connections.
	 */
	protected void serverStopped() {
		serverMain.showMsg("Server has stopped listening for connections.");
	}

	public void setServerMain(ServerMain serverMain) {
		this.serverMain = serverMain;
	}

	// Class methods ***************************************************

}
// .End of EchoServer class
